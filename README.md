# Kuali Build Form Clone Tool

What is it?

The Kuali Build Form Clone Tool, a feature uniquely tailored within Kuali Build lets you replicate any Kuali Build form to different Kuali environments.

Why Would This be Beneficial to You?

1. Efficiency:
The Form Clone Tool streamlines the process by allowing users to replicate existing forms and workflows, eliminating the need to recreate from scratch. This optimizes time management, freeing up resources for other tasks.
2. Backup Utility:
The cloning feature offers an added layer of protection. Users can experiment with modifications, knowing they have a secure backup of their original work.
3. Migration Ease:
Migrate forms with ease between environments. This tool facilitates the seamless transfer of forms from development environments, like sandbox, to production, ensuring consistency and accuracy.
4. Development Flexibility:
Although typical development occurs in the production environment using draft mode, the Form Clone Tool offers an alternative. It allows for development in various environments, such as sandbox or staging, with easy integration into production.
5. Risk Management:
By cloning and preserving the current form state, users can explore various changes without compromising the original. This ensures the stability and reliability of both academic and administrative processes.
In Summary:
The Form Clone Tool is designed to enhance the user experience by promoting efficiency, flexibility, and security. Whether you're staff or a student, it's a tool aimed at making form management and development more streamlined and reliable.

What will be replicated:

- Form
- Owner (unless requested)
- Setting(s)
- Administrator(s)
- Permission(s)
- Integration(s)
- Workflow
- Setting(s)
- Integration(s)
- Documents dashboard (Settings - not documents)
- Setting(s)
- Filter(s)

### Step 1. User Identification

- Identifies and logs the user details like `user_duid`, `kuali_user_id`, etc.

- Necessary to ensure the correct user is associated with the migration.

### Step 2. User Verification and Setup

- Ensures the target user is provisioned in the target Kuali environment.

- Ensures that the target user has the necessary permissions to create apps in the target Kuali environment.

- Extracts information about the target user for further processing.

### Step 3. Form Preparation

- Publishes the originating form in the source Kuali environment, ensuring it's ready for migration.

### Step 4. User Authentication Setup

- Generates an authentication token for the target user.

- Retrieves the authentication token for further operations.

### Step 5. Form Creation in Target Environment

- Creates a new form in the target Kuali environment (as user) based on details from the source.

- Sets the newly created form as a favorite for easy user navigation/access.

- Revokes the temporary user authentication token once it's no longer needed.

### Step 6. Form Content Migration

- Extracts the details and data from the originating form.

- Extracts the workflow associated with the originating form.

- Notes integrations connected and in use within the current form

- Clones the details and data to the target form.

- Clones the workflow to the target form.

- Creates worklist of any integrations associated with the form.

### Step 7. Form Settings Migration

- Extracts the form settings of the originating form.

- Extracts the data settings of the originating form.

- Extracts the workflow settings of the originating form.

- Clones the form settings to the target form.

- Clones the data settings to the target form.

- Clones the workflow settings to the target form.

### Step 8. Integration and Workflow Update

- Reconstructs the integrations in the target environment, only if the source and target Kuali instances are different.

- reconstruction includes the following:

- Form integrations

- Workflow Integrations

- Clones the workflow of the target form to match the source form.

- Assigns integrations to target form within target environment.

- Sets integrations associated with the form in target environment to private.

### Step 9. Permissions Handling

- Extracts the permissions associated with the source form and updates the target form accordingly.

- Finalizes the updates to the target form based on the source form.

- Clones the permissions settings of the target form.

### Step 10. Finalizing Migration

- Publishes the target form, making it accessible and usable in the target Kuali environment.

## Prerequisites

- Python 3.11 or later installed
- Packages listed in requirements.txt file are installed

### Environment variables<br>

```docker
AUTH_TOKEN_SBX='Bearer xxx_your_sandbox_token_xxx'
KUALI_INSTANCE_SBX='duke-sbx' # you will need to change this to your own instance
AUTH_TOKEN_STG='Bearer xxx_your_staging_token_xxx'
KUALI_INSTANCE_STG='duke-stg' # you will need to change this to your own instance
AUTH_TOKEN_PRD='Bearer xxx_your_production_token_xxx'
KUALI_INSTANCE_PRD='duke' # you will need to change this to your own instance
BASIC_AUTH_USERNAME='xxx_your_basic_auth_username_xxx' # to use with your Kuali API/Integration username
BASIC_AUTH_PASSWORD='xxx_your_basic_auth_password_xxx' # to use with your Kuali API/Integration password
```

## Installation

Before you can run this project, you'll need to install the dependencies.<br>

Run the following command to uninstall all conflicting packages:

```bash
for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc; do sudo apt-get remove $pkg; done
```

Set up Docker's apt repository.

```bash
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```

Install the Docker packages.

```bash
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

1. Clone the repository to your local machine
2. Navigate to the project directory in a terminal.
3. Build the Docker image using the following command:

```bash
export CI_REGISTRY=gitlab.oit.duke.edu && export CI_PROJECT_PATH=kuali-build/kb-form-migration && export CI_PROJECT_NAME=kb-form-migration && docker compose build
```

## Usage

- To test the application in a Docker container, use the following command:

```bash
export CI_REGISTRY=gitlab.oit.duke.edu && export CI_PROJECT_PATH=kuali-build/kb-form-migration && export CI_PROJECT_NAME=kb-form-migration && docker compose up
```

- To run the application in a Docker container, use the following command:

```bash
export CI_REGISTRY=gitlab.oit.duke.edu && export CI_PROJECT_PATH=kuali-build/kb-form-migration && export CI_PROJECT_NAME=kb-form-migration && docker compose up -d
```

Of course, if you're curious to see how it works on localhost - just run:

```bash
uvicorn endpoints:app --host 0.0.0.0 --port 8118 --log-level info --workers 1 --loop uvloop --proxy-headers --http httptools
```

## Pulling from GitLab<br>

1. Login
- NOTE: adjust the depth as necessary -- if public repo, then disregard this step

```bash
source $(find . -maxdepth 5 -name .env | head -1) && echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
```

1. Pull
```bash
docker pull gitlab-registry.oit.duke.edu/kuali-build/kb-form-migration
```

1. Environemnt

```bash
export CI_REGISTRY=gitlab.oit.duke.edu
export CI_PROJECT_PATH=kuali-build/kb-form-migration
export CI_PROJECT_NAME=kb-form-migration
```

4. Build

```bash
docker compose build
```

5. Run

```bash
docker compose up -d
```



# Project Documentation

### File: [endpoints.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/endpoints.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard Libs
import sys
from contextlib import asynccontextmanager as acm
from os import getenv, makedirs, environ
from os.path import dirname, abspath, join
from fastapi import FastAPI, Depends, HTTPException, Request, BackgroundTasks
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from fastapi.responses import JSONResponse, ORJSONResponse
from loguru import logger
from dotenv import load_dotenv
import uvloop

import modules as mod
from utilities.task_processor import process_create_form_task
from utilities.middleware import configure_middlewares
from utilities.kuali_environment import get_env_variables
from utilities.blocked_ip_models import create_tables

CURRENT_DIR = dirname(abspath(__file__))

load_dotenv(verbose=True)
uvloop.install()
security = HTTPBasic()


def verify_credentials(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = getenv("BASIC_AUTH_USERNAME")
    correct_password = getenv("BASIC_AUTH_PASSWORD")
    if (
        credentials.username != correct_username
        or credentials.password != correct_password
    ):
        raise HTTPException(status_code=401, detail="Incorrect username or password")
    return credentials


def configure_logging(target_form_name: str):
    log_file = join(CURRENT_DIR, "kuali_skeletons", target_form_name, "actions.log")
    logger.add(
        log_file,
        rotation="10 MB",
        compression="zip",
        retention="10 days",
        format="{time}",
        serialize=True,
        enqueue=True,
        diagnose=True,
        backtrace=True,
        catch=True,
    )


def create_directories(target_form_name: str):
    base_dir = join(CURRENT_DIR, "kuali_skeletons", target_form_name)
    sub_dirs = ["integrations", "permissions"]
    for sub_dir in sub_dirs:
        makedirs(join(base_dir, sub_dir), exist_ok=True)


@acm
async def lifespan(app: FastAPI):
    logger.info("starting up")
    await create_tables()
    yield
    logger.warning("shutting down")
    await CLIENT.aclose()


app = FastAPI(
    openapi_url="/openapi.json",
    docs_url="/docs",
    redoc_url="/redoc",
    default_response_class=ORJSONResponse,
    dependencies=[Depends(verify_credentials)],
    lifespan=lifespan,
)

configure_middlewares(app)


@app.get("/knock-knock")
async def alive_response():
    return {"app": "ok"}


@app.get("/environment")
async def select_env():
    return [{"env": "production"}, {"env": "sandbox"}, {"env": "staging"}]


@app.get("/search")
async def search_spaces(search: str, kuali_instance: str):
    from_env_vars = get_env_variables(kuali_instance)
    source_kuali = mod.Kuali(from_env_vars["instance"], from_env_vars["auth_token"])
    searching = await mod.search_for_app(search, source_kuali)
    return JSONResponse(searching)


@app.post("/perform_tasks")
async def api_perform_tasks(request: Request, background_tasks: BackgroundTasks):
    data = await request.json()
    meta = data.get("meta", {}).get("createdBy", {})
    from_form = data.get("from_form", {}).get("data", {})
    if (
        form_other_owner := data.get("owner_select", {})
        .get("data", {})
        .get("typeahead", {})
    ):
        user_duid = form_other_owner.get("schoolId")
        kuali_user_id = form_other_owner.get("id")
    else:
        user_duid = meta.get("schoolId")
        kuali_user_id = meta.get("id")
    from_environment = data.get("from_environment", {}).get("data", {}).get("env")
    to_environment = data.get("to_environment", {}).get("data", {}).get("env")
    from_env_vars = get_env_variables(from_environment)
    to_env_vars = get_env_variables(to_environment)
    source_kuali = mod.Kuali(from_env_vars["instance"], from_env_vars["auth_token"])
    target_kuali = mod.Kuali(to_env_vars["instance"], to_env_vars["auth_token"])
    from_form_id = from_form.get("id")
    form_name = from_form.get("name")
    icon_name = from_form.get("tileOptions_iconName")
    icon_color = from_form.get("tileOptions_backgroundColor")
    target_form_name = data.get("target_form_name")
    configure_logging(target_form_name)
    create_directories(target_form_name)
    background_tasks.add_task(
        process_create_form_task,
        to_environment,
        request,
        user_duid,
        kuali_user_id,
        from_form_id,
        form_name,
        target_form_name,
        source_kuali,
        target_kuali,
        icon_name,
        icon_color,
    )

    return JSONResponse(
        status_code=202,
        content={"status": "Just.. give me a minute"},
        headers={"Connection": "close"},
    )
```

</details>

<details><summary><b>verify_credentials - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>verify_credentials - Code</b></summary>

```python
    correct_username = getenv("BASIC_AUTH_USERNAME")
    correct_password = getenv("BASIC_AUTH_PASSWORD")
    if (
        credentials.username != correct_username
        or credentials.password != correct_password
    ):
        raise HTTPException(status_code=401, detail="Incorrect username or password")
    return credentials
```

</details>

<details><summary><b>configure_logging - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>configure_logging - Code</b></summary>

```python
    log_file = join(CURRENT_DIR, "kuali_skeletons", target_form_name, "actions.log")
    logger.add(
        log_file,
        rotation="10 MB",
        compression="zip",
        retention="10 days",
        format="{time}",
        serialize=True,
        enqueue=True,
        diagnose=True,
        backtrace=True,
        catch=True,
    )
```

</details>

<details><summary><b>create_directories - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>create_directories - Code</b></summary>

```python
    base_dir = join(CURRENT_DIR, "kuali_skeletons", target_form_name)
    sub_dirs = ["integrations", "permissions"]
    for sub_dir in sub_dirs:
        makedirs(join(base_dir, sub_dir), exist_ok=True)
```

</details>

## Directory: modules

### File: [**init**.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/__init__.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
from .graphql_calls import Kuali
from .graphql_calls import KualiGraphQL
from .graphql_calls import chunkify
from .graphql_calls import SEMAPHORE
from .graphql_calls import CLIENT
from .graphql_calls import fetch_json_from_url
from .graphql_variables import KualiVariables

from .search_apps import search_for_app

from .circuit_breaker import CircuitBreaker

from .create_users import search_for_user
from .create_users import create_new_users
from .create_builder import add_owner_to_build_group
from .create_form import create_target_form
from .create_token import create_target_user_token
from .create_filters import create_target_form_filters
from .create_integrations import create_integrations
from .create_group import create_groups
from .create_permissions import create_permissions
from .create_group import create_groups

from .extract_user import extract_target_user_info
from .extract_apps import get_a_list_of_all_apps
from .extract_token import extract_target_user_token
from .extract_form import extract_originating_form
from .extract_form import publish_originating_form
from .extract_workflow import extract_originating_workflow
from .extract_integrations import extract_form_integrations
from .extract_permissions import extract_and_update_permissions
from .extract_permissions import extract_policy_groups
from .extract_permissions import update_policy_groups
from .extract_permissions import write_permission_files
from .extract_settings import extract_originating_form_settings
from .extract_settings import extract_originating_form_data_settings

from .reconstruct_integrations import reconstruct_integrations

from .update_form import update_target_form
from .update_workflow import update_target_workflow
from .update_integrations import update_form_integrations
from .update_permissions import update_form_permissions
from .update_settings import update_target_form_settings
from .update_settings import update_target_form_data_settings
from .update_token import revoke_target_user_token
from .update_favorite import update_target_form_favorite

from .publish_form import publish_target_form

from .delete_test_apps import delete_tests
```

</details>

### File: [circuit_breaker.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/circuit_breaker.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
import sys
from loguru import logger
import time


class CircuitBreaker:
    """
    Represents a circuit breaker for managing failures and retries.

    Args:
        max_failures (int): The maximum number of failures allowed before the circuit is opened.
        reset_time (float): The time in seconds after which the circuit is reset.
        backoff_factor (float): The factor by which the wait time increases for each retry attempt.
        max_attempts (int): The maximum number of retry attempts allowed.

    Methods:
        record_failure(): Records a failure and determines if a retry is possible.
        reset_attempts(): Resets the retry attempts counter.
        record_success(): Records a successful execution and resets the failure count.
        can_execute(): Checks if the circuit is closed and allows execution.

    Examples:
        >>> circuit_breaker = CircuitBreaker(max_failures=5, reset_time=1, backoff_factor=1.5, max_attempts=10)
        >>> circuit_breaker.record_failure()
        >>> circuit_breaker.can_execute()
    """
    def __init__(
        self, max_failures=5, reset_time=1, backoff_factor=1.5, max_attempts=10
    ):
        self.max_failures = max_failures
        self.reset_time = reset_time
        self.failures = 0
        self.last_failure_time = None
        self.backoff_factor = backoff_factor
        self.attempt = 0
        self.max_attempts = max_attempts

    async def record_failure(self):
        self.failures += 1
        self.attempt += 1
        self.last_failure_time = time.time()
        logger.warning(f"Failure recorded. Total failures: {self.failures}.")

        if self.failures >= self.max_failures:
            if self.attempt <= self.max_attempts:
                wait_time = self.reset_time * (self.backoff_factor**self.attempt)
                logger.warning(
                    f"Attempt {self.attempt} of {self.max_attempts}. Waiting for {wait_time} seconds before retry."
                )
                await asyncio.sleep(wait_time)
            else:
                logger.error(f"Max attempts ({self.max_attempts}) reached. Giving up.")

    def reset_attempts(self):
        self.attempt = 0

    def record_success(self):
        if self.failures > 0:
            logger.info(f"Success recorded after {self.failures} failures.")
        else:
            logger.info("Success recorded.")
        self.failures = 0
        self.last_failure_time = None

    def can_execute(self):
        if self.failures < self.max_failures:
            return True
        if time.time() - self.last_failure_time > self.reset_time:
            self.record_success()
            return True
        logger.warning(
            f"Cannot execute. Last failure was less than {self.reset_time} seconds ago."
        )
        return False
```

</details>

<details><summary><b>CircuitBreaker - Docstring</b></summary>

```txt
Represents a circuit breaker for managing failures and retries.

Args:
    max_failures (int): The maximum number of failures allowed before the circuit is opened.
    reset_time (float): The time in seconds after which the circuit is reset.
    backoff_factor (float): The factor by which the wait time increases for each retry attempt.
    max_attempts (int): The maximum number of retry attempts allowed.

Methods:
    record_failure(): Records a failure and determines if a retry is possible.
    reset_attempts(): Resets the retry attempts counter.
    record_success(): Records a successful execution and resets the failure count.
    can_execute(): Checks if the circuit is closed and allows execution.

Examples:
    >>> circuit_breaker = CircuitBreaker(max_failures=5, reset_time=1, backoff_factor=1.5, max_attempts=10)
    >>> circuit_breaker.record_failure()
    >>> circuit_breaker.can_execute()
```

</details>
<details><summary><b>CircuitBreaker - Code</b></summary>

```python
    def __init__(
        self, max_failures=5, reset_time=1, backoff_factor=1.5, max_attempts=10
    ):
        self.max_failures = max_failures
        self.reset_time = reset_time
        self.failures = 0
        self.last_failure_time = None
        self.backoff_factor = backoff_factor
        self.attempt = 0
        self.max_attempts = max_attempts

    async def record_failure(self):
        self.failures += 1
        self.attempt += 1
        self.last_failure_time = time.time()
        logger.warning(f"Failure recorded. Total failures: {self.failures}.")

        if self.failures >= self.max_failures:
            if self.attempt <= self.max_attempts:
                wait_time = self.reset_time * (self.backoff_factor**self.attempt)
                logger.warning(
                    f"Attempt {self.attempt} of {self.max_attempts}. Waiting for {wait_time} seconds before retry."
                )
                await asyncio.sleep(wait_time)
            else:
                logger.error(f"Max attempts ({self.max_attempts}) reached. Giving up.")

    def reset_attempts(self):
        self.attempt = 0

    def record_success(self):
        if self.failures > 0:
            logger.info(f"Success recorded after {self.failures} failures.")
        else:
            logger.info("Success recorded.")
        self.failures = 0
        self.last_failure_time = None

    def can_execute(self):
        if self.failures < self.max_failures:
            return True
        if time.time() - self.last_failure_time > self.reset_time:
            self.record_success()
            return True
        logger.warning(
            f"Cannot execute. Last failure was less than {self.reset_time} seconds ago."
        )
        return False
```

</details>

<details><summary><b>__init__ - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>__init__ - Code</b></summary>

```python
        self, max_failures=5, reset_time=1, backoff_factor=1.5, max_attempts=10
    ):
        self.max_failures = max_failures
        self.reset_time = reset_time
        self.failures = 0
        self.last_failure_time = None
        self.backoff_factor = backoff_factor
        self.attempt = 0
        self.max_attempts = max_attempts
```

</details>

<details><summary><b>reset_attempts - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>reset_attempts - Code</b></summary>

```python
        self.attempt = 0
```

</details>

<details><summary><b>record_success - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>record_success - Code</b></summary>

```python
        if self.failures > 0:
            logger.info(f"Success recorded after {self.failures} failures.")
        else:
            logger.info("Success recorded.")
        self.failures = 0
        self.last_failure_time = None
```

</details>

<details><summary><b>can_execute - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>can_execute - Code</b></summary>

```python
        if self.failures < self.max_failures:
            return True
        if time.time() - self.last_failure_time > self.reset_time:
            self.record_success()
            return True
        logger.warning(
            f"Cannot execute. Last failure was less than {self.reset_time} seconds ago."
        )
        return False
```

</details>

### File: [create_builder.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/create_builder.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
# sys.path.append(current_dir)

from modules.graphql_calls import Kuali, KualiGraphQL
from modules.graphql_variables import KualiVariables


async def add_owner_to_build_group(env_str, kuali_user_id, kuali_instance):
    """
    Adds the owner identified by the provided Kuali user ID to the build group based on the specified environment.
    The environment is determined by the env_str parameter.
    It sends a GraphQL mutation request to add the user to the corresponding group.

    Args:
        env_str: The environment string ("production", "sandbox", or other).
        kuali_user_id: The ID of the Kuali user.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    logger.info(kuali_instance)
    if env_str == "production":
        group_id = "62014a361db08c0a125717ce"
    if env_str == "sandbox":
        group_id = "621ce0b691de7c4ef79dc4d2"
    else:
        group_id = "65108c977c6aed989e18cf7b"

    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.AddGroupRoleUser()
    logger.info(graphql)
    variables = KualiVariables.Mutations.AddGroupRoleUser(
        group_id, "members", kuali_user_id
    )
    logger.info(variables)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    logger.info(outcome)
    try:
        results = (
            outcome.get("data", {})
            .get("addGroupRoleUser", {})
            .get("result", {})
            .get("members", [{}])
        )
        logger.info(results)
        for result in results:
            user_id = result.get("id")
            user_name = result.get("label")
            if user_id == kuali_user_id:
                logger.info(f"{user_name} has been added to build app creator group")
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
```

</details>

### File: [create_filters.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/create_filters.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def create_target_form_filters(target_form_id, target_form_name, kuali_instance):
    """
    Creates custom filters for the target form using the provided target form ID, target form name,
    and Kuali instance.

    Args:
        target_form_id: The ID of the target form.
        target_form_name: The name of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    with open(join(parent_dir, "custom_filters.json"), "r") as f:
        integrations = json.load(f)

    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.CreateFilters()

    for form_filter in integrations:
        form_filter["appId"] = target_form_id

        variables = KualiVariables.Mutations.CreateFilters(form_filter)
        outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
        try:
            success_returned = outcome

            if success_returned is not None:
                user_display_name = form_filter["name"]
                form_filter.pop("name", None)
                graphql = KualiGraphQL.Mutations.ShareFilters()
                variables = KualiVariables.Mutations.ShareFilters(form_filter)
                shared_filters_outcome = await Kuali.FetchResponse(
                    url, headers, graphql, variables
                )
                try:
                    success_returned = (
                        shared_filters_outcome.get("data", {})
                        .get("shareFilter", {})
                        .get("id")
                    )
                    constr_url = f"{user_display_name}: https://duke.kualibuild.com/app/builder/document-list/{target_form_id}?id={success_returned}"

                    if success_returned is not None:
                        logger.success(
                            f"Custom documents page filter and link created for {user_display_name}"
                        )
                        with open(
                            join(
                                parent_dir,
                                "kuali_skeletons",
                                target_form_name,
                                "shared_filters.txt",
                            ),
                            "a",
                        ) as f:
                            f.write(constr_url + "\n")
                    else:
                        raise KeyError
                except Exception as oops:
                    logger.error(f"Error: {str(oops)}")
            else:
                raise KeyError
        except Exception as oops:
            logger.error(f"Error: {str(oops)}")
```

</details>

### File: [create_form.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/create_form.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def create_target_form(
    target_form_name,
    target_space_id,
    user_id,
    icon_name,
    icon_color,
    user_auth_token,
    user_name,
    kuali_instance,
):
    """
    Creates a target form using the provided target form name, target space ID, user ID, icon name,
    icon color, user authentication token, user name, and Kuali instance.

    Args:
        target_form_name: The name of the target form.
        target_space_id: The ID of the target space.
        user_id: The ID of the user.
        icon_name: The name of the icon.
        icon_color: The color of the icon.
        user_auth_token: The authentication token of the user.
        user_name: The name of the user.
        kuali_instance: The Kuali instance.

    Returns:
        The ID of the created target form.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers(user_auth_token)
    graphql = KualiGraphQL.Mutations.CreateApp()
    variables = KualiVariables.Mutations.CreateApp(
        target_form_name, target_space_id, icon_name, icon_color
    )
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = outcome.get("data", {}).get("data", {})

        if success_returned is None:
            raise KeyError
        target_form_id = success_returned.get("id", None)
        logger.success(f"Form created with id: {target_form_id}")

        return target_form_id
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
```

</details>

### File: [create_group.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/create_group.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger
import traceback

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables
from .create_users import search_for_user


async def create_groups(members, group_name, kuali_from, kuali_to):
    """
    Creates a group with the provided members and group name using the Kuali instances for the source and destination.

    Args:
        members: The list of members to add to the group.
        group_name: The name of the group.
        kuali_from: The Kuali instance for the source.
        kuali_to: The Kuali instance for the destination.

    Returns:
        A dictionary containing the ID of the created group, or None if the group creation failed.
    """
    url = kuali_to.url()
    headers = kuali_to.headers()
    graphql = KualiGraphQL.Mutations.CreateGroup()
    variables = KualiVariables.Mutations.CreateGroup(group_name)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        if group := outcome.get("data", {}).get("createGroup", {}).get("group"):
            if group := group.get("id"):
                logger.info(group)
                await add_group_members(members, group, kuali_from, kuali_to)
                return {"id": group}
        logger.error("Group ID or Category ID not found in response.")
    except Exception as oops:
        logger.error(f"Error: {str(oops)}\n{traceback.format_exc()}")
    return None


async def add_group_members(members, new_group_id, kuali_from, kuali_to):
    """
    Adds members to a group with the provided group ID using the Kuali instances for the source and destination.

    Args:
        members: The list of members to add to the group.
        new_group_id: The ID of the group.
        kuali_from: The Kuali instance for the source.
        kuali_to: The Kuali instance for the destination.

    Returns:
        None.
    """
    url = kuali_to.url()
    headers = kuali_to.headers()
    graphql = KualiGraphQL.Mutations.AddGroupRoleUser()
    for member in members:
        user_id = member.get("node", {}).get("id")
        get_each_user = await search_for_user(user_id, kuali_from, kuali_to)

        if isinstance(get_each_user, dict):
            user_id = get_each_user.get("id")

        variables = KualiVariables.Mutations.AddGroupRoleUser(
            new_group_id, "members", user_id
        )
        outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
        try:
            results = (
                outcome.get("data", {})
                .get("addGroupRoleUser", {})
                .get("result", {})
                .get("members", [])
            )
            logger.info(f"{results}")
        except Exception as oops:
            logger.error(f"Error: {str(oops)}\n{traceback.format_exc()}")
```

</details>

### File: [create_integrations.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/create_integrations.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def create_integrations(target_form_id, role_name, kuali_instance):
    """
    Creates integrations for the target form using the provided target form ID, role name,
    and Kuali instance.

    Args:
        target_form_id: The ID of the target form.
        role_name: The name of the role.
        kuali_instance: The Kuali instance.

    Returns:
        The ID of the created target form, or None if the creation failed.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.CreateIntegration()
    variables = KualiVariables.Mutations.CreatePolicyGroup(target_form_id, role_name)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = outcome.get("data", {}).get("data", {})

        if success_returned is not None:
            target_form_id = success_returned.get("id", None)
            logger.success(f"Form created with id: {target_form_id}")

            return target_form_id
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
```

</details>

### File: [create_permissions.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/create_permissions.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def create_permissions(target_form_id, role_name, kuali_instance):
    """
    Creates permissions for the target form using the provided target form ID, role name,
    and Kuali instance.

    Args:
        target_form_id: The ID of the target form.
        role_name: The name of the role.
        kuali_instance: The Kuali instance.

    Returns:
        The ID of the created policy group, or None if the creation failed.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.CreatePolicyGroup()
    variables = KualiVariables.Mutations.CreatePolicyGroup(target_form_id, role_name)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = outcome.get("data", {}).get("data", {})

        if success_returned is not None:
            target_form_id = success_returned.get("id", None)
            logger.success(f"Policy Group created for form: {target_form_id}")

            return target_form_id
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
```

</details>

### File: [create_token.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/create_token.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def create_target_user_token(
    user_id, target_form_name, user_name, kuali_instance
):
    """
    Creates a temporary authentication token for the target user using the provided user ID,
    target form name, user name, and Kuali instance.

    Args:
        user_id: The ID of the target user.
        target_form_name: The name of the target form.
        user_name: The name of the target user.
        kuali_instance: The Kuali instance.

    Returns:
        The temporary authentication token for the target user, or None if the creation failed.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.CreateToken()
    variables = KualiVariables.Mutations.CreateToken(user_id, target_form_name)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = outcome.get("data", {}).get("createApiKey", None)
        if success_returned is not None:
            user_auth_token = success_returned
            logger.success(f"{user_name} temp auth token created!")
            return user_auth_token
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
```

</details>

### File: [create_users.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/create_users.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger
import traceback

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def search_for_user(user_kuali_id, kuali_from, kuali_to):
    """
    Searches for a user with the provided Kuali ID in the source environment and retrieves or creates
    the corresponding user in the target environment using the provided Kuali instances.

    Args:
        user_kuali_id: The Kuali ID of the user.
        kuali_from: The Kuali instance for the source environment.
        kuali_to: The Kuali instance for the target environment.

    Returns:
        A dictionary containing the user data in the target environment, or None if the search or creation failed.
    """
    try:
        user_data_from = await get_user_data(user_kuali_id, kuali_from)
        if not user_data_from:
            raise ValueError("User not found in source environment")

        schoolId = user_data_from.get("schoolId")

        user_data_to = await get_target_user_data(
            schoolId, kuali_to
        ) or await create_new_users(format_create_user(user_data_from), kuali_to)

        if not user_data_to:
            raise ValueError("Failed to retrieve or create user in target environment")
        return {
            "email": user_data_to.get("email"),
            "firstName": user_data_to.get("firstName"),
            "id": user_data_to.get("id"),
            "lastName": user_data_to.get("lastName"),
            "schoolId": user_data_to.get("schoolId"),
            "username": user_data_to.get("username"),
            "name": user_data_to.get("displayName"),
        }
    except Exception as damn:
        logger.error(f"Error: {str(damn)}\n{traceback.format_exc()}")
        return None


async def get_user_data(user_kuali_id, kuali):
    """
    Retrieves user data for the specified Kuali ID using the provided Kuali instance.

    Args:
        user_kuali_id: The Kuali ID of the user.
        kuali: The Kuali instance.

    Returns:
        A dictionary containing the user data, or None if the user data could not be retrieved.
    """
    url = kuali.url()
    headers = kuali.headers()
    graphql = KualiGraphQL.Queries.GetUserByKualiID()
    variables = KualiVariables.Queries.GetUserByKualiID(user_kuali_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    return outcome.get("data", {}).get("user") or None


async def get_target_user_data(schoolId, kuali):
    """
    Retrieves the target user data for the specified school ID using the provided Kuali instance.

    Args:
        schoolId: The school ID of the target user.
        kuali: The Kuali instance.

    Returns:
        A dictionary containing the target user data, or None if the user data could not be retrieved.
    """
    url = kuali.url()
    headers = kuali.headers()
    graphql = KualiGraphQL.Queries.GetUser()
    variables = KualiVariables.Queries.GetUser(schoolId)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)

    if users_connection := outcome.get("data", {}).get("usersConnection"):
        edges = users_connection.get("edges", [])
        if not edges:
            logger.warning(
                f"No edges found in users_connection for schoolId: {schoolId}"
            )
        return edges[0].get("node") if edges else None
    logger.warning(f"No usersConnection found for schoolId: {schoolId}")
    return None


def format_create_user(user_data):
    """
    Formats the user data into a dictionary suitable for creating a new user.

    Args:
        user_data: The user data to be formatted.

    Returns:
        A dictionary containing the formatted user data.
    """
    if user_data:
        return {
            "data": {
                "role": "user",
                "approved": "true",
                "id": user_data.get("id"),
                "firstName": user_data.get("firstName"),
                "lastName": user_data.get("lastName"),
                "name": user_data.get("name"),
                "username": user_data.get("username"),
                "email": user_data.get("email"),
                "schoolId": user_data.get("schoolId"),
            }
        }


async def create_new_users(create_user, kuali_to):
    """
    Creates new users using the provided user data and Kuali instance.

    Args:
        create_user: The user data for creating new users.
        kuali_to: The Kuali instance for the target environment.

    Returns:
        A dictionary containing the user data of the created users, or None if the creation failed.
    """
    try:
        url = kuali_to.url()
        headers = kuali_to.headers()
        graphql = KualiGraphQL.Mutations.CreateUser()
        variables = KualiVariables.Mutations.CreateUser(create_user)
        outcome = await Kuali.FetchResponse(url, headers, graphql, variables)

        user_data = outcome.get("data", {}).get("createUser")
        if not user_data:
            logger.error("No user data returned while creating new user.")
            return None

        if errors := user_data.get("errors"):
            logger.error(f"Errors encountered while creating user: {errors}")

        return user_data
    except Exception as dammit:
        logger.error(
            f"Error in create_new_users: {str(dammit)}\n{traceback.format_exc()}"
        )
```

</details>

<details><summary><b>format_create_user - Docstring</b></summary>

```txt
Formats the user data into a dictionary suitable for creating a new user.

Args:
    user_data: The user data to be formatted.

Returns:
    A dictionary containing the formatted user data.
```

</details>
<details><summary><b>format_create_user - Code</b></summary>

```python
    if user_data:
        return {
            "data": {
                "role": "user",
                "approved": "true",
                "id": user_data.get("id"),
                "firstName": user_data.get("firstName"),
                "lastName": user_data.get("lastName"),
                "name": user_data.get("name"),
                "username": user_data.get("username"),
                "email": user_data.get("email"),
                "schoolId": user_data.get("schoolId"),
            }
        }
```

</details>

### File: [delete_test_apps.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/delete_test_apps.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
import os
import sys
import json
from os.path import dirname, abspath, join
from loguru import logger
import aiofiles

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def delete_tests(target_form_name, kuali_instance):
    """
    Deletes test applications and integrations associated with the target form using the provided
    target form name and Kuali instance.

    Args:
        target_form_name: The name of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()

    gql_file = join(
        parent_dir,
        "kuali_skeletons",
        target_form_name,
        "remove_created_apps_and_integrations.gql",
    )
    json_file = join(
        parent_dir,
        "kuali_skeletons",
        target_form_name,
        "remove_created_apps_and_integrations.json",
    )

    async with aiofiles.open(gql_file, mode="r") as gql:
        graphql = await gql.read()

    async with aiofiles.open(json_file, mode="r") as variable:
        variables = await variable.read()

    variables = json.loads(variables)

    try:
        outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
        success_returned = outcome.get("data", {})
        if app_status := success_returned.get("deleteAppAlias"):
            logger.success(
                "#################App + Integrations deleted successfully#################"
            )

        for key in success_returned:
            if key.startswith("removeIntegration") and success_returned.get(key):
                logger.success(f"{key} deleted successfully")

        if not success_returned:
            raise KeyError("Items not deleted from target environment")

    except json.JSONDecodeError:
        logger.error("Error decoding JSON response")
    except KeyError as ke:
        logger.error(f"KeyError: {ke}")
    except Exception as oops:
        logger.error(f"Unexpected error: {str(oops)}")
```

</details>

### File: [extract_apps.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/extract_apps.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def get_a_list_of_all_apps(kuali_instance):
    """
    Retrieves a list of all apps from the Kuali instance.

    Args:
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Queries.ExtractApps()
    variables = KualiVariables.Queries.ExtractApps()
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    returned_data = sorted(
        [
            {"id": app["id"], "name": " ".join(app["name"].strip().split())}
            for app in outcome["data"]["apps"]
        ],
        key=lambda app: app["name"],
    )

    for item in returned_data:
        item["name"] = " ".join(item["name"].split())

    file_path = join(parent_dir, "apps.json")

    with open(file_path, "w") as f:
        json.dump(returned_data, f, indent=2)


if __name__ == "__main__":
    import asyncio
    import uvloop

    uvloop.install()
    asyncio.run(get_a_list_of_all_apps())
```

</details>

### File: [extract_form.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/extract_form.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def publish_originating_form(originating_form_id, kuali_instance):
    """
    Publishes the originating form with the provided form ID using the provided Kuali instance.

    Args:
        originating_form_id: The ID of the originating form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.PublishForm()
    variables = KualiVariables.Mutations.PublishForm(originating_form_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = (
            outcome.get("data", {})
            .get("publishApp", {})
            .get("dataset", {})
            .get("isPublished", None)
        )
        if success_returned is not None:
            logger.success(
                "Published the origin form to ensure we have the most up-to-date template"
            )
        else:
            raise KeyError
    except Exception:
        error_returned = outcome.get("errors", [{}])[0].get("message", "Unknown error")
        logger.success(
            f"No need to publish the original form because Kuali says '{error_returned}'"
        )


def find_ids(obj, result=None):
    """
    Recursively searches for and extracts IDs from the provided object.

    Args:
        obj: The object to search for IDs.
        result: The list to store the extracted IDs.

    Returns:
        A list of extracted IDs.
    """
    if result is None:
        result = []
    if isinstance(obj, dict):
        for k, v in obj.items():
            if k == "id" and isinstance(v, str) and len(v) == 24 and "data" not in v:
                new_item = {"integrationId": v}
                if new_item not in result:
                    result.append(new_item)
            elif isinstance(v, (dict, list)):
                find_ids(v, result)
    elif isinstance(obj, list):
        for item in obj:
            find_ids(item, result)
    return result


async def extract_originating_form(
    originating_form_id,
    target_form_id,
    originating_form_name,
    target_form_name,
    kuali_instance,
):
    """
    Extracts the originating form with the provided form ID and saves it as a JSON file.
    Additionally, it searches for 'id' keys with 24-character values in the extracted form
    and saves them to another JSON file.

    Args:
        originating_form_id: The ID of the originating form.
        target_form_id: The ID of the target form.
        originating_form_name: The name of the originating form.
        target_form_name: The name of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Queries.ExtractForm()
    variables = KualiVariables.Queries.ExtractForm(originating_form_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = (
            outcome.get("data", {}).get("app", {}).get("formVersion", None)
        )

        if success_returned is not None:
            success_returned["appId"] = target_form_id
            json_str = json.dumps(success_returned, ensure_ascii=False, indent=4)
            with open(
                join(
                    parent_dir,
                    "kuali_skeletons",
                    target_form_name,
                    "form.json",
                ),
                "w",
            ) as f:
                f.write(json_str)
            logger.success("form.json file created")

            # Find all 'id' keys with 24-character values and write them to a new JSON file
            id_dict = find_ids(success_returned)
            with open(
                join(
                    parent_dir,
                    "kuali_skeletons",
                    target_form_name,
                    "integrations.json",
                ),
                "w",
            ) as f:
                f.write(json.dumps(id_dict, indent=4))
            logger.success("integrations.json file created")
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
```

</details>

<details><summary><b>find_ids - Docstring</b></summary>

```txt
Recursively searches for and extracts IDs from the provided object.

Args:
    obj: The object to search for IDs.
    result: The list to store the extracted IDs.

Returns:
    A list of extracted IDs.
```

</details>
<details><summary><b>find_ids - Code</b></summary>

```python
    if result is None:
        result = []
    if isinstance(obj, dict):
        for k, v in obj.items():
            if k == "id" and isinstance(v, str) and len(v) == 24 and "data" not in v:
                new_item = {"integrationId": v}
                if new_item not in result:
                    result.append(new_item)
            elif isinstance(v, (dict, list)):
                find_ids(v, result)
    elif isinstance(obj, list):
        for item in obj:
            find_ids(item, result)
    return result
```

</details>

### File: [extract_groups.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/extract_groups.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables
from .create_users import search_for_user


async def extract_groups(group_id, kuali_from, kuali_to):
    """
    Extracts the members of a group with the provided group ID from the source environment using the
    Kuali instance for the source environment and returns the extracted members.

    Args:
        group_id: The ID of the group.
        kuali_from: The Kuali instance for the source environment.
        kuali_to: The Kuali instance for the target environment.

    Returns:
        A list of dictionaries containing the extracted members of the group, or an empty list if no members are found.
    """
    url = kuali_from.url()
    headers = kuali_from.headers()
    graphql = KualiGraphQL.Queries.GetGroupByKualiID()
    variables = KualiVariables.Queries.GetGroupByKualiID(group_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)

    try:
        group = outcome.get("data", {}).get("group", {})
        group_name = group.get("name")
        roles = group.get("roles", [])

        for role in roles:
            return role.get("membersConnection", {}).get("edges", [{}])
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")

    return []
```

</details>

### File: [extract_integrations.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/extract_integrations.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


replace_ids = {"6499ba62ea5822d07308fa6e": "64c2fd781a39e7ec9d7807e5"}


def replace_id_in_object(obj, replace_ids):
    """
    Recursively replaces IDs in the provided object with the corresponding replacement IDs.

    Args:
        obj: The object in which to replace the IDs.
        replace_ids: A dictionary mapping original IDs to replacement IDs.

    Returns:
        The object with replaced IDs.
    """
    if isinstance(obj, dict):
        for k, v in obj.items():
            if k == "id" and v in replace_ids:
                obj["id"] = replace_ids[v]
            if isinstance(v, (dict, list)):
                replace_id_in_object(v, replace_ids)
    elif isinstance(obj, list):
        for item in obj:
            replace_id_in_object(item, replace_ids)
    return obj


def update_labels(obj, new_label, target_id):
    """
    Recursively updates the label of the integration with the specified target ID in the provided object
    to the new label.

    Args:
        obj: The object to search for the integration with the target ID.
        new_label: The new label to update the integration with.
        target_id: The target ID of the integration to update.

    Returns:
        The updated object.
    """
    if isinstance(obj, dict):
        if (
            "details" in obj
            and isinstance(obj["details"], dict)
            and obj["details"].get("id") == target_id
        ):
            logger.success(
                f"Updating matching integration | id: {target_id} | name: {new_label}"
            )
            if "label" in obj["details"]:
                logger.success(
                    f"Updating label from '{obj['details']['label']}' to '{new_label}'"
                )
                obj["details"]["label"] = new_label
        for k, v in obj.items():
            if isinstance(v, (dict, list)):
                update_labels(v, new_label, target_id)
    elif isinstance(obj, list):
        for item in obj:
            update_labels(item, new_label, target_id)
    return obj


async def extract_form_integrations(
    originating_form_id,
    target_form_id,
    originating_form_name,
    target_form_name,
    kuali_from,
    kuali_to,
):
    """
    Extracts form integrations from the originating form, updates the integration IDs and labels,
    and saves the updated integrations as JSON files. It also generates a GraphQL mutation string
    and a variables JSON file for removing the created apps and integrations.

    Args:
        originating_form_id: The ID of the originating form.
        target_form_id: The ID of the target form.
        originating_form_name: The name of the originating form.
        target_form_name: The name of the target form.
        kuali_from: The Kuali instance for the source environment.
        kuali_to: The Kuali instance for the target environment.

    Returns:
        None.
    """
    with open(
        join(
            parent_dir,
            "kuali_skeletons",
            target_form_name,
            "integrations.json",
        ),
        "r",
    ) as f:
        integrations = json.load(f)

    index = 1
    new_integrations = []
    for integration in integrations:
        if integration_id := integration.get("integrationId"):
            # Replace or remove the integration ID
            if integration_id in replace_ids:
                if replace_ids[integration_id] is None:
                    logger.warning(f"Removing integration {integration_id}...")
                    continue
                else:
                    logger.info(
                        f"Replacing integration {integration_id} with {replace_ids[integration_id]}..."
                    )
                    integration_id = replace_ids[integration_id]
            url = kuali_from.url()
            headers = kuali_from.headers()
            graphql = KualiGraphQL.Queries.ExtractIntegration()
            variables = KualiVariables.Queries.ExtractIntegration(integration_id)
            outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
            try:
                integration_name = (
                    outcome.get("data", {}).get("integration", {}).get("name")
                )
                data_shared_with = (
                    outcome.get("data", {})
                    .get("integration", {})
                    .get("dataSharedWith", {})
                )
                share_type = data_shared_with.get("type", None)
                success_returned = data_shared_with.get("apps", [])

                if integration_name is None:
                    logger.warning(
                        f"No name for integration {integration_id}. Removing from list..."
                    )
                    continue

                if data_shared_with is None:
                    logger.warning(
                        f"No dataSharedWith for integration {integration_id}. Skipping..."
                    )
                    continue

                integration["integrationName"] = integration_name
                if share_type == "ALL" and kuali_from == kuali_to:
                    integration["Integration availability"] = "Shared with all apps"
                elif share_type == "SPECIFIC":
                    integration[
                        "Integration availability"
                    ] = "Shared with specific apps only"
                new_integrations.append(integration)

                if share_type is None:
                    raise KeyError
                for app in success_returned:
                    app["label"] = app.pop("name", None)
                new_app = {"id": target_form_id, "label": target_form_name}
                success_returned.append(new_app)
                seen = set()
                success_returned = [
                    app
                    for app in success_returned
                    if app["id"] not in seen and not seen.add(app["id"])
                ]

                if kuali_from != kuali_to:
                    share_type = "SPECIFIC"

                data_to_prepend = {
                    "id": integration_id,
                    "sharingType": share_type,
                    "apps": success_returned,
                }

                json_str = json.dumps(data_to_prepend, indent=4)
                with open(
                    join(
                        parent_dir,
                        "kuali_skeletons",
                        target_form_name,
                        "integrations",
                        f"integration_{index}.json",
                    ),
                    "w",
                ) as f:
                    f.write(json_str)

                logger.success(f"integration_{index}.json file created")
                index += 1
            except Exception as oops:
                logger.success(
                    "Delisting unused integration from form integration options"
                )

        with open(
            join(
                parent_dir,
                "kuali_skeletons",
                target_form_name,
                "integrations.json",
            ),
            "w",
        ) as f:
            json.dump(integrations, f, indent=4)

    #####################Construct East removal###############################

    mutation_parts = []  # To store parts of the mutation string.
    variables = {"appId": target_form_id}  # Initializing the variables dict with appId.

    for idx, integration in enumerate(
        new_integrations, start=1
    ):  # Note: idx starts from 1.
        if integration_id := integration.get("integrationId"):
            mutation_parts.append(
                f"  removeIntegration{idx}: removeIntegration(args: {{id: $id{idx}}}) {{ id }}"
            )
            variables[f"id{idx}"] = integration_id

    # Constructing the final mutation string.
    mutation_str = f"mutation CombinedMutation($appId: ID!, {' ,'.join([f'$id{i}: ID!' for i in range(1, len(new_integrations) + 1)])}) {{\n"
    mutation_str += f"  deleteAppAlias: deleteApp(id: $appId)\n"
    mutation_str += "\n".join(mutation_parts)
    mutation_str += "\n}"

    # Writing the mutation string to a .gql file
    gql_filename = join(
        parent_dir,
        "kuali_skeletons",
        target_form_name,
        "remove_created_apps_and_integrations.gql",
    )
    with open(gql_filename, "w") as f:
        f.write(mutation_str)

    # Writing the variables to a .json file
    json_filename = join(
        parent_dir,
        "kuali_skeletons",
        target_form_name,
        "remove_created_apps_and_integrations.json",
    )
    with open(json_filename, "w") as f:
        json.dump(variables, f, indent=4)
```

</details>

<details><summary><b>replace_id_in_object - Docstring</b></summary>

```txt
Recursively replaces IDs in the provided object with the corresponding replacement IDs.

Args:
    obj: The object in which to replace the IDs.
    replace_ids: A dictionary mapping original IDs to replacement IDs.

Returns:
    The object with replaced IDs.
```

</details>
<details><summary><b>replace_id_in_object - Code</b></summary>

```python
    if isinstance(obj, dict):
        for k, v in obj.items():
            if k == "id" and v in replace_ids:
                obj["id"] = replace_ids[v]
            if isinstance(v, (dict, list)):
                replace_id_in_object(v, replace_ids)
    elif isinstance(obj, list):
        for item in obj:
            replace_id_in_object(item, replace_ids)
    return obj
```

</details>

<details><summary><b>update_labels - Docstring</b></summary>

```txt
Recursively updates the label of the integration with the specified target ID in the provided object
to the new label.

Args:
    obj: The object to search for the integration with the target ID.
    new_label: The new label to update the integration with.
    target_id: The target ID of the integration to update.

Returns:
    The updated object.
```

</details>
<details><summary><b>update_labels - Code</b></summary>

```python
    if isinstance(obj, dict):
        if (
            "details" in obj
            and isinstance(obj["details"], dict)
            and obj["details"].get("id") == target_id
        ):
            logger.success(
                f"Updating matching integration | id: {target_id} | name: {new_label}"
            )
            if "label" in obj["details"]:
                logger.success(
                    f"Updating label from '{obj['details']['label']}' to '{new_label}'"
                )
                obj["details"]["label"] = new_label
        for k, v in obj.items():
            if isinstance(v, (dict, list)):
                update_labels(v, new_label, target_id)
    elif isinstance(obj, list):
        for item in obj:
            update_labels(item, new_label, target_id)
    return obj
```

</details>

### File: [extract_permissions.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/extract_permissions.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger
import string
import aiofiles
import traceback

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables
from .create_users import search_for_user
from .create_group import create_groups, add_group_members

NAME_MAPPING = {
    "Administrators": "admin",
    "Reviewers": "reviewer",
    "All Anonymous Users": "users",
}
FILE_DIR = dirname(abspath(__file__))
PARENT_DIR = dirname(FILE_DIR)
sys.path.append(FILE_DIR)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables
from .create_users import search_for_user
from .extract_groups import extract_groups


async def extract_policy_groups(form_id, form_name, kuali):
    """
    Extracts the policy groups associated with the specified form ID using the provided Kuali instance.

    Args:
        form_id: The ID of the form.
        form_name: The name of the form.
        kuali: The Kuali instance.

    Returns:
        A list of dictionaries containing the extracted policy groups, or an empty list if no policy groups are found.
    """
    try:
        url = kuali.url()
        headers = kuali.headers()
        graphql = KualiGraphQL.Queries.ExtractPermissions()
        variables = KualiVariables.Queries.ExtractPermissions(form_id)
        outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
        return outcome.get("data", {}).get("app", {}).get("listPolicyGroups", [{}])
    except Exception as oops:
        logger.error(
            f"Error in extract_policy_groups: {str(oops)}\n{traceback.format_exc()}"
        )
        return []


async def update_policy_groups(policy_groups, newbee_policy_group_dict):
    """
    Updates the IDs of the policy groups in the provided list of policy groups
    using the dictionary of new policy group IDs.

    Args:
        policy_groups: The list of policy groups to update.
        newbee_policy_group_dict: The dictionary mapping policy group names to new policy group IDs.

    Returns:
        None.
    """
    try:
        for group in policy_groups:
            if group["name"] in newbee_policy_group_dict:
                group["id"] = newbee_policy_group_dict[group["name"]]
    except Exception as oops:
        logger.error(f"Error: {str(oops)}\n{traceback.format_exc()}")


async def write_permission_files(policy_groups, target_form_name, kuali_from, kuali_to):
    """
    Writes permission files for the specified policy groups and target form name.
    The function searches for users, updates user IDs, and creates new groups if necessary.
    It then writes the permission files in JSON format based on the granted permissions.

    Args:
        policy_groups: The list of policy groups.
        target_form_name: The name of the target form.
        kuali_from: The Kuali instance for the source environment.
        kuali_to: The Kuali instance for the target environment.

    Returns:
        None.
    """
    try:
        for group in policy_groups:
            for user in group["identities"]:
                if user.get("type") == "USER":
                    new_user_info = await search_for_user(
                        user["id"], kuali_from, kuali_to
                    )

                    if not new_user_info or not new_user_info.get("id"):
                        logger.error(f"No user info received for user ID: {user['id']}")
                        continue

                    user["id"] = new_user_info["id"]

                    granted_permission_name = user["label"]
                    group_name = NAME_MAPPING.get(group["name"], group["name"]).replace(
                        " ", "_"
                    )
                    filename = f"{granted_permission_name}_as_{group_name}.json".lower()
                    filepath = join(
                        PARENT_DIR,
                        "kuali_skeletons",
                        target_form_name,
                        "permissions",
                        filename,
                    )

                    async with aiofiles.open(filepath, mode="w") as file:
                        await file.write(
                            json.dumps(
                                {"roleId": group["id"], "identity": user},
                                indent=4,
                            )
                        )

                    logger.success(
                        f"{granted_permission_name} granted {group_name} privileges"
                    )

                elif user.get("type") == "GROUP":
                    group_peeps = await extract_groups(user["id"], kuali_from, kuali_to)
                    new_group_name = f"{user['label']}-{target_form_name}"
                    create_group = await create_groups(
                        group_peeps, new_group_name, kuali_from, kuali_to
                    )

                    if not create_group or not create_group.get("id"):
                        logger.error(f"No user info received for user ID: {user['id']}")
                        continue

                    user["id"] = create_group["id"]

                    granted_permission_name = user["label"]
                    group_name = NAME_MAPPING.get(group["name"], group["name"]).replace(
                        " ", "_"
                    )
                    filename = f"{granted_permission_name}_as_{group_name}.json".lower()
                    filepath = join(
                        PARENT_DIR,
                        "kuali_skeletons",
                        target_form_name,
                        "permissions",
                        filename,
                    )

                    async with aiofiles.open(filepath, mode="w") as file:
                        await file.write(
                            json.dumps(
                                {"roleId": group["id"], "identity": user},
                                indent=4,
                            )
                        )

                    logger.success(
                        f"{granted_permission_name} granted {group_name} privileges"
                    )

    except Exception as oops:
        logger.error(f"Error: {str(oops)}\n{traceback.format_exc()}")


async def extract_and_update_permissions(
    originating_form_id,
    target_form_id,
    originating_form_name,
    target_form_name,
    kuali_from,
    kuali_to,
):
    """
    Extracts policy groups from the originating form and the target form using the provided Kuali instances.
    It updates the policy group IDs in the originating form based on the target form's policy group IDs,
    and writes permission files for the policy groups in the originating form.

    Args:
        originating_form_id: The ID of the originating form.
        target_form_id: The ID of the target form.
        originating_form_name: The name of the originating form.
        target_form_name: The name of the target form.
        kuali_from: The Kuali instance for the source environment.
        kuali_to: The Kuali instance for the target environment.

    Returns:
        None.
    """
    try:
        og_policy_groups = await extract_policy_groups(
            originating_form_id, originating_form_name, kuali_from
        )
        newbee_policy_groups = await extract_policy_groups(
            target_form_id, target_form_name, kuali_to
        )
        newbee_policy_group_dict = {
            group["name"]: group["id"]
            for group in newbee_policy_groups
            if "name" in group and "id" in group
        }

        await update_policy_groups(og_policy_groups, newbee_policy_group_dict)
        await write_permission_files(
            og_policy_groups, target_form_name, kuali_from, kuali_to
        )
    except Exception as oops:
        logger.error(
            f"Error in extract_and_update_permissions: {str(oops)}\n{traceback.format_exc()}"
        )
```

</details>

### File: [extract_settings.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/extract_settings.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def extract_originating_form_settings(originating_form_id, kuali_instance):
    """
    Extracts the settings of an originating form from a Kuali instance.

    Args:
        originating_form_id (str): The ID of the originating form.
        kuali_instance: The Kuali instance to extract the form settings from.

    Returns:
        dict: The extracted form settings.

    Raises:
        KeyError: If the form settings cannot be found.
    """

    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Queries.ExtractFormSettings()
    variables = KualiVariables.Queries.ExtractFormSettings(originating_form_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = outcome.get("data", {}).get("app", {}).get("dataset")
        if success_returned is None:
            raise KeyError
        logger.success("Extracted form settings")
        success_returned["appId"] = success_returned.pop("id")
        success_returned.pop("changeTypes", None)
        return success_returned
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")


async def extract_originating_form_data_settings(originating_form_id, kuali_instance):
    """
    Extracts the settings of an originating form from a Kuali instance.

    Args:
        originating_form_id (str): The ID of the originating form.
        kuali_instance: The Kuali instance to extract the form settings from.

    Returns:
        dict: The extracted form settings.

    Raises:
        KeyError: If the form settings cannot be found.

    Example:
        originating_form_id = "12345"
        kuali_instance = KualiInstance()
        settings = await extract_originating_form_settings(originating_form_id, kuali_instance)
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Queries.ExtractDataSettings()
    variables = KualiVariables.Queries.ExtractDataSettings(originating_form_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        form_data_setting = outcome.get("data", {}).get("app", {}).get("dataset")

        if form_data_setting is None:
            raise KeyError
        logger.success("Extracted form data")
        dataset = form_data_setting.get("documentListConfig")
        dataset["appId"] = form_data_setting.pop("id")
        dataset.pop("changeTypes", None)
        logger.success("Document dataset extracted")
        return dataset

    except JSONDecodeError:
        logger.error("Error decoding JSON response")
    except KeyError as ke:
        logger.error(f"KeyError: {ke}")
    except Exception as oops:
        logger.error(f"Unexpected error: {str(oops)}")
```

</details>

### File: [extract_token.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/extract_token.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
import os
import sys
import json
from os.path import dirname, abspath, join
from loguru import logger
import aiofiles

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def extract_target_user_token(user_id, target_form_name, kuali_instance):
    """
    Extracts the target user's API tokens associated with the specified user ID and target form name
    using the provided Kuali instance. It saves the extracted tokens as a JSON file and returns the JSON string.

    Args:
        user_id: The ID of the target user.
        target_form_name: The name of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        A JSON string representing the extracted API tokens.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Queries.ExtractUserApiKeys()
    variables = KualiVariables.Queries.ExtractUserApiKeys(user_id, target_form_name)

    try:
        outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
        success_returned = outcome.get("data", {}).get("user", {}).get("apiKeys", [{}])

        if not success_returned:
            raise KeyError("Key 'apiKeys' not found in the response or it's None")

        json_str = json.dumps(success_returned, indent=4)
        file_path = join(
            parent_dir,
            "kuali_skeletons",
            target_form_name,
            "user_api_keys.json",
        )

        async with aiofiles.open(file_path, mode="w") as f:
            await f.write(json_str)

        logger.success("Tokens extracted")
        return json_str

    except JSONDecodeError:
        logger.error("Error decoding JSON response")
    except KeyError as ke:
        logger.error(f"KeyError: {ke}")
    except Exception as oops:
        logger.error(f"Unexpected error: {str(oops)}")
```

</details>

### File: [extract_user.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/extract_user.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def extract_target_user_info(app_owner, kuali_instance):
    """
    Extracts the target user's information associated with the specified app owner
    using the provided Kuali instance.

    Args:
        app_owner: The app owner.
        kuali_instance: The Kuali instance.

    Returns:
        A dictionary containing the extracted target user's information,
        or None if the information is not found.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Queries.GetUser()
    variables = KualiVariables.Queries.GetUser(app_owner)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = (
            outcome.get("data", {})
            .get("usersConnection", {})
            .get("edges", [])[0]
            .get("node")
        )

        if success_returned is None:
            raise KeyError
        user_id = success_returned.get("id", None)

        return success_returned
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
```

</details>

### File: [extract_workflow.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/extract_workflow.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


def find_ids_and_externals(obj, result=None):
    """
    Find integration IDs and external IDs within a nested dictionary or list.

    Args:
        obj: The nested dictionary or list to search for IDs and externals.
        result: A list to store the found integration IDs and external IDs.

    Returns:
        A list of dictionaries containing the integration IDs and external IDs found in the input object.

    Examples:
        >>> data = {
        ...     "id": "123456789012345678901234",
        ...     "externals": ["external:1", "external:2"],
        ...     "nested": {
        ...         "id": "987654321098765432109876",
        ...         "externals": ["external:3", "external:4"]
        ...     }
        ... }
        >>> find_ids_and_externals(data)
        [{'integrationId': '123456789012345678901234'}, {'integrationId': '1'}, {'integrationId': '2'}, {'integrationId': '987654321098765432109876'}, {'integrationId': '3'}, {'integrationId': '4'}]
    """
    if result is None:
        result = []
    if isinstance(obj, dict):
        for k, v in obj.items():
            if k == "id" and isinstance(v, str) and len(v) == 24:
                if v not in [item["integrationId"] for item in result]:
                    result.append({"integrationId": v})
            elif k == "externals" and isinstance(v, list):
                for i in v:
                    if isinstance(i, str):
                        external_id = i.split(":")[1]
                        if external_id not in [
                            item["integrationId"] for item in result
                        ]:
                            result.append({"integrationId": external_id})
            elif isinstance(v, (dict, list)):
                find_ids_and_externals(v, result)
    elif isinstance(obj, list):
        for item in obj:
            find_ids_and_externals(item, result)
    return result


def append_to_json_file(filename, data):
    """
    Append data to a JSON file.

    Args:
        filename: The path to the JSON file.
        data: The data to append to the file.

    Returns:
        None

    Raises:
        FileNotFoundError: If the specified file does not exist.
        JSONDecodeError: If the file contains invalid JSON.

    Examples:
        >>> data = [{"name": "John", "age": 30}, {"name": "Jane", "age": 25}]
        >>> append_to_json_file("data.json", data)
    """
    try:
        with open(filename, "r") as f:
            existing_data = json.load(f)
    except (FileNotFoundError, JSONDecodeError):
        existing_data = []

    for item in data:
        if item not in existing_data:
            existing_data.append(item)

    with open(filename, "w") as f:
        json.dump(existing_data, f, indent=4)


async def extract_originating_workflow(
    originating_form_id,
    target_form_id,
    originating_form_name,
    target_form_name,
    kuali_instance,
):
    """
    Extracts the originating workflow from a Kuali instance and saves it as JSON files.

    Args:
        originating_form_id: The ID of the originating form.
        target_form_id: The ID of the target form.
        originating_form_name: The name of the originating form.
        target_form_name: The name of the target form.
        kuali_instance: The instance of the Kuali API.

    Returns:
        None

    Raises:
        KeyError: If the workflow extraction fails.

    Examples:
        >>> originating_form_id = "1234567890"
        >>> target_form_id = "0987654321"
        >>> originating_form_name = "OriginatingForm"
        >>> target_form_name = "TargetForm"
        >>> kuali_instance = KualiInstance()
        >>> await extract_originating_workflow(
        ...     originating_form_id,
        ...     target_form_id,
        ...     originating_form_name,
        ...     target_form_name,
        ...     kuali_instance,
        ... )
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Queries.ExtractWorkflows()
    variables = KualiVariables.Queries.ExtractWorkflows(originating_form_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = (
            outcome.get("data", {})
            .get("app", {})
            .get("dataset", {})
            .get("workflow", {})
            .get("steps", None)
        )

        if success_returned is not None:
            success_returned = {
                "appId": target_form_id,
                "args": {"appId": target_form_id, "steps": success_returned},
            }
            json_str = json.dumps(success_returned, indent=4)
            with open(
                join(
                    parent_dir,
                    "kuali_skeletons",
                    target_form_name,
                    "workflow.json",
                ),
                "w",
            ) as f:
                f.write(json_str)
            logger.success("data.json file created")

            id_and_external_dict = find_ids_and_externals(success_returned)

            filename = join(
                parent_dir,
                "kuali_skeletons",
                target_form_name,
                "integrations.json",
            )

            append_to_json_file(filename, id_and_external_dict)

            logger.success(f"{filename} file updated")
        else:
            raise KeyError
    except Exception as oops:
        logger.error(f"Error when trying to extract workflow: {str(oops)}")
```

</details>

<details><summary><b>find_ids_and_externals - Docstring</b></summary>

```txt
Find integration IDs and external IDs within a nested dictionary or list.

Args:
    obj: The nested dictionary or list to search for IDs and externals.
    result: A list to store the found integration IDs and external IDs.

Returns:
    A list of dictionaries containing the integration IDs and external IDs found in the input object.

Examples:
    >>> data = {
    ...     "id": "123456789012345678901234",
    ...     "externals": ["external:1", "external:2"],
    ...     "nested": {
    ...         "id": "987654321098765432109876",
    ...         "externals": ["external:3", "external:4"]
    ...     }
    ... }
    >>> find_ids_and_externals(data)
    [{'integrationId': '123456789012345678901234'}, {'integrationId': '1'}, {'integrationId': '2'}, {'integrationId': '987654321098765432109876'}, {'integrationId': '3'}, {'integrationId': '4'}]
```

</details>
<details><summary><b>find_ids_and_externals - Code</b></summary>

```python
    if result is None:
        result = []
    if isinstance(obj, dict):
        for k, v in obj.items():
            if k == "id" and isinstance(v, str) and len(v) == 24:
                if v not in [item["integrationId"] for item in result]:
                    result.append({"integrationId": v})
            elif k == "externals" and isinstance(v, list):
                for i in v:
                    if isinstance(i, str):
                        external_id = i.split(":")[1]
                        if external_id not in [
                            item["integrationId"] for item in result
                        ]:
                            result.append({"integrationId": external_id})
            elif isinstance(v, (dict, list)):
                find_ids_and_externals(v, result)
    elif isinstance(obj, list):
        for item in obj:
            find_ids_and_externals(item, result)
    return result
```

</details>

<details><summary><b>append_to_json_file - Docstring</b></summary>

```txt
Append data to a JSON file.

Args:
    filename: The path to the JSON file.
    data: The data to append to the file.

Returns:
    None

Raises:
    FileNotFoundError: If the specified file does not exist.
    JSONDecodeError: If the file contains invalid JSON.

Examples:
    >>> data = [{"name": "John", "age": 30}, {"name": "Jane", "age": 25}]
    >>> append_to_json_file("data.json", data)
```

</details>
<details><summary><b>append_to_json_file - Code</b></summary>

```python
    try:
        with open(filename, "r") as f:
            existing_data = json.load(f)
    except (FileNotFoundError, JSONDecodeError):
        existing_data = []

    for item in data:
        if item not in existing_data:
            existing_data.append(item)

    with open(filename, "w") as f:
        json.dump(existing_data, f, indent=4)
```

</details>

### File: [graphql_calls.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/graphql_calls.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard library imports
import sys
from os import getenv
from os.path import dirname, abspath, join
from pathlib import Path
from json import JSONDecodeError
from typing import Dict, Any

# Third-party imports
import orjson
from httpx import AsyncClient
from httpx import Timeout
from httpx import Limits
from httpx import RequestError
from httpx import ReadTimeout
from httpx import HTTPStatusError
from fastapi.exceptions import HTTPException
from dotenv import load_dotenv
from loguru import logger
from asyncio import Semaphore
import uvloop

load_dotenv(verbose=True)
uvloop.install()

BASE_URL = "https://pof.trinity.duke.edu/feed"
SEMAPHORE = Semaphore(10)
timeouts = Timeout(45, connect=25, read=45, write=45)
CLIENT = AsyncClient(
    http2=True,
    timeout=timeouts,
    trust_env=True,
    default_encoding="utf-8",
    limits=Limits(max_keepalive_connections=5, max_connections=10),
)


instance = getenv("KUALI_INSTANCE_PRD")

current_dir = dirname(abspath(__file__))
parent_dir = dirname(dirname(abspath(__file__)))
sys.path.append(current_dir)

from circuit_breaker import CircuitBreaker


circuit_breaker = CircuitBreaker()


def chunkify(lst, n):
    """Let give Kuali some breathing room and yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


async def fetch_json_from_url(url: str) -> Dict[str, Any]:
    """
    Fetches JSON data from a given URL.

    Args:
        url (str): The URL to fetch JSON data from.

    Returns:
        Dict[str, Any]: The JSON data as a dictionary.

    Raises:
        HTTPException: If the circuit breaker is open and execution is not allowed.

    Examples:
        >>> data = await fetch_json_from_url(url)
    """
    if not circuit_breaker.can_execute():
        raise HTTPException(
            status_code=503,
            detail="Service temporarily unavailable due to multiple failures",
        )
    async with SEMAPHORE:
        try:
            response = await CLIENT.get(url)
            response.raise_for_status()
            return response.json()
        except (HTTPStatusError, JSONDecodeError) as dang:
            circuit_breaker.record_failure()
            logger.error(f"Error occurred while fetching from {url}: {dang}")
            return {}


class Kuali:
    """
    Represents a Kuali API client.

    Args:
        instance: The Kuali instance.
        auth_token: The authentication token.

    Methods:
        url(): Get the URL for the Kuali API.
        headers(user_auth_token=None): Get the headers for API requests.
        FetchResponse(url, headers, query, variables, retries=3, timeout=30.0): Fetch a response from the Kuali API.

    Examples:
        >>> instance = "example"
        >>> auth_token = "1234567890"
        >>> kuali_client = Kuali(instance, auth_token)
        >>> api_url = kuali_client.url()
        >>> headers = kuali_client.headers()
        >>> response = await Kuali.FetchResponse(api_url, headers, query, variables)
    """

    def __init__(self, instance, auth_token):
        """
        Represents a GraphQL client.

        Args:
            instance (str): The instance URL of the GraphQL server.
            auth_token (str): The authentication token for accessing the GraphQL server.

        Attributes:
            instance (str): The instance URL of the GraphQL server.
            auth_token (str): The authentication token for accessing the GraphQL server.

        Examples:
            >>> client = GraphQLClient(instance="https://example.com/graphql", auth_token="my_token")
        """
        self.instance = instance
        self.auth_token = auth_token

    def url(self):
        """
        Returns the URL for the GraphQL endpoint.

        Returns:
            str: The URL for the GraphQL endpoint.

        Examples:
            >>> endpoint_url = url()
        """
        return f"https://{self.instance}.kualibuild.com/app/api/v0/graphql"

    def headers(self, user_auth_token=None):
        """
        Returns the headers for making a GraphQL request.

        Args:
            user_auth_token (str, optional): The user-specific authentication token. Defaults to None.

        Returns:
            Dict[str, str]: The headers for the GraphQL request.

        Examples:
            >>> request_headers = headers(user_auth_token="my_token")
        """
        token = self.auth_token if user_auth_token is None else user_auth_token
        return {
            "Accept-Encoding": "gzip, deflate",
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Connection": "keep-alive",
            "DNT": "1",
            "Origin": f"https://{self.instance}.kualibuild.com",
            "Authorization": f"{token}",
        }

    @staticmethod
    async def FetchResponse(url, headers, query, variables, retries=3, timeout=30.0):
        """
        Fetches a response from a GraphQL endpoint.

        Args:
            url (str): The URL of the GraphQL endpoint.
            headers (Dict[str, str]): The headers to include in the request.
            query (str): The GraphQL query.
            variables (Dict[str, Any]): The variables to include in the request.
            retries (int): The number of retries in case of failure. Defaults to 3.
            timeout (float): The timeout value for the request in seconds. Defaults to 30.0.

        Returns:
            Dict[str, Any]: The JSON response as a dictionary.

        Raises:
            JSONDecodeError: If there is an error decoding the JSON response.
            ReadTimeout: If the request times out after all retries.

        Examples:
            >>> response = await FetchResponse(url, headers, query, variables)
        """
        for retry in range(retries):
            try:
                response = await CLIENT.post(
                    url,
                    headers=headers,
                    json={"query": query, "variables": variables},
                )
                try:
                    return response.json()
                except JSONDecodeError:
                    logger.error(f"Error decoding JSON:\n{response.text}")
                    raise
            except ReadTimeout:
                if retry == retries - 1:
                    raise
                else:
                    logger.warning(
                        f"Request timed out. Retrying ({retry + 1}/{retries})..."
                    )


class KualiGraphQL:
    """
    Represents a collection of GraphQL queries and mutations for the Kuali API.

    Attributes:
        gql_path: The path to the GraphQL files.

    Classes:
        Mutations: Contains static methods for retrieving mutation queries.
        Queries: Contains static methods for retrieving query queries.

    Examples:
        >>> mutation = KualiGraphQL.Mutations.AddGroupRoleUser()
        >>> query = KualiGraphQL.Queries.GetUser()
    """

    gql_path = join(parent_dir, "GraphQL")

    class Mutations:
        """
        Contains static methods for retrieving GraphQL mutation queries related to various operations.

        Methods:
            AddGroupRoleUser(): Retrieves the AddGroupRoleUser mutation query.
            CreateGroup(): Retrieves the CreateGroup mutation query.
            CreateUser(): Retrieves the CreateUser mutation query.
            CreateIntegration(): Retrieves the CreateIntegration mutation query.
            CreateToken(): Retrieves the CreateToken mutation query.
            CreateApp(): Retrieves the CreateApp mutation query.
            UpdateFormSettings(): Retrieves the UpdateFormSettings mutation query.
            UpdateDataSettings(): Retrieves the UpdateDataSettings mutation query.
            RevokeToken(): Retrieves the RevokeToken mutation query.
            PublishForm(): Retrieves the PublishForm mutation query.
            UpdateForm(): Retrieves the UpdateForm mutation query.
            UpdateWorkflow(): Retrieves the UpdateWorkflow mutation query.
            UpdateIntegration(): Retrieves the UpdateIntegration mutation query.
            UpdatePermissions(): Retrieves the UpdatePermissions mutation query.
            CreateFilters(): Retrieves the CreateFilters mutation query.
            ShareFilters(): Retrieves the ShareFilters mutation query.
            UpdateAppFavorite(): Retrieves the UpdateAppFavorite mutation query.
            CreatePolicyGroup(): Retrieves the CreatePolicyGroup mutation query.
        """

        @staticmethod
        def AddGroupRoleUser():
            """
            Retrieves the AddGroupRoleUser GraphQL query.

            Returns:
                The AddGroupRoleUser query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "AddGroupRoleUser.gql"), "r") as file:
                return file.read()

        @staticmethod
        def CreateGroup():
            """
            Retrieves the CreateGroup GraphQL mutation.

            Returns:
                The CreateGroup mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateGroup.gql"), "r") as file:
                return file.read()

        @staticmethod
        def CreateUser():
            """
            Retrieves the CreateUser GraphQL mutation.

            Returns:
                The CreateUser mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateUser.gql"), "r") as file:
                return file.read()

        @staticmethod
        def CreateIntegration():
            """
            Retrieves the CreateIntegration GraphQL mutation.

            Returns:
                The CreateIntegration mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "CreateIntegration.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def CreateToken():
            """
            Retrieves the CreateToken GraphQL mutation.

            Returns:
                The CreateToken mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateToken.gql"), "r") as file:
                return file.read()

        @staticmethod
        def CreateApp():
            """
            Retrieves the CreateApp GraphQL mutation.

            Returns:
                The CreateApp mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateApp.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateFormSettings():
            """
            Retrieves the UpdateFormSettings GraphQL mutation.

            Returns:
                The UpdateFormSettings mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdateFormSettings.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def UpdateDataSettings():
            """
            Retrieves the UpdateDataSettings GraphQL mutation.

            Returns:
                The UpdateDataSettings mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdateDataSettings.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def RevokeToken():
            """
            Retrieves the RevokeToken GraphQL mutation.

            Returns:
                The RevokeToken mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "RevokeToken.gql"), "r") as file:
                return file.read()

        @staticmethod
        def PublishForm():
            """
            Retrieves the PublishForm GraphQL mutation.

            Returns:
                The PublishForm mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "PublishForm.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateForm():
            """
            Retrieves the UpdateForm GraphQL mutation.

            Returns:
                The UpdateForm mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "UpdateForm.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateWorkflow():
            """
            Retrieves the UpdateWorkflow GraphQL mutation.

            Returns:
                The UpdateWorkflow mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "UpdateWorkflow.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateIntegration():
            """
            Retrieves the UpdateIntegration GraphQL mutation.

            Returns:
                The UpdateIntegration mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdateIntegration.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def UpdatePermissions():
            """
            Retrieves the UpdatePermissions GraphQL mutation.

            Returns:
                The UpdatePermissions mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdatePermissions.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def CreateFilters():
            """
            Retrieves the CreateFilters GraphQL mutation.

            Returns:
                The CreateFilters mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateFilters.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ShareFilters():
            """
            Retrieves the ShareFilters GraphQL mutation.

            Returns:
                The ShareFilters mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "ShareFilters.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateAppFavorite():
            """
            Retrieves the UpdateAppFavorite GraphQL mutation.

            Returns:
                The UpdateAppFavorite mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdateAppFavorite.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def CreatePolicyGroup():
            """
            Retrieves the CreatePolicyGroup GraphQL mutation.

            Returns:
                The CreatePolicyGroup mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "CreatePolicyGroup.gql"), "r"
            ) as file:
                return file.read()

    class Queries:
        """
        Contains static methods for retrieving GraphQL queries related to various operations.

        Methods:
            SearchSpaces(): Retrieves the SearchSpaces query.
            GetGroupByKualiID(): Retrieves the GetGroupByKualiID query.
            GetUserByKualiID(): Retrieves the GetUserByKualiID query.
            GetUser(): Retrieves the GetUser query.
            ExtractUserApiKeys(): Retrieves the ExtractUserApiKeys query.
            ExtractIntegrationBodies(): Retrieves the ExtractIntegrationBodies query.
            ExtractForm(): Retrieves the ExtractForm query.
            ExtractFormSettings(): Retrieves the ExtractFormSettings query.
            ExtractIntegration(): Retrieves the ExtractIntegration query.
            ExtractWorkflows(): Retrieves the ExtractWorkflows query.
            SpaceIDs(): Retrieves the SpaceIDs query.
            ExtractApps(): Retrieves the ExtractApps query.
            ExtractPermissions(): Retrieves the ExtractPermissions query.
            ExtractDataSettings(): Retrieves the ExtractDataSettings query.
        """

        @staticmethod
        def SearchSpaces():
            """
            Retrieves the SearchSpaces GraphQL query.

            Returns:
                The SearchSpaces query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "SearchSpaces.gql"), "r") as file:
                return file.read()

        @staticmethod
        def GetGroupByKualiID():
            """
            Retrieves the GetGroupByKualiID GraphQL query.

            Returns:
                The GetGroupByKualiID query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "GetGroupByKualiID.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def GetUserByKualiID():
            """
            Retrieves the GetUserByKualiID GraphQL query.

            Returns:
                The GetUserByKualiID query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "GetUserByKualiID.gql"), "r") as file:
                return file.read()

        @staticmethod
        def GetUser():
            """
            Retrieves the GetUser GraphQL query.

            Returns:
                The GetUser query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "GetUser.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ExtractUserApiKeys():
            """
            Retrieves the ExtractUserApiKeys GraphQL query.

            Returns:
                The ExtractUserApiKeys query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractUserApiKeys.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def ExtractIntegrationBodies():
            """
            Retrieves the ExtractIntegrationBodies GraphQL query.

            Returns:
                The ExtractIntegrationBodies query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractIntegrationBodies.gql"),
                "r",
            ) as file:
                return file.read()

        @staticmethod
        def ExtractForm():
            """
            Retrieves the ExtractForm GraphQL query.

            Returns:
                The ExtractForm query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "ExtractForm.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ExtractFormSettings():
            """
            Retrieves the ExtractFormSettings GraphQL query.

            Returns:
                The ExtractFormSettings query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractFormSettings.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def ExtractIntegration():
            """
            Retrieves the ExtractIntegration GraphQL query.

            Returns:
                The ExtractIntegration query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractIntegration.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def ExtractWorkflows():
            """
            Retrieves the ExtractWorkflows GraphQL query.

            Returns:
                The ExtractWorkflows query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "ExtractWorkflows.gql"), "r") as file:
                return file.read()

        @staticmethod
        def SpaceIDs():
            """
            Retrieves the SpaceIDs GraphQL query.

            Returns:
                The SpaceIDs query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "SpaceIDs.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ExtractApps():
            """
            Retrieves the ExtractApps GraphQL query.

            Returns:
                The ExtractApps query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "ExtractApps.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ExtractPermissions():
            """
            Retrieves the ExtractPermissions GraphQL query.

            Returns:
                The ExtractPermissions query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractPermissions.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def ExtractDataSettings():
            """
            Retrieves the ExtractDataSettings GraphQL query.

            Returns:
                The ExtractDataSettings query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractDataSettings.gql"), "r"
            ) as file:
                return file.read()
```

</details>

<details><summary><b>chunkify - Docstring</b></summary>

```txt
Let give Kuali some breathing room and yield successive n-sized chunks from lst.
```

</details>
<details><summary><b>chunkify - Code</b></summary>

```python

```

</details>

<details><summary><b>Kuali - Docstring</b></summary>

```txt
Represents a Kuali API client.

Args:
    instance: The Kuali instance.
    auth_token: The authentication token.

Methods:
    url(): Get the URL for the Kuali API.
    headers(user_auth_token=None): Get the headers for API requests.
    FetchResponse(url, headers, query, variables, retries=3, timeout=30.0): Fetch a response from the Kuali API.

Examples:
    >>> instance = "example"
    >>> auth_token = "1234567890"
    >>> kuali_client = Kuali(instance, auth_token)
    >>> api_url = kuali_client.url()
    >>> headers = kuali_client.headers()
    >>> response = await Kuali.FetchResponse(api_url, headers, query, variables)
```

</details>
<details><summary><b>Kuali - Code</b></summary>

```python

    def __init__(self, instance, auth_token):
        """
        Represents a GraphQL client.

        Args:
            instance (str): The instance URL of the GraphQL server.
            auth_token (str): The authentication token for accessing the GraphQL server.

        Attributes:
            instance (str): The instance URL of the GraphQL server.
            auth_token (str): The authentication token for accessing the GraphQL server.

        Examples:
            >>> client = GraphQLClient(instance="https://example.com/graphql", auth_token="my_token")
        """
        self.instance = instance
        self.auth_token = auth_token

    def url(self):
        """
        Returns the URL for the GraphQL endpoint.

        Returns:
            str: The URL for the GraphQL endpoint.

        Examples:
            >>> endpoint_url = url()
        """
        return f"https://{self.instance}.kualibuild.com/app/api/v0/graphql"

    def headers(self, user_auth_token=None):
        """
        Returns the headers for making a GraphQL request.

        Args:
            user_auth_token (str, optional): The user-specific authentication token. Defaults to None.

        Returns:
            Dict[str, str]: The headers for the GraphQL request.

        Examples:
            >>> request_headers = headers(user_auth_token="my_token")
        """
        token = self.auth_token if user_auth_token is None else user_auth_token
        return {
            "Accept-Encoding": "gzip, deflate",
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Connection": "keep-alive",
            "DNT": "1",
            "Origin": f"https://{self.instance}.kualibuild.com",
            "Authorization": f"{token}",
        }

    @staticmethod
    async def FetchResponse(url, headers, query, variables, retries=3, timeout=30.0):
        """
        Fetches a response from a GraphQL endpoint.

        Args:
            url (str): The URL of the GraphQL endpoint.
            headers (Dict[str, str]): The headers to include in the request.
            query (str): The GraphQL query.
            variables (Dict[str, Any]): The variables to include in the request.
            retries (int): The number of retries in case of failure. Defaults to 3.
            timeout (float): The timeout value for the request in seconds. Defaults to 30.0.

        Returns:
            Dict[str, Any]: The JSON response as a dictionary.

        Raises:
            JSONDecodeError: If there is an error decoding the JSON response.
            ReadTimeout: If the request times out after all retries.

        Examples:
            >>> response = await FetchResponse(url, headers, query, variables)
        """
        for retry in range(retries):
            try:
                response = await CLIENT.post(
                    url,
                    headers=headers,
                    json={"query": query, "variables": variables},
                )
                try:
                    return response.json()
                except JSONDecodeError:
                    logger.error(f"Error decoding JSON:\n{response.text}")
                    raise
            except ReadTimeout:
                if retry == retries - 1:
                    raise
                else:
                    logger.warning(
                        f"Request timed out. Retrying ({retry + 1}/{retries})..."
                    )
```

</details>

<details><summary><b>KualiGraphQL - Docstring</b></summary>

```txt
Represents a collection of GraphQL queries and mutations for the Kuali API.

Attributes:
    gql_path: The path to the GraphQL files.

Classes:
    Mutations: Contains static methods for retrieving mutation queries.
    Queries: Contains static methods for retrieving query queries.

Examples:
    >>> mutation = KualiGraphQL.Mutations.AddGroupRoleUser()
    >>> query = KualiGraphQL.Queries.GetUser()
```

</details>
<details><summary><b>KualiGraphQL - Code</b></summary>

```python

    gql_path = join(parent_dir, "GraphQL")

    class Mutations:
        """
        Contains static methods for retrieving GraphQL mutation queries related to various operations.

        Methods:
            AddGroupRoleUser(): Retrieves the AddGroupRoleUser mutation query.
            CreateGroup(): Retrieves the CreateGroup mutation query.
            CreateUser(): Retrieves the CreateUser mutation query.
            CreateIntegration(): Retrieves the CreateIntegration mutation query.
            CreateToken(): Retrieves the CreateToken mutation query.
            CreateApp(): Retrieves the CreateApp mutation query.
            UpdateFormSettings(): Retrieves the UpdateFormSettings mutation query.
            UpdateDataSettings(): Retrieves the UpdateDataSettings mutation query.
            RevokeToken(): Retrieves the RevokeToken mutation query.
            PublishForm(): Retrieves the PublishForm mutation query.
            UpdateForm(): Retrieves the UpdateForm mutation query.
            UpdateWorkflow(): Retrieves the UpdateWorkflow mutation query.
            UpdateIntegration(): Retrieves the UpdateIntegration mutation query.
            UpdatePermissions(): Retrieves the UpdatePermissions mutation query.
            CreateFilters(): Retrieves the CreateFilters mutation query.
            ShareFilters(): Retrieves the ShareFilters mutation query.
            UpdateAppFavorite(): Retrieves the UpdateAppFavorite mutation query.
            CreatePolicyGroup(): Retrieves the CreatePolicyGroup mutation query.
        """

        @staticmethod
        def AddGroupRoleUser():
            """
            Retrieves the AddGroupRoleUser GraphQL query.

            Returns:
                The AddGroupRoleUser query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "AddGroupRoleUser.gql"), "r") as file:
                return file.read()

        @staticmethod
        def CreateGroup():
            """
            Retrieves the CreateGroup GraphQL mutation.

            Returns:
                The CreateGroup mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateGroup.gql"), "r") as file:
                return file.read()

        @staticmethod
        def CreateUser():
            """
            Retrieves the CreateUser GraphQL mutation.

            Returns:
                The CreateUser mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateUser.gql"), "r") as file:
                return file.read()

        @staticmethod
        def CreateIntegration():
            """
            Retrieves the CreateIntegration GraphQL mutation.

            Returns:
                The CreateIntegration mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "CreateIntegration.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def CreateToken():
            """
            Retrieves the CreateToken GraphQL mutation.

            Returns:
                The CreateToken mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateToken.gql"), "r") as file:
                return file.read()

        @staticmethod
        def CreateApp():
            """
            Retrieves the CreateApp GraphQL mutation.

            Returns:
                The CreateApp mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateApp.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateFormSettings():
            """
            Retrieves the UpdateFormSettings GraphQL mutation.

            Returns:
                The UpdateFormSettings mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdateFormSettings.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def UpdateDataSettings():
            """
            Retrieves the UpdateDataSettings GraphQL mutation.

            Returns:
                The UpdateDataSettings mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdateDataSettings.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def RevokeToken():
            """
            Retrieves the RevokeToken GraphQL mutation.

            Returns:
                The RevokeToken mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "RevokeToken.gql"), "r") as file:
                return file.read()

        @staticmethod
        def PublishForm():
            """
            Retrieves the PublishForm GraphQL mutation.

            Returns:
                The PublishForm mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "PublishForm.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateForm():
            """
            Retrieves the UpdateForm GraphQL mutation.

            Returns:
                The UpdateForm mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "UpdateForm.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateWorkflow():
            """
            Retrieves the UpdateWorkflow GraphQL mutation.

            Returns:
                The UpdateWorkflow mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "UpdateWorkflow.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateIntegration():
            """
            Retrieves the UpdateIntegration GraphQL mutation.

            Returns:
                The UpdateIntegration mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdateIntegration.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def UpdatePermissions():
            """
            Retrieves the UpdatePermissions GraphQL mutation.

            Returns:
                The UpdatePermissions mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdatePermissions.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def CreateFilters():
            """
            Retrieves the CreateFilters GraphQL mutation.

            Returns:
                The CreateFilters mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateFilters.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ShareFilters():
            """
            Retrieves the ShareFilters GraphQL mutation.

            Returns:
                The ShareFilters mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "ShareFilters.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateAppFavorite():
            """
            Retrieves the UpdateAppFavorite GraphQL mutation.

            Returns:
                The UpdateAppFavorite mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdateAppFavorite.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def CreatePolicyGroup():
            """
            Retrieves the CreatePolicyGroup GraphQL mutation.

            Returns:
                The CreatePolicyGroup mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "CreatePolicyGroup.gql"), "r"
            ) as file:
                return file.read()

    class Queries:
        """
        Contains static methods for retrieving GraphQL queries related to various operations.

        Methods:
            SearchSpaces(): Retrieves the SearchSpaces query.
            GetGroupByKualiID(): Retrieves the GetGroupByKualiID query.
            GetUserByKualiID(): Retrieves the GetUserByKualiID query.
            GetUser(): Retrieves the GetUser query.
            ExtractUserApiKeys(): Retrieves the ExtractUserApiKeys query.
            ExtractIntegrationBodies(): Retrieves the ExtractIntegrationBodies query.
            ExtractForm(): Retrieves the ExtractForm query.
            ExtractFormSettings(): Retrieves the ExtractFormSettings query.
            ExtractIntegration(): Retrieves the ExtractIntegration query.
            ExtractWorkflows(): Retrieves the ExtractWorkflows query.
            SpaceIDs(): Retrieves the SpaceIDs query.
            ExtractApps(): Retrieves the ExtractApps query.
            ExtractPermissions(): Retrieves the ExtractPermissions query.
            ExtractDataSettings(): Retrieves the ExtractDataSettings query.
        """

        @staticmethod
        def SearchSpaces():
            """
            Retrieves the SearchSpaces GraphQL query.

            Returns:
                The SearchSpaces query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "SearchSpaces.gql"), "r") as file:
                return file.read()

        @staticmethod
        def GetGroupByKualiID():
            """
            Retrieves the GetGroupByKualiID GraphQL query.

            Returns:
                The GetGroupByKualiID query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "GetGroupByKualiID.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def GetUserByKualiID():
            """
            Retrieves the GetUserByKualiID GraphQL query.

            Returns:
                The GetUserByKualiID query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "GetUserByKualiID.gql"), "r") as file:
                return file.read()

        @staticmethod
        def GetUser():
            """
            Retrieves the GetUser GraphQL query.

            Returns:
                The GetUser query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "GetUser.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ExtractUserApiKeys():
            """
            Retrieves the ExtractUserApiKeys GraphQL query.

            Returns:
                The ExtractUserApiKeys query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractUserApiKeys.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def ExtractIntegrationBodies():
            """
            Retrieves the ExtractIntegrationBodies GraphQL query.

            Returns:
                The ExtractIntegrationBodies query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractIntegrationBodies.gql"),
                "r",
            ) as file:
                return file.read()

        @staticmethod
        def ExtractForm():
            """
            Retrieves the ExtractForm GraphQL query.

            Returns:
                The ExtractForm query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "ExtractForm.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ExtractFormSettings():
            """
            Retrieves the ExtractFormSettings GraphQL query.

            Returns:
                The ExtractFormSettings query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractFormSettings.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def ExtractIntegration():
            """
            Retrieves the ExtractIntegration GraphQL query.

            Returns:
                The ExtractIntegration query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractIntegration.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def ExtractWorkflows():
            """
            Retrieves the ExtractWorkflows GraphQL query.

            Returns:
                The ExtractWorkflows query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "ExtractWorkflows.gql"), "r") as file:
                return file.read()

        @staticmethod
        def SpaceIDs():
            """
            Retrieves the SpaceIDs GraphQL query.

            Returns:
                The SpaceIDs query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "SpaceIDs.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ExtractApps():
            """
            Retrieves the ExtractApps GraphQL query.

            Returns:
                The ExtractApps query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "ExtractApps.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ExtractPermissions():
            """
            Retrieves the ExtractPermissions GraphQL query.

            Returns:
                The ExtractPermissions query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractPermissions.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def ExtractDataSettings():
            """
            Retrieves the ExtractDataSettings GraphQL query.

            Returns:
                The ExtractDataSettings query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractDataSettings.gql"), "r"
            ) as file:
                return file.read()
```

</details>

<details><summary><b>__init__ - Docstring</b></summary>

```txt
Represents a GraphQL client.

Args:
    instance (str): The instance URL of the GraphQL server.
    auth_token (str): The authentication token for accessing the GraphQL server.

Attributes:
    instance (str): The instance URL of the GraphQL server.
    auth_token (str): The authentication token for accessing the GraphQL server.

Examples:
    >>> client = GraphQLClient(instance="https://example.com/graphql", auth_token="my_token")
```

</details>
<details><summary><b>__init__ - Code</b></summary>

```python
        self.instance = instance
        self.auth_token = auth_token
```

</details>

<details><summary><b>url - Docstring</b></summary>

```txt
Returns the URL for the GraphQL endpoint.

Returns:
    str: The URL for the GraphQL endpoint.

Examples:
    >>> endpoint_url = url()
```

</details>
<details><summary><b>url - Code</b></summary>

```python
        return f"https://{self.instance}.kualibuild.com/app/api/v0/graphql"
```

</details>

<details><summary><b>headers - Docstring</b></summary>

```txt
Returns the headers for making a GraphQL request.

Args:
    user_auth_token (str, optional): The user-specific authentication token. Defaults to None.

Returns:
    Dict[str, str]: The headers for the GraphQL request.

Examples:
    >>> request_headers = headers(user_auth_token="my_token")
```

</details>
<details><summary><b>headers - Code</b></summary>

```python
        token = self.auth_token if user_auth_token is None else user_auth_token
        return {
            "Accept-Encoding": "gzip, deflate",
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Connection": "keep-alive",
            "DNT": "1",
            "Origin": f"https://{self.instance}.kualibuild.com",
            "Authorization": f"{token}",
        }
```

</details>

<details><summary><b>Mutations - Docstring</b></summary>

```txt
Contains static methods for retrieving GraphQL mutation queries related to various operations.

Methods:
    AddGroupRoleUser(): Retrieves the AddGroupRoleUser mutation query.
    CreateGroup(): Retrieves the CreateGroup mutation query.
    CreateUser(): Retrieves the CreateUser mutation query.
    CreateIntegration(): Retrieves the CreateIntegration mutation query.
    CreateToken(): Retrieves the CreateToken mutation query.
    CreateApp(): Retrieves the CreateApp mutation query.
    UpdateFormSettings(): Retrieves the UpdateFormSettings mutation query.
    UpdateDataSettings(): Retrieves the UpdateDataSettings mutation query.
    RevokeToken(): Retrieves the RevokeToken mutation query.
    PublishForm(): Retrieves the PublishForm mutation query.
    UpdateForm(): Retrieves the UpdateForm mutation query.
    UpdateWorkflow(): Retrieves the UpdateWorkflow mutation query.
    UpdateIntegration(): Retrieves the UpdateIntegration mutation query.
    UpdatePermissions(): Retrieves the UpdatePermissions mutation query.
    CreateFilters(): Retrieves the CreateFilters mutation query.
    ShareFilters(): Retrieves the ShareFilters mutation query.
    UpdateAppFavorite(): Retrieves the UpdateAppFavorite mutation query.
    CreatePolicyGroup(): Retrieves the CreatePolicyGroup mutation query.
```

</details>
<details><summary><b>Mutations - Code</b></summary>

```python

        @staticmethod
        def AddGroupRoleUser():
            """
            Retrieves the AddGroupRoleUser GraphQL query.

            Returns:
                The AddGroupRoleUser query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "AddGroupRoleUser.gql"), "r") as file:
                return file.read()

        @staticmethod
        def CreateGroup():
            """
            Retrieves the CreateGroup GraphQL mutation.

            Returns:
                The CreateGroup mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateGroup.gql"), "r") as file:
                return file.read()

        @staticmethod
        def CreateUser():
            """
            Retrieves the CreateUser GraphQL mutation.

            Returns:
                The CreateUser mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateUser.gql"), "r") as file:
                return file.read()

        @staticmethod
        def CreateIntegration():
            """
            Retrieves the CreateIntegration GraphQL mutation.

            Returns:
                The CreateIntegration mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "CreateIntegration.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def CreateToken():
            """
            Retrieves the CreateToken GraphQL mutation.

            Returns:
                The CreateToken mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateToken.gql"), "r") as file:
                return file.read()

        @staticmethod
        def CreateApp():
            """
            Retrieves the CreateApp GraphQL mutation.

            Returns:
                The CreateApp mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateApp.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateFormSettings():
            """
            Retrieves the UpdateFormSettings GraphQL mutation.

            Returns:
                The UpdateFormSettings mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdateFormSettings.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def UpdateDataSettings():
            """
            Retrieves the UpdateDataSettings GraphQL mutation.

            Returns:
                The UpdateDataSettings mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdateDataSettings.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def RevokeToken():
            """
            Retrieves the RevokeToken GraphQL mutation.

            Returns:
                The RevokeToken mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "RevokeToken.gql"), "r") as file:
                return file.read()

        @staticmethod
        def PublishForm():
            """
            Retrieves the PublishForm GraphQL mutation.

            Returns:
                The PublishForm mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "PublishForm.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateForm():
            """
            Retrieves the UpdateForm GraphQL mutation.

            Returns:
                The UpdateForm mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "UpdateForm.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateWorkflow():
            """
            Retrieves the UpdateWorkflow GraphQL mutation.

            Returns:
                The UpdateWorkflow mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "UpdateWorkflow.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateIntegration():
            """
            Retrieves the UpdateIntegration GraphQL mutation.

            Returns:
                The UpdateIntegration mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdateIntegration.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def UpdatePermissions():
            """
            Retrieves the UpdatePermissions GraphQL mutation.

            Returns:
                The UpdatePermissions mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdatePermissions.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def CreateFilters():
            """
            Retrieves the CreateFilters GraphQL mutation.

            Returns:
                The CreateFilters mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateFilters.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ShareFilters():
            """
            Retrieves the ShareFilters GraphQL mutation.

            Returns:
                The ShareFilters mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "ShareFilters.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateAppFavorite():
            """
            Retrieves the UpdateAppFavorite GraphQL mutation.

            Returns:
                The UpdateAppFavorite mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdateAppFavorite.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def CreatePolicyGroup():
            """
            Retrieves the CreatePolicyGroup GraphQL mutation.

            Returns:
                The CreatePolicyGroup mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "CreatePolicyGroup.gql"), "r"
            ) as file:
                return file.read()
```

</details>

<details><summary><b>Queries - Docstring</b></summary>

```txt
Contains static methods for retrieving GraphQL queries related to various operations.

Methods:
    SearchSpaces(): Retrieves the SearchSpaces query.
    GetGroupByKualiID(): Retrieves the GetGroupByKualiID query.
    GetUserByKualiID(): Retrieves the GetUserByKualiID query.
    GetUser(): Retrieves the GetUser query.
    ExtractUserApiKeys(): Retrieves the ExtractUserApiKeys query.
    ExtractIntegrationBodies(): Retrieves the ExtractIntegrationBodies query.
    ExtractForm(): Retrieves the ExtractForm query.
    ExtractFormSettings(): Retrieves the ExtractFormSettings query.
    ExtractIntegration(): Retrieves the ExtractIntegration query.
    ExtractWorkflows(): Retrieves the ExtractWorkflows query.
    SpaceIDs(): Retrieves the SpaceIDs query.
    ExtractApps(): Retrieves the ExtractApps query.
    ExtractPermissions(): Retrieves the ExtractPermissions query.
    ExtractDataSettings(): Retrieves the ExtractDataSettings query.
```

</details>
<details><summary><b>Queries - Code</b></summary>

```python

        @staticmethod
        def SearchSpaces():
            """
            Retrieves the SearchSpaces GraphQL query.

            Returns:
                The SearchSpaces query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "SearchSpaces.gql"), "r") as file:
                return file.read()

        @staticmethod
        def GetGroupByKualiID():
            """
            Retrieves the GetGroupByKualiID GraphQL query.

            Returns:
                The GetGroupByKualiID query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "GetGroupByKualiID.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def GetUserByKualiID():
            """
            Retrieves the GetUserByKualiID GraphQL query.

            Returns:
                The GetUserByKualiID query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "GetUserByKualiID.gql"), "r") as file:
                return file.read()

        @staticmethod
        def GetUser():
            """
            Retrieves the GetUser GraphQL query.

            Returns:
                The GetUser query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "GetUser.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ExtractUserApiKeys():
            """
            Retrieves the ExtractUserApiKeys GraphQL query.

            Returns:
                The ExtractUserApiKeys query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractUserApiKeys.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def ExtractIntegrationBodies():
            """
            Retrieves the ExtractIntegrationBodies GraphQL query.

            Returns:
                The ExtractIntegrationBodies query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractIntegrationBodies.gql"),
                "r",
            ) as file:
                return file.read()

        @staticmethod
        def ExtractForm():
            """
            Retrieves the ExtractForm GraphQL query.

            Returns:
                The ExtractForm query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "ExtractForm.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ExtractFormSettings():
            """
            Retrieves the ExtractFormSettings GraphQL query.

            Returns:
                The ExtractFormSettings query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractFormSettings.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def ExtractIntegration():
            """
            Retrieves the ExtractIntegration GraphQL query.

            Returns:
                The ExtractIntegration query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractIntegration.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def ExtractWorkflows():
            """
            Retrieves the ExtractWorkflows GraphQL query.

            Returns:
                The ExtractWorkflows query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "ExtractWorkflows.gql"), "r") as file:
                return file.read()

        @staticmethod
        def SpaceIDs():
            """
            Retrieves the SpaceIDs GraphQL query.

            Returns:
                The SpaceIDs query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "SpaceIDs.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ExtractApps():
            """
            Retrieves the ExtractApps GraphQL query.

            Returns:
                The ExtractApps query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "ExtractApps.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ExtractPermissions():
            """
            Retrieves the ExtractPermissions GraphQL query.

            Returns:
                The ExtractPermissions query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractPermissions.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def ExtractDataSettings():
            """
            Retrieves the ExtractDataSettings GraphQL query.

            Returns:
                The ExtractDataSettings query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractDataSettings.gql"), "r"
            ) as file:
                return file.read()
```

</details>

<details><summary><b>AddGroupRoleUser - Docstring</b></summary>

```txt
Retrieves the AddGroupRoleUser GraphQL query.

Returns:
    The AddGroupRoleUser query as a string.
```

</details>
<details><summary><b>AddGroupRoleUser - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "AddGroupRoleUser.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>CreateGroup - Docstring</b></summary>

```txt
Retrieves the CreateGroup GraphQL mutation.

Returns:
    The CreateGroup mutation as a string.
```

</details>
<details><summary><b>CreateGroup - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "CreateGroup.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>CreateUser - Docstring</b></summary>

```txt
Retrieves the CreateUser GraphQL mutation.

Returns:
    The CreateUser mutation as a string.
```

</details>
<details><summary><b>CreateUser - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "CreateUser.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>CreateIntegration - Docstring</b></summary>

```txt
Retrieves the CreateIntegration GraphQL mutation.

Returns:
    The CreateIntegration mutation as a string.
```

</details>
<details><summary><b>CreateIntegration - Code</b></summary>

```python
            with open(
                join(KualiGraphQL.gql_path, "CreateIntegration.gql"), "r"
            ) as file:
                return file.read()
```

</details>

<details><summary><b>CreateToken - Docstring</b></summary>

```txt
Retrieves the CreateToken GraphQL mutation.

Returns:
    The CreateToken mutation as a string.
```

</details>
<details><summary><b>CreateToken - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "CreateToken.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>CreateApp - Docstring</b></summary>

```txt
Retrieves the CreateApp GraphQL mutation.

Returns:
    The CreateApp mutation as a string.
```

</details>
<details><summary><b>CreateApp - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "CreateApp.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>UpdateFormSettings - Docstring</b></summary>

```txt
Retrieves the UpdateFormSettings GraphQL mutation.

Returns:
    The UpdateFormSettings mutation as a string.
```

</details>
<details><summary><b>UpdateFormSettings - Code</b></summary>

```python
            with open(
                join(KualiGraphQL.gql_path, "UpdateFormSettings.gql"), "r"
            ) as file:
                return file.read()
```

</details>

<details><summary><b>UpdateDataSettings - Docstring</b></summary>

```txt
Retrieves the UpdateDataSettings GraphQL mutation.

Returns:
    The UpdateDataSettings mutation as a string.
```

</details>
<details><summary><b>UpdateDataSettings - Code</b></summary>

```python
            with open(
                join(KualiGraphQL.gql_path, "UpdateDataSettings.gql"), "r"
            ) as file:
                return file.read()
```

</details>

<details><summary><b>RevokeToken - Docstring</b></summary>

```txt
Retrieves the RevokeToken GraphQL mutation.

Returns:
    The RevokeToken mutation as a string.
```

</details>
<details><summary><b>RevokeToken - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "RevokeToken.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>PublishForm - Docstring</b></summary>

```txt
Retrieves the PublishForm GraphQL mutation.

Returns:
    The PublishForm mutation as a string.
```

</details>
<details><summary><b>PublishForm - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "PublishForm.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>UpdateForm - Docstring</b></summary>

```txt
Retrieves the UpdateForm GraphQL mutation.

Returns:
    The UpdateForm mutation as a string.
```

</details>
<details><summary><b>UpdateForm - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "UpdateForm.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>UpdateWorkflow - Docstring</b></summary>

```txt
Retrieves the UpdateWorkflow GraphQL mutation.

Returns:
    The UpdateWorkflow mutation as a string.
```

</details>
<details><summary><b>UpdateWorkflow - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "UpdateWorkflow.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>UpdateIntegration - Docstring</b></summary>

```txt
Retrieves the UpdateIntegration GraphQL mutation.

Returns:
    The UpdateIntegration mutation as a string.
```

</details>
<details><summary><b>UpdateIntegration - Code</b></summary>

```python
            with open(
                join(KualiGraphQL.gql_path, "UpdateIntegration.gql"), "r"
            ) as file:
                return file.read()
```

</details>

<details><summary><b>UpdatePermissions - Docstring</b></summary>

```txt
Retrieves the UpdatePermissions GraphQL mutation.

Returns:
    The UpdatePermissions mutation as a string.
```

</details>
<details><summary><b>UpdatePermissions - Code</b></summary>

```python
            with open(
                join(KualiGraphQL.gql_path, "UpdatePermissions.gql"), "r"
            ) as file:
                return file.read()
```

</details>

<details><summary><b>CreateFilters - Docstring</b></summary>

```txt
Retrieves the CreateFilters GraphQL mutation.

Returns:
    The CreateFilters mutation as a string.
```

</details>
<details><summary><b>CreateFilters - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "CreateFilters.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>ShareFilters - Docstring</b></summary>

```txt
Retrieves the ShareFilters GraphQL mutation.

Returns:
    The ShareFilters mutation as a string.
```

</details>
<details><summary><b>ShareFilters - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "ShareFilters.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>UpdateAppFavorite - Docstring</b></summary>

```txt
Retrieves the UpdateAppFavorite GraphQL mutation.

Returns:
    The UpdateAppFavorite mutation as a string.
```

</details>
<details><summary><b>UpdateAppFavorite - Code</b></summary>

```python
            with open(
                join(KualiGraphQL.gql_path, "UpdateAppFavorite.gql"), "r"
            ) as file:
                return file.read()
```

</details>

<details><summary><b>CreatePolicyGroup - Docstring</b></summary>

```txt
Retrieves the CreatePolicyGroup GraphQL mutation.

Returns:
    The CreatePolicyGroup mutation as a string.
```

</details>
<details><summary><b>CreatePolicyGroup - Code</b></summary>

```python
            with open(
                join(KualiGraphQL.gql_path, "CreatePolicyGroup.gql"), "r"
            ) as file:
                return file.read()
```

</details>

<details><summary><b>SearchSpaces - Docstring</b></summary>

```txt
Retrieves the SearchSpaces GraphQL query.

Returns:
    The SearchSpaces query as a string.
```

</details>
<details><summary><b>SearchSpaces - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "SearchSpaces.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>GetGroupByKualiID - Docstring</b></summary>

```txt
Retrieves the GetGroupByKualiID GraphQL query.

Returns:
    The GetGroupByKualiID query as a string.
```

</details>
<details><summary><b>GetGroupByKualiID - Code</b></summary>

```python
            with open(
                join(KualiGraphQL.gql_path, "GetGroupByKualiID.gql"), "r"
            ) as file:
                return file.read()
```

</details>

<details><summary><b>GetUserByKualiID - Docstring</b></summary>

```txt
Retrieves the GetUserByKualiID GraphQL query.

Returns:
    The GetUserByKualiID query as a string.
```

</details>
<details><summary><b>GetUserByKualiID - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "GetUserByKualiID.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>GetUser - Docstring</b></summary>

```txt
Retrieves the GetUser GraphQL query.

Returns:
    The GetUser query as a string.
```

</details>
<details><summary><b>GetUser - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "GetUser.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>ExtractUserApiKeys - Docstring</b></summary>

```txt
Retrieves the ExtractUserApiKeys GraphQL query.

Returns:
    The ExtractUserApiKeys query as a string.
```

</details>
<details><summary><b>ExtractUserApiKeys - Code</b></summary>

```python
            with open(
                join(KualiGraphQL.gql_path, "ExtractUserApiKeys.gql"), "r"
            ) as file:
                return file.read()
```

</details>

<details><summary><b>ExtractIntegrationBodies - Docstring</b></summary>

```txt
Retrieves the ExtractIntegrationBodies GraphQL query.

Returns:
    The ExtractIntegrationBodies query as a string.
```

</details>
<details><summary><b>ExtractIntegrationBodies - Code</b></summary>

```python
            with open(
                join(KualiGraphQL.gql_path, "ExtractIntegrationBodies.gql"),
                "r",
            ) as file:
                return file.read()
```

</details>

<details><summary><b>ExtractForm - Docstring</b></summary>

```txt
Retrieves the ExtractForm GraphQL query.

Returns:
    The ExtractForm query as a string.
```

</details>
<details><summary><b>ExtractForm - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "ExtractForm.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>ExtractFormSettings - Docstring</b></summary>

```txt
Retrieves the ExtractFormSettings GraphQL query.

Returns:
    The ExtractFormSettings query as a string.
```

</details>
<details><summary><b>ExtractFormSettings - Code</b></summary>

```python
            with open(
                join(KualiGraphQL.gql_path, "ExtractFormSettings.gql"), "r"
            ) as file:
                return file.read()
```

</details>

<details><summary><b>ExtractIntegration - Docstring</b></summary>

```txt
Retrieves the ExtractIntegration GraphQL query.

Returns:
    The ExtractIntegration query as a string.
```

</details>
<details><summary><b>ExtractIntegration - Code</b></summary>

```python
            with open(
                join(KualiGraphQL.gql_path, "ExtractIntegration.gql"), "r"
            ) as file:
                return file.read()
```

</details>

<details><summary><b>ExtractWorkflows - Docstring</b></summary>

```txt
Retrieves the ExtractWorkflows GraphQL query.

Returns:
    The ExtractWorkflows query as a string.
```

</details>
<details><summary><b>ExtractWorkflows - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "ExtractWorkflows.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>SpaceIDs - Docstring</b></summary>

```txt
Retrieves the SpaceIDs GraphQL query.

Returns:
    The SpaceIDs query as a string.
```

</details>
<details><summary><b>SpaceIDs - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "SpaceIDs.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>ExtractApps - Docstring</b></summary>

```txt
Retrieves the ExtractApps GraphQL query.

Returns:
    The ExtractApps query as a string.
```

</details>
<details><summary><b>ExtractApps - Code</b></summary>

```python
            with open(join(KualiGraphQL.gql_path, "ExtractApps.gql"), "r") as file:
                return file.read()
```

</details>

<details><summary><b>ExtractPermissions - Docstring</b></summary>

```txt
Retrieves the ExtractPermissions GraphQL query.

Returns:
    The ExtractPermissions query as a string.
```

</details>
<details><summary><b>ExtractPermissions - Code</b></summary>

```python
            with open(
                join(KualiGraphQL.gql_path, "ExtractPermissions.gql"), "r"
            ) as file:
                return file.read()
```

</details>

<details><summary><b>ExtractDataSettings - Docstring</b></summary>

```txt
Retrieves the ExtractDataSettings GraphQL query.

Returns:
    The ExtractDataSettings query as a string.
```

</details>
<details><summary><b>ExtractDataSettings - Code</b></summary>

```python
            with open(
                join(KualiGraphQL.gql_path, "ExtractDataSettings.gql"), "r"
            ) as file:
                return file.read()
```

</details>

### File: [graphql_variables.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/graphql_variables.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
import os
from os.path import dirname, abspath, join
import sys
import glob
import json

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)


class KualiVariables:
    """
    Contains static methods for generating GraphQL mutation and query variables for various operations.

    Classes:
        Mutations: Contains methods for generating mutation variables.
        Queries: Contains methods for generating query variables.
    """

    class Mutations:
        """
        Contains static methods for generating GraphQL mutation variables for various operations.

        Methods:
            AddGroupRoleUser(group_id, role_id, user_id): Generates the mutation variables for adding a group role user.
            CreateGroup(group_name): Generates the mutation variables for creating a group.
            CreateUser(create_user): Generates the mutation variables for creating a user.
            PublishForm(originating_form_id): Generates the mutation variables for publishing a form.
            CreateToken(user_id, target_form_name): Generates the mutation variables for creating a token.
            CreateFilters(form_filters): Generates the mutation variables for creating filters.
            ShareFilters(shared_filters): Generates the mutation variables for sharing filters.
            CreateApp(target_form_name, target_space_id, icon_name, icon_color): Generates the mutation variables for creating an app.
            RevokeToken(token_id, user_id): Generates the mutation variables for revoking a token.
            UpdateForm(originating_form_id, target_form_id, originating_form_name, target_form_name): Generates the mutation variables for updating a form.
            UpdateFormSettings(form_settings): Generates the mutation variables for updating form settings.
            UpdateDataSettings(data_settings): Generates the mutation variables for updating data settings.
            UpdateWorkflow(originating_form_id, target_form_id, originating_form_name, target_form_name): Generates the mutation variables for updating a workflow.
            UpdatePermissions(originating_form_id, target_form_id, originating_form_name, target_form_name): Generates the mutation variables for updating permissions.
            PublishTargetForm(target_form_id): Generates the mutation variables for publishing a target form.
            UpdateAppFavorite(target_form_id): Generates the mutation variables for updating app favorite status.
            CreatePolicyGroup(target_form_id, role_name): Generates the mutation variables for creating a policy group.
        """

        @staticmethod
        def AddGroupRoleUser(groud_id, role_id, user_id):
            """
            Generates the mutation variables for adding a group role user.

            Args:
                group_id: The ID of the group.
                role_id: The ID of the role.
                user_id: The ID of the user.

            Returns:
                A dictionary containing the mutation variables.
            """
            return {"groupId": groud_id, "roleId": role_id, "userId": user_id}

        @staticmethod
        def CreateGroup(group_name):
            """
            Generates the mutation variables for creating a group.

            Args:
                group_name: The name of the group.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"name": group_name})

        @staticmethod
        def CreateUser(create_user):
            """
            Generates the mutation variables for creating a user.

            Args:
                create_user: A dictionary containing the user data.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(create_user)

        @staticmethod
        def PublishForm(originating_form_id):
            """
            Generates the mutation variables for publishing a form.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"id": originating_form_id, "showDraft": True})

        @staticmethod
        def CreateToken(user_id, target_form_name):
            """
            Generates the mutation variables for creating a token.

            Args:
                user_id: The ID of the user.
                target_form_name: The name of the target form.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"userId": user_id, "name": target_form_name})

        @staticmethod
        def CreateFilters(form_filters):
            """
            Generates the mutation variables for creating filters.

            Args:
                form_filters: A dictionary containing the form filters.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(form_filters)

        @staticmethod
        def ShareFilters(shared_filters):
            """
            Generates the mutation variables for sharing filters.

            Args:
                shared_filters: A dictionary containing the shared filters.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(shared_filters)

        @staticmethod
        def CreateApp(target_form_name, target_space_id, icon_name, icon_color):
            """
            Generates the mutation variables for creating an app.

            Args:
                target_form_name: The name of the target form.
                target_space_id: The ID of the target space.
                icon_name: The name of the app's icon.
                icon_color: The background color of the app's icon.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(
                {
                    "args": {
                        "name": target_form_name,
                        "backgroundColor": icon_color,
                        "iconName": icon_name,
                        "spaceId": target_space_id,
                    }
                }
            )

        @staticmethod
        def RevokeToken(token_id, user_id):
            """
            Generates the mutation variables for revoking a token.

            Args:
                token_id: The ID of the token.
                user_id: The ID of the user.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"userId": user_id, "tokenId": token_id})

        @staticmethod
        def UpdateForm(
            originating_form_id,
            target_form_id,
            originating_form_name,
            target_form_name,
        ):
            """
            Generates the mutation variables for updating a form.

            Args:
                originating_form_id: The ID of the originating form.
                target_form_id: The ID of the target form.
                originating_form_name: The name of the originating form.
                target_form_name: The name of the target form.

            Returns:
                A string containing the JSON data read from the form file.
            """
            with open(
                join(
                    parent_dir,
                    "kuali_skeletons",
                    target_form_name,
                    "form.json",
                ),
                "r",
            ) as f:
                json_str = f.read()
            return json_str

        @staticmethod
        def UpdateFormSettings(form_settings):
            """
            Generates the mutation variables for updating form settings.

            Args:
                form_settings: A dictionary containing the form settings.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(form_settings)

        @staticmethod
        def UpdateDataSettings(data_settings):
            """
            Generates the mutation variables for updating data settings.

            Args:
                data_settings: A dictionary containing the data settings.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(data_settings)

        @staticmethod
        def UpdateWorkflow(
            originating_form_id,
            target_form_id,
            originating_form_name,
            target_form_name,
        ):
            """
            Generates the mutation variables for updating a workflow.

            Args:
                originating_form_id: The ID of the originating form.
                target_form_id: The ID of the target form.
                originating_form_name: The name of the originating form.
                target_form_name: The name of the target form.

            Returns:
                A string containing the JSON data read from the workflow file.
            """
            with open(
                join(
                    parent_dir,
                    "kuali_skeletons",
                    target_form_name,
                    "workflow.json",
                ),
                "r",
            ) as f:
                json_str = f.read()
            return json_str

        @staticmethod
        def UpdatePermissions(
            originating_form_id,
            target_form_id,
            originating_form_name,
            target_form_name,
        ):
            """
            Generates the mutation variables for updating permissions.

            Args:
                originating_form_id: The ID of the originating form.
                target_form_id: The ID of the target form.
                originating_form_name: The name of the originating form.
                target_form_name: The name of the target form.

            Returns:
                A list of strings containing the JSON data read from the permission files.
            """
            directory = join(
                parent_dir, "kuali_skeletons", target_form_name, "permissions"
            )
            files = glob.glob(join(directory, "*.json"))
            json_strings = []
            for file in files:
                with open(file, "r") as f:
                    json_str = f.read()
                    json_strings.append(json_str)
            return json_strings

        @staticmethod
        def PublishTargetForm(target_form_id):
            """
            Generates the mutation variables for publishing a target form.

            Args:
                target_form_id: The ID of the target form.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"id": target_form_id, "showDraft": False})

        @staticmethod
        def UpdateAppFavorite(target_form_id):
            """
            Generates the mutation variables for updating the favorite status of an app.

            Args:
                target_form_id: The ID of the target form (application).

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"applicationId": target_form_id, "isFavorite": True})

        @staticmethod
        def CreatePolicyGroup(target_form_id, role_name):
            """
            Generates the mutation variables for creating a policy group.

            Args:
                target_form_id: The ID of the target form (application).
                role_name: The name of the role.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"applicationId": target_form_id, "name": role_name})

    class Queries:
        """
        Contains static methods for generating GraphQL query variables for various operations.

        Methods:
            SearchSpaces: Generates the query variables for searching spaces.
            GetGroupByKualiID: Generates the query variables for retrieving a group by Kuali ID.
            PermissionsPage: Generates the query variables for retrieving permissions page.
            GetUserByKualiID: Generates the query variables for retrieving a user by Kuali ID.
            GetUser: Generates the query variables for retrieving a user.
            ExtractApps: Generates the query variables for extracting apps.
            ExtractIntegrationBodies: Generates the query variables for extracting integration bodies.
            ExtractUserApiKeys: Generates the query variables for extracting user API keys.
            ExtractForm: Generates the query variables for extracting a form.
            ExtractFormSettings: Generates the query variables for extracting form settings.
            ExtractWorkflows: Generates the query variables for extracting workflows.
            ExtractIntegration: Generates the query variables for extracting an integration.
            ExtractPermissions: Generates the query variables for extracting permissions.
            ExtractDataSettings: Generates the query variables for extracting data settings.
        """

        @staticmethod
        def SearchSpaces(search):
            """
            Generates the query variables for searching spaces.

            Args:
                search: The search query.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"query": search})

        @staticmethod
        def GetGroupByKualiID(group_id):
            """
            Generates the query variables for retrieving a group by Kuali ID.

            Args:
                group_id: The Kuali ID of the group.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": group_id})

        @staticmethod
        def PermissionsPage(appId):
            """
            Generates the query variables for retrieving permissions page.

            Args:
                appId: The ID of the application.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"appId": appId})

        @staticmethod
        def GetUserByKualiID(user_kuali_id):
            """
            Generates the query variables for retrieving a user by Kuali ID.

            Args:
                user_kuali_id: The Kuali ID of the user.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": user_kuali_id})

        @staticmethod
        def GetUser(app_owner):
            """
            Generates the query variables for retrieving a user.

            Args:
                app_owner: The owner of the application.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps(
                {
                    "active": "ACTIVE",
                    "limit": 1,
                    "skip": 0,
                    "sort": [],
                    "q": app_owner,
                }
            )

        @staticmethod
        def ExtractApps():
            """
            Generates the query variables for extracting apps.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"query": ""})

        @staticmethod
        def ExtractIntegrationBodies(integrationId):
            """
            Generates the query variables for extracting integration bodies.

            Args:
                integrationId: The ID of the integration.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"integrationId": integrationId})

        @staticmethod
        def ExtractUserApiKeys(user_id, target_form_name):
            """
            Generates the query variables for extracting user API keys.

            Args:
                user_id: The ID of the user.
                target_form_name: The name of the target form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": user_id})

        @staticmethod
        def ExtractForm(originating_form_id):
            """
            Generates the query variables for extracting a form.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": originating_form_id})

        @staticmethod
        def ExtractFormSettings(originating_form_id):
            """
            Generates the query variables for extracting form settings.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"appId": originating_form_id})

        @staticmethod
        def ExtractWorkflows(originating_form_id):
            """
            Generates the query variables for extracting workflows.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": originating_form_id})

        @staticmethod
        def ExtractIntegration(integration_id):
            """
            Generates the query variables for extracting an integration.

            Args:
                integration_id: The ID of the integration.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"integrationId": integration_id})

        @staticmethod
        def ExtractPermissions(form_id):
            """
            Generates the query variables for extracting permissions.

            Args:
                form_id: The ID of the form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"appId": form_id})

        @staticmethod
        def ExtractDataSettings(originating_form_id):
            """
            Generates the query variables for extracting data settings.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"appId": originating_form_id, "query": ""})
```

</details>

<details><summary><b>KualiVariables - Docstring</b></summary>

```txt
Contains static methods for generating GraphQL mutation and query variables for various operations.

Classes:
    Mutations: Contains methods for generating mutation variables.
    Queries: Contains methods for generating query variables.
```

</details>
<details><summary><b>KualiVariables - Code</b></summary>

```python

    class Mutations:
        """
        Contains static methods for generating GraphQL mutation variables for various operations.

        Methods:
            AddGroupRoleUser(group_id, role_id, user_id): Generates the mutation variables for adding a group role user.
            CreateGroup(group_name): Generates the mutation variables for creating a group.
            CreateUser(create_user): Generates the mutation variables for creating a user.
            PublishForm(originating_form_id): Generates the mutation variables for publishing a form.
            CreateToken(user_id, target_form_name): Generates the mutation variables for creating a token.
            CreateFilters(form_filters): Generates the mutation variables for creating filters.
            ShareFilters(shared_filters): Generates the mutation variables for sharing filters.
            CreateApp(target_form_name, target_space_id, icon_name, icon_color): Generates the mutation variables for creating an app.
            RevokeToken(token_id, user_id): Generates the mutation variables for revoking a token.
            UpdateForm(originating_form_id, target_form_id, originating_form_name, target_form_name): Generates the mutation variables for updating a form.
            UpdateFormSettings(form_settings): Generates the mutation variables for updating form settings.
            UpdateDataSettings(data_settings): Generates the mutation variables for updating data settings.
            UpdateWorkflow(originating_form_id, target_form_id, originating_form_name, target_form_name): Generates the mutation variables for updating a workflow.
            UpdatePermissions(originating_form_id, target_form_id, originating_form_name, target_form_name): Generates the mutation variables for updating permissions.
            PublishTargetForm(target_form_id): Generates the mutation variables for publishing a target form.
            UpdateAppFavorite(target_form_id): Generates the mutation variables for updating app favorite status.
            CreatePolicyGroup(target_form_id, role_name): Generates the mutation variables for creating a policy group.
        """

        @staticmethod
        def AddGroupRoleUser(groud_id, role_id, user_id):
            """
            Generates the mutation variables for adding a group role user.

            Args:
                group_id: The ID of the group.
                role_id: The ID of the role.
                user_id: The ID of the user.

            Returns:
                A dictionary containing the mutation variables.
            """
            return {"groupId": groud_id, "roleId": role_id, "userId": user_id}

        @staticmethod
        def CreateGroup(group_name):
            """
            Generates the mutation variables for creating a group.

            Args:
                group_name: The name of the group.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"name": group_name})

        @staticmethod
        def CreateUser(create_user):
            """
            Generates the mutation variables for creating a user.

            Args:
                create_user: A dictionary containing the user data.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(create_user)

        @staticmethod
        def PublishForm(originating_form_id):
            """
            Generates the mutation variables for publishing a form.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"id": originating_form_id, "showDraft": True})

        @staticmethod
        def CreateToken(user_id, target_form_name):
            """
            Generates the mutation variables for creating a token.

            Args:
                user_id: The ID of the user.
                target_form_name: The name of the target form.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"userId": user_id, "name": target_form_name})

        @staticmethod
        def CreateFilters(form_filters):
            """
            Generates the mutation variables for creating filters.

            Args:
                form_filters: A dictionary containing the form filters.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(form_filters)

        @staticmethod
        def ShareFilters(shared_filters):
            """
            Generates the mutation variables for sharing filters.

            Args:
                shared_filters: A dictionary containing the shared filters.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(shared_filters)

        @staticmethod
        def CreateApp(target_form_name, target_space_id, icon_name, icon_color):
            """
            Generates the mutation variables for creating an app.

            Args:
                target_form_name: The name of the target form.
                target_space_id: The ID of the target space.
                icon_name: The name of the app's icon.
                icon_color: The background color of the app's icon.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(
                {
                    "args": {
                        "name": target_form_name,
                        "backgroundColor": icon_color,
                        "iconName": icon_name,
                        "spaceId": target_space_id,
                    }
                }
            )

        @staticmethod
        def RevokeToken(token_id, user_id):
            """
            Generates the mutation variables for revoking a token.

            Args:
                token_id: The ID of the token.
                user_id: The ID of the user.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"userId": user_id, "tokenId": token_id})

        @staticmethod
        def UpdateForm(
            originating_form_id,
            target_form_id,
            originating_form_name,
            target_form_name,
        ):
            """
            Generates the mutation variables for updating a form.

            Args:
                originating_form_id: The ID of the originating form.
                target_form_id: The ID of the target form.
                originating_form_name: The name of the originating form.
                target_form_name: The name of the target form.

            Returns:
                A string containing the JSON data read from the form file.
            """
            with open(
                join(
                    parent_dir,
                    "kuali_skeletons",
                    target_form_name,
                    "form.json",
                ),
                "r",
            ) as f:
                json_str = f.read()
            return json_str

        @staticmethod
        def UpdateFormSettings(form_settings):
            """
            Generates the mutation variables for updating form settings.

            Args:
                form_settings: A dictionary containing the form settings.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(form_settings)

        @staticmethod
        def UpdateDataSettings(data_settings):
            """
            Generates the mutation variables for updating data settings.

            Args:
                data_settings: A dictionary containing the data settings.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(data_settings)

        @staticmethod
        def UpdateWorkflow(
            originating_form_id,
            target_form_id,
            originating_form_name,
            target_form_name,
        ):
            """
            Generates the mutation variables for updating a workflow.

            Args:
                originating_form_id: The ID of the originating form.
                target_form_id: The ID of the target form.
                originating_form_name: The name of the originating form.
                target_form_name: The name of the target form.

            Returns:
                A string containing the JSON data read from the workflow file.
            """
            with open(
                join(
                    parent_dir,
                    "kuali_skeletons",
                    target_form_name,
                    "workflow.json",
                ),
                "r",
            ) as f:
                json_str = f.read()
            return json_str

        @staticmethod
        def UpdatePermissions(
            originating_form_id,
            target_form_id,
            originating_form_name,
            target_form_name,
        ):
            """
            Generates the mutation variables for updating permissions.

            Args:
                originating_form_id: The ID of the originating form.
                target_form_id: The ID of the target form.
                originating_form_name: The name of the originating form.
                target_form_name: The name of the target form.

            Returns:
                A list of strings containing the JSON data read from the permission files.
            """
            directory = join(
                parent_dir, "kuali_skeletons", target_form_name, "permissions"
            )
            files = glob.glob(join(directory, "*.json"))
            json_strings = []
            for file in files:
                with open(file, "r") as f:
                    json_str = f.read()
                    json_strings.append(json_str)
            return json_strings

        @staticmethod
        def PublishTargetForm(target_form_id):
            """
            Generates the mutation variables for publishing a target form.

            Args:
                target_form_id: The ID of the target form.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"id": target_form_id, "showDraft": False})

        @staticmethod
        def UpdateAppFavorite(target_form_id):
            """
            Generates the mutation variables for updating the favorite status of an app.

            Args:
                target_form_id: The ID of the target form (application).

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"applicationId": target_form_id, "isFavorite": True})

        @staticmethod
        def CreatePolicyGroup(target_form_id, role_name):
            """
            Generates the mutation variables for creating a policy group.

            Args:
                target_form_id: The ID of the target form (application).
                role_name: The name of the role.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"applicationId": target_form_id, "name": role_name})

    class Queries:
        """
        Contains static methods for generating GraphQL query variables for various operations.

        Methods:
            SearchSpaces: Generates the query variables for searching spaces.
            GetGroupByKualiID: Generates the query variables for retrieving a group by Kuali ID.
            PermissionsPage: Generates the query variables for retrieving permissions page.
            GetUserByKualiID: Generates the query variables for retrieving a user by Kuali ID.
            GetUser: Generates the query variables for retrieving a user.
            ExtractApps: Generates the query variables for extracting apps.
            ExtractIntegrationBodies: Generates the query variables for extracting integration bodies.
            ExtractUserApiKeys: Generates the query variables for extracting user API keys.
            ExtractForm: Generates the query variables for extracting a form.
            ExtractFormSettings: Generates the query variables for extracting form settings.
            ExtractWorkflows: Generates the query variables for extracting workflows.
            ExtractIntegration: Generates the query variables for extracting an integration.
            ExtractPermissions: Generates the query variables for extracting permissions.
            ExtractDataSettings: Generates the query variables for extracting data settings.
        """

        @staticmethod
        def SearchSpaces(search):
            """
            Generates the query variables for searching spaces.

            Args:
                search: The search query.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"query": search})

        @staticmethod
        def GetGroupByKualiID(group_id):
            """
            Generates the query variables for retrieving a group by Kuali ID.

            Args:
                group_id: The Kuali ID of the group.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": group_id})

        @staticmethod
        def PermissionsPage(appId):
            """
            Generates the query variables for retrieving permissions page.

            Args:
                appId: The ID of the application.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"appId": appId})

        @staticmethod
        def GetUserByKualiID(user_kuali_id):
            """
            Generates the query variables for retrieving a user by Kuali ID.

            Args:
                user_kuali_id: The Kuali ID of the user.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": user_kuali_id})

        @staticmethod
        def GetUser(app_owner):
            """
            Generates the query variables for retrieving a user.

            Args:
                app_owner: The owner of the application.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps(
                {
                    "active": "ACTIVE",
                    "limit": 1,
                    "skip": 0,
                    "sort": [],
                    "q": app_owner,
                }
            )

        @staticmethod
        def ExtractApps():
            """
            Generates the query variables for extracting apps.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"query": ""})

        @staticmethod
        def ExtractIntegrationBodies(integrationId):
            """
            Generates the query variables for extracting integration bodies.

            Args:
                integrationId: The ID of the integration.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"integrationId": integrationId})

        @staticmethod
        def ExtractUserApiKeys(user_id, target_form_name):
            """
            Generates the query variables for extracting user API keys.

            Args:
                user_id: The ID of the user.
                target_form_name: The name of the target form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": user_id})

        @staticmethod
        def ExtractForm(originating_form_id):
            """
            Generates the query variables for extracting a form.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": originating_form_id})

        @staticmethod
        def ExtractFormSettings(originating_form_id):
            """
            Generates the query variables for extracting form settings.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"appId": originating_form_id})

        @staticmethod
        def ExtractWorkflows(originating_form_id):
            """
            Generates the query variables for extracting workflows.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": originating_form_id})

        @staticmethod
        def ExtractIntegration(integration_id):
            """
            Generates the query variables for extracting an integration.

            Args:
                integration_id: The ID of the integration.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"integrationId": integration_id})

        @staticmethod
        def ExtractPermissions(form_id):
            """
            Generates the query variables for extracting permissions.

            Args:
                form_id: The ID of the form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"appId": form_id})

        @staticmethod
        def ExtractDataSettings(originating_form_id):
            """
            Generates the query variables for extracting data settings.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"appId": originating_form_id, "query": ""})
```

</details>

<details><summary><b>Mutations - Docstring</b></summary>

```txt
Contains static methods for generating GraphQL mutation variables for various operations.

Methods:
    AddGroupRoleUser(group_id, role_id, user_id): Generates the mutation variables for adding a group role user.
    CreateGroup(group_name): Generates the mutation variables for creating a group.
    CreateUser(create_user): Generates the mutation variables for creating a user.
    PublishForm(originating_form_id): Generates the mutation variables for publishing a form.
    CreateToken(user_id, target_form_name): Generates the mutation variables for creating a token.
    CreateFilters(form_filters): Generates the mutation variables for creating filters.
    ShareFilters(shared_filters): Generates the mutation variables for sharing filters.
    CreateApp(target_form_name, target_space_id, icon_name, icon_color): Generates the mutation variables for creating an app.
    RevokeToken(token_id, user_id): Generates the mutation variables for revoking a token.
    UpdateForm(originating_form_id, target_form_id, originating_form_name, target_form_name): Generates the mutation variables for updating a form.
    UpdateFormSettings(form_settings): Generates the mutation variables for updating form settings.
    UpdateDataSettings(data_settings): Generates the mutation variables for updating data settings.
    UpdateWorkflow(originating_form_id, target_form_id, originating_form_name, target_form_name): Generates the mutation variables for updating a workflow.
    UpdatePermissions(originating_form_id, target_form_id, originating_form_name, target_form_name): Generates the mutation variables for updating permissions.
    PublishTargetForm(target_form_id): Generates the mutation variables for publishing a target form.
    UpdateAppFavorite(target_form_id): Generates the mutation variables for updating app favorite status.
    CreatePolicyGroup(target_form_id, role_name): Generates the mutation variables for creating a policy group.
```

</details>
<details><summary><b>Mutations - Code</b></summary>

```python

        @staticmethod
        def AddGroupRoleUser(groud_id, role_id, user_id):
            """
            Generates the mutation variables for adding a group role user.

            Args:
                group_id: The ID of the group.
                role_id: The ID of the role.
                user_id: The ID of the user.

            Returns:
                A dictionary containing the mutation variables.
            """
            return {"groupId": groud_id, "roleId": role_id, "userId": user_id}

        @staticmethod
        def CreateGroup(group_name):
            """
            Generates the mutation variables for creating a group.

            Args:
                group_name: The name of the group.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"name": group_name})

        @staticmethod
        def CreateUser(create_user):
            """
            Generates the mutation variables for creating a user.

            Args:
                create_user: A dictionary containing the user data.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(create_user)

        @staticmethod
        def PublishForm(originating_form_id):
            """
            Generates the mutation variables for publishing a form.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"id": originating_form_id, "showDraft": True})

        @staticmethod
        def CreateToken(user_id, target_form_name):
            """
            Generates the mutation variables for creating a token.

            Args:
                user_id: The ID of the user.
                target_form_name: The name of the target form.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"userId": user_id, "name": target_form_name})

        @staticmethod
        def CreateFilters(form_filters):
            """
            Generates the mutation variables for creating filters.

            Args:
                form_filters: A dictionary containing the form filters.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(form_filters)

        @staticmethod
        def ShareFilters(shared_filters):
            """
            Generates the mutation variables for sharing filters.

            Args:
                shared_filters: A dictionary containing the shared filters.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(shared_filters)

        @staticmethod
        def CreateApp(target_form_name, target_space_id, icon_name, icon_color):
            """
            Generates the mutation variables for creating an app.

            Args:
                target_form_name: The name of the target form.
                target_space_id: The ID of the target space.
                icon_name: The name of the app's icon.
                icon_color: The background color of the app's icon.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(
                {
                    "args": {
                        "name": target_form_name,
                        "backgroundColor": icon_color,
                        "iconName": icon_name,
                        "spaceId": target_space_id,
                    }
                }
            )

        @staticmethod
        def RevokeToken(token_id, user_id):
            """
            Generates the mutation variables for revoking a token.

            Args:
                token_id: The ID of the token.
                user_id: The ID of the user.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"userId": user_id, "tokenId": token_id})

        @staticmethod
        def UpdateForm(
            originating_form_id,
            target_form_id,
            originating_form_name,
            target_form_name,
        ):
            """
            Generates the mutation variables for updating a form.

            Args:
                originating_form_id: The ID of the originating form.
                target_form_id: The ID of the target form.
                originating_form_name: The name of the originating form.
                target_form_name: The name of the target form.

            Returns:
                A string containing the JSON data read from the form file.
            """
            with open(
                join(
                    parent_dir,
                    "kuali_skeletons",
                    target_form_name,
                    "form.json",
                ),
                "r",
            ) as f:
                json_str = f.read()
            return json_str

        @staticmethod
        def UpdateFormSettings(form_settings):
            """
            Generates the mutation variables for updating form settings.

            Args:
                form_settings: A dictionary containing the form settings.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(form_settings)

        @staticmethod
        def UpdateDataSettings(data_settings):
            """
            Generates the mutation variables for updating data settings.

            Args:
                data_settings: A dictionary containing the data settings.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(data_settings)

        @staticmethod
        def UpdateWorkflow(
            originating_form_id,
            target_form_id,
            originating_form_name,
            target_form_name,
        ):
            """
            Generates the mutation variables for updating a workflow.

            Args:
                originating_form_id: The ID of the originating form.
                target_form_id: The ID of the target form.
                originating_form_name: The name of the originating form.
                target_form_name: The name of the target form.

            Returns:
                A string containing the JSON data read from the workflow file.
            """
            with open(
                join(
                    parent_dir,
                    "kuali_skeletons",
                    target_form_name,
                    "workflow.json",
                ),
                "r",
            ) as f:
                json_str = f.read()
            return json_str

        @staticmethod
        def UpdatePermissions(
            originating_form_id,
            target_form_id,
            originating_form_name,
            target_form_name,
        ):
            """
            Generates the mutation variables for updating permissions.

            Args:
                originating_form_id: The ID of the originating form.
                target_form_id: The ID of the target form.
                originating_form_name: The name of the originating form.
                target_form_name: The name of the target form.

            Returns:
                A list of strings containing the JSON data read from the permission files.
            """
            directory = join(
                parent_dir, "kuali_skeletons", target_form_name, "permissions"
            )
            files = glob.glob(join(directory, "*.json"))
            json_strings = []
            for file in files:
                with open(file, "r") as f:
                    json_str = f.read()
                    json_strings.append(json_str)
            return json_strings

        @staticmethod
        def PublishTargetForm(target_form_id):
            """
            Generates the mutation variables for publishing a target form.

            Args:
                target_form_id: The ID of the target form.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"id": target_form_id, "showDraft": False})

        @staticmethod
        def UpdateAppFavorite(target_form_id):
            """
            Generates the mutation variables for updating the favorite status of an app.

            Args:
                target_form_id: The ID of the target form (application).

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"applicationId": target_form_id, "isFavorite": True})

        @staticmethod
        def CreatePolicyGroup(target_form_id, role_name):
            """
            Generates the mutation variables for creating a policy group.

            Args:
                target_form_id: The ID of the target form (application).
                role_name: The name of the role.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"applicationId": target_form_id, "name": role_name})
```

</details>

<details><summary><b>Queries - Docstring</b></summary>

```txt
Contains static methods for generating GraphQL query variables for various operations.

Methods:
    SearchSpaces: Generates the query variables for searching spaces.
    GetGroupByKualiID: Generates the query variables for retrieving a group by Kuali ID.
    PermissionsPage: Generates the query variables for retrieving permissions page.
    GetUserByKualiID: Generates the query variables for retrieving a user by Kuali ID.
    GetUser: Generates the query variables for retrieving a user.
    ExtractApps: Generates the query variables for extracting apps.
    ExtractIntegrationBodies: Generates the query variables for extracting integration bodies.
    ExtractUserApiKeys: Generates the query variables for extracting user API keys.
    ExtractForm: Generates the query variables for extracting a form.
    ExtractFormSettings: Generates the query variables for extracting form settings.
    ExtractWorkflows: Generates the query variables for extracting workflows.
    ExtractIntegration: Generates the query variables for extracting an integration.
    ExtractPermissions: Generates the query variables for extracting permissions.
    ExtractDataSettings: Generates the query variables for extracting data settings.
```

</details>
<details><summary><b>Queries - Code</b></summary>

```python

        @staticmethod
        def SearchSpaces(search):
            """
            Generates the query variables for searching spaces.

            Args:
                search: The search query.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"query": search})

        @staticmethod
        def GetGroupByKualiID(group_id):
            """
            Generates the query variables for retrieving a group by Kuali ID.

            Args:
                group_id: The Kuali ID of the group.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": group_id})

        @staticmethod
        def PermissionsPage(appId):
            """
            Generates the query variables for retrieving permissions page.

            Args:
                appId: The ID of the application.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"appId": appId})

        @staticmethod
        def GetUserByKualiID(user_kuali_id):
            """
            Generates the query variables for retrieving a user by Kuali ID.

            Args:
                user_kuali_id: The Kuali ID of the user.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": user_kuali_id})

        @staticmethod
        def GetUser(app_owner):
            """
            Generates the query variables for retrieving a user.

            Args:
                app_owner: The owner of the application.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps(
                {
                    "active": "ACTIVE",
                    "limit": 1,
                    "skip": 0,
                    "sort": [],
                    "q": app_owner,
                }
            )

        @staticmethod
        def ExtractApps():
            """
            Generates the query variables for extracting apps.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"query": ""})

        @staticmethod
        def ExtractIntegrationBodies(integrationId):
            """
            Generates the query variables for extracting integration bodies.

            Args:
                integrationId: The ID of the integration.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"integrationId": integrationId})

        @staticmethod
        def ExtractUserApiKeys(user_id, target_form_name):
            """
            Generates the query variables for extracting user API keys.

            Args:
                user_id: The ID of the user.
                target_form_name: The name of the target form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": user_id})

        @staticmethod
        def ExtractForm(originating_form_id):
            """
            Generates the query variables for extracting a form.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": originating_form_id})

        @staticmethod
        def ExtractFormSettings(originating_form_id):
            """
            Generates the query variables for extracting form settings.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"appId": originating_form_id})

        @staticmethod
        def ExtractWorkflows(originating_form_id):
            """
            Generates the query variables for extracting workflows.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": originating_form_id})

        @staticmethod
        def ExtractIntegration(integration_id):
            """
            Generates the query variables for extracting an integration.

            Args:
                integration_id: The ID of the integration.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"integrationId": integration_id})

        @staticmethod
        def ExtractPermissions(form_id):
            """
            Generates the query variables for extracting permissions.

            Args:
                form_id: The ID of the form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"appId": form_id})

        @staticmethod
        def ExtractDataSettings(originating_form_id):
            """
            Generates the query variables for extracting data settings.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"appId": originating_form_id, "query": ""})
```

</details>

<details><summary><b>AddGroupRoleUser - Docstring</b></summary>

```txt
Generates the mutation variables for adding a group role user.

Args:
    group_id: The ID of the group.
    role_id: The ID of the role.
    user_id: The ID of the user.

Returns:
    A dictionary containing the mutation variables.
```

</details>
<details><summary><b>AddGroupRoleUser - Code</b></summary>

```python
            return {"groupId": groud_id, "roleId": role_id, "userId": user_id}
```

</details>

<details><summary><b>CreateGroup - Docstring</b></summary>

```txt
Generates the mutation variables for creating a group.

Args:
    group_name: The name of the group.

Returns:
    A JSON string containing the mutation variables.
```

</details>
<details><summary><b>CreateGroup - Code</b></summary>

```python
            return json.dumps({"name": group_name})
```

</details>

<details><summary><b>CreateUser - Docstring</b></summary>

```txt
Generates the mutation variables for creating a user.

Args:
    create_user: A dictionary containing the user data.

Returns:
    A JSON string containing the mutation variables.
```

</details>
<details><summary><b>CreateUser - Code</b></summary>

```python
            return json.dumps(create_user)
```

</details>

<details><summary><b>PublishForm - Docstring</b></summary>

```txt
Generates the mutation variables for publishing a form.

Args:
    originating_form_id: The ID of the originating form.

Returns:
    A JSON string containing the mutation variables.
```

</details>
<details><summary><b>PublishForm - Code</b></summary>

```python
            return json.dumps({"id": originating_form_id, "showDraft": True})
```

</details>

<details><summary><b>CreateToken - Docstring</b></summary>

```txt
Generates the mutation variables for creating a token.

Args:
    user_id: The ID of the user.
    target_form_name: The name of the target form.

Returns:
    A JSON string containing the mutation variables.
```

</details>
<details><summary><b>CreateToken - Code</b></summary>

```python
            return json.dumps({"userId": user_id, "name": target_form_name})
```

</details>

<details><summary><b>CreateFilters - Docstring</b></summary>

```txt
Generates the mutation variables for creating filters.

Args:
    form_filters: A dictionary containing the form filters.

Returns:
    A JSON string containing the mutation variables.
```

</details>
<details><summary><b>CreateFilters - Code</b></summary>

```python
            return json.dumps(form_filters)
```

</details>

<details><summary><b>ShareFilters - Docstring</b></summary>

```txt
Generates the mutation variables for sharing filters.

Args:
    shared_filters: A dictionary containing the shared filters.

Returns:
    A JSON string containing the mutation variables.
```

</details>
<details><summary><b>ShareFilters - Code</b></summary>

```python
            return json.dumps(shared_filters)
```

</details>

<details><summary><b>CreateApp - Docstring</b></summary>

```txt
Generates the mutation variables for creating an app.

Args:
    target_form_name: The name of the target form.
    target_space_id: The ID of the target space.
    icon_name: The name of the app's icon.
    icon_color: The background color of the app's icon.

Returns:
    A JSON string containing the mutation variables.
```

</details>
<details><summary><b>CreateApp - Code</b></summary>

```python
            return json.dumps(
                {
                    "args": {
                        "name": target_form_name,
                        "backgroundColor": icon_color,
                        "iconName": icon_name,
                        "spaceId": target_space_id,
                    }
                }
            )
```

</details>

<details><summary><b>RevokeToken - Docstring</b></summary>

```txt
Generates the mutation variables for revoking a token.

Args:
    token_id: The ID of the token.
    user_id: The ID of the user.

Returns:
    A JSON string containing the mutation variables.
```

</details>
<details><summary><b>RevokeToken - Code</b></summary>

```python
            return json.dumps({"userId": user_id, "tokenId": token_id})
```

</details>

<details><summary><b>UpdateForm - Docstring</b></summary>

```txt
Generates the mutation variables for updating a form.

Args:
    originating_form_id: The ID of the originating form.
    target_form_id: The ID of the target form.
    originating_form_name: The name of the originating form.
    target_form_name: The name of the target form.

Returns:
    A string containing the JSON data read from the form file.
```

</details>
<details><summary><b>UpdateForm - Code</b></summary>

```python
                target_form_name: The name of the target form.

            Returns:
                A string containing the JSON data read from the form file.
            """
            with open(
                join(
                    parent_dir,
                    "kuali_skeletons",
                    target_form_name,
                    "form.json",
                ),
                "r",
            ) as f:
                json_str = f.read()
            return json_str
```

</details>

<details><summary><b>UpdateFormSettings - Docstring</b></summary>

```txt
Generates the mutation variables for updating form settings.

Args:
    form_settings: A dictionary containing the form settings.

Returns:
    A JSON string containing the mutation variables.
```

</details>
<details><summary><b>UpdateFormSettings - Code</b></summary>

```python
            return json.dumps(form_settings)
```

</details>

<details><summary><b>UpdateDataSettings - Docstring</b></summary>

```txt
Generates the mutation variables for updating data settings.

Args:
    data_settings: A dictionary containing the data settings.

Returns:
    A JSON string containing the mutation variables.
```

</details>
<details><summary><b>UpdateDataSettings - Code</b></summary>

```python
            return json.dumps(data_settings)
```

</details>

<details><summary><b>UpdateWorkflow - Docstring</b></summary>

```txt
Generates the mutation variables for updating a workflow.

Args:
    originating_form_id: The ID of the originating form.
    target_form_id: The ID of the target form.
    originating_form_name: The name of the originating form.
    target_form_name: The name of the target form.

Returns:
    A string containing the JSON data read from the workflow file.
```

</details>
<details><summary><b>UpdateWorkflow - Code</b></summary>

```python
                target_form_name: The name of the target form.

            Returns:
                A string containing the JSON data read from the workflow file.
            """
            with open(
                join(
                    parent_dir,
                    "kuali_skeletons",
                    target_form_name,
                    "workflow.json",
                ),
                "r",
            ) as f:
                json_str = f.read()
            return json_str
```

</details>

<details><summary><b>UpdatePermissions - Docstring</b></summary>

```txt
Generates the mutation variables for updating permissions.

Args:
    originating_form_id: The ID of the originating form.
    target_form_id: The ID of the target form.
    originating_form_name: The name of the originating form.
    target_form_name: The name of the target form.

Returns:
    A list of strings containing the JSON data read from the permission files.
```

</details>
<details><summary><b>UpdatePermissions - Code</b></summary>

```python
                target_form_name: The name of the target form.

            Returns:
                A list of strings containing the JSON data read from the permission files.
            """
            directory = join(
                parent_dir, "kuali_skeletons", target_form_name, "permissions"
            )
            files = glob.glob(join(directory, "*.json"))
            json_strings = []
            for file in files:
                with open(file, "r") as f:
                    json_str = f.read()
                    json_strings.append(json_str)
            return json_strings
```

</details>

<details><summary><b>PublishTargetForm - Docstring</b></summary>

```txt
Generates the mutation variables for publishing a target form.

Args:
    target_form_id: The ID of the target form.

Returns:
    A JSON string containing the mutation variables.
```

</details>
<details><summary><b>PublishTargetForm - Code</b></summary>

```python
            return json.dumps({"id": target_form_id, "showDraft": False})
```

</details>

<details><summary><b>UpdateAppFavorite - Docstring</b></summary>

```txt
Generates the mutation variables for updating the favorite status of an app.

Args:
    target_form_id: The ID of the target form (application).

Returns:
    A JSON string containing the mutation variables.
```

</details>
<details><summary><b>UpdateAppFavorite - Code</b></summary>

```python
            return json.dumps({"applicationId": target_form_id, "isFavorite": True})
```

</details>

<details><summary><b>CreatePolicyGroup - Docstring</b></summary>

```txt
Generates the mutation variables for creating a policy group.

Args:
    target_form_id: The ID of the target form (application).
    role_name: The name of the role.

Returns:
    A JSON string containing the mutation variables.
```

</details>
<details><summary><b>CreatePolicyGroup - Code</b></summary>

```python
            return json.dumps({"applicationId": target_form_id, "name": role_name})
```

</details>

<details><summary><b>SearchSpaces - Docstring</b></summary>

```txt
Generates the query variables for searching spaces.

Args:
    search: The search query.

Returns:
    A JSON string containing the query variables.
```

</details>
<details><summary><b>SearchSpaces - Code</b></summary>

```python
            return json.dumps({"query": search})
```

</details>

<details><summary><b>GetGroupByKualiID - Docstring</b></summary>

```txt
Generates the query variables for retrieving a group by Kuali ID.

Args:
    group_id: The Kuali ID of the group.

Returns:
    A JSON string containing the query variables.
```

</details>
<details><summary><b>GetGroupByKualiID - Code</b></summary>

```python
            return json.dumps({"id": group_id})
```

</details>

<details><summary><b>PermissionsPage - Docstring</b></summary>

```txt
Generates the query variables for retrieving permissions page.

Args:
    appId: The ID of the application.

Returns:
    A JSON string containing the query variables.
```

</details>
<details><summary><b>PermissionsPage - Code</b></summary>

```python
            return json.dumps({"appId": appId})
```

</details>

<details><summary><b>GetUserByKualiID - Docstring</b></summary>

```txt
Generates the query variables for retrieving a user by Kuali ID.

Args:
    user_kuali_id: The Kuali ID of the user.

Returns:
    A JSON string containing the query variables.
```

</details>
<details><summary><b>GetUserByKualiID - Code</b></summary>

```python
            return json.dumps({"id": user_kuali_id})
```

</details>

<details><summary><b>GetUser - Docstring</b></summary>

```txt
Generates the query variables for retrieving a user.

Args:
    app_owner: The owner of the application.

Returns:
    A JSON string containing the query variables.
```

</details>
<details><summary><b>GetUser - Code</b></summary>

```python
            return json.dumps(
                {
                    "active": "ACTIVE",
                    "limit": 1,
                    "skip": 0,
                    "sort": [],
                    "q": app_owner,
                }
            )
```

</details>

<details><summary><b>ExtractApps - Docstring</b></summary>

```txt
Generates the query variables for extracting apps.

Returns:
    A JSON string containing the query variables.
```

</details>
<details><summary><b>ExtractApps - Code</b></summary>

```python
            return json.dumps({"query": ""})
```

</details>

<details><summary><b>ExtractIntegrationBodies - Docstring</b></summary>

```txt
Generates the query variables for extracting integration bodies.

Args:
    integrationId: The ID of the integration.

Returns:
    A JSON string containing the query variables.
```

</details>
<details><summary><b>ExtractIntegrationBodies - Code</b></summary>

```python
            return json.dumps({"integrationId": integrationId})
```

</details>

<details><summary><b>ExtractUserApiKeys - Docstring</b></summary>

```txt
Generates the query variables for extracting user API keys.

Args:
    user_id: The ID of the user.
    target_form_name: The name of the target form.

Returns:
    A JSON string containing the query variables.
```

</details>
<details><summary><b>ExtractUserApiKeys - Code</b></summary>

```python
            return json.dumps({"id": user_id})
```

</details>

<details><summary><b>ExtractForm - Docstring</b></summary>

```txt
Generates the query variables for extracting a form.

Args:
    originating_form_id: The ID of the originating form.

Returns:
    A JSON string containing the query variables.
```

</details>
<details><summary><b>ExtractForm - Code</b></summary>

```python
            return json.dumps({"id": originating_form_id})
```

</details>

<details><summary><b>ExtractFormSettings - Docstring</b></summary>

```txt
Generates the query variables for extracting form settings.

Args:
    originating_form_id: The ID of the originating form.

Returns:
    A JSON string containing the query variables.
```

</details>
<details><summary><b>ExtractFormSettings - Code</b></summary>

```python
            return json.dumps({"appId": originating_form_id})
```

</details>

<details><summary><b>ExtractWorkflows - Docstring</b></summary>

```txt
Generates the query variables for extracting workflows.

Args:
    originating_form_id: The ID of the originating form.

Returns:
    A JSON string containing the query variables.
```

</details>
<details><summary><b>ExtractWorkflows - Code</b></summary>

```python
            return json.dumps({"id": originating_form_id})
```

</details>

<details><summary><b>ExtractIntegration - Docstring</b></summary>

```txt
Generates the query variables for extracting an integration.

Args:
    integration_id: The ID of the integration.

Returns:
    A JSON string containing the query variables.
```

</details>
<details><summary><b>ExtractIntegration - Code</b></summary>

```python
            return json.dumps({"integrationId": integration_id})
```

</details>

<details><summary><b>ExtractPermissions - Docstring</b></summary>

```txt
Generates the query variables for extracting permissions.

Args:
    form_id: The ID of the form.

Returns:
    A JSON string containing the query variables.
```

</details>
<details><summary><b>ExtractPermissions - Code</b></summary>

```python
            return json.dumps({"appId": form_id})
```

</details>

<details><summary><b>ExtractDataSettings - Docstring</b></summary>

```txt
Generates the query variables for extracting data settings.

Args:
    originating_form_id: The ID of the originating form.

Returns:
    A JSON string containing the query variables.
```

</details>
<details><summary><b>ExtractDataSettings - Code</b></summary>

```python
            return json.dumps({"appId": originating_form_id, "query": ""})
```

</details>

### File: [publish_form.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/publish_form.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def publish_target_form(target_form_id, kuali_instance):
    """
    Publishes the target form using the provided target form ID and Kuali instance.

    Args:
        target_form_id: The ID of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.PublishForm()
    variables = KualiVariables.Mutations.PublishTargetForm(target_form_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = outcome

        if success_returned is not None:
            logger.success("Form published!")
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
```

</details>

### File: [reconstruct_integrations.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/reconstruct_integrations.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger
import aiofiles

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


def recursive_replace(obj, old_id, new_id):
    """
    Recursively replaces occurrences of the old ID with the new ID in the given object.

    Args:
        obj: The object to perform the replacement on.
        old_id: The old ID to be replaced.
        new_id: The new ID to replace with.

    Returns:
        The modified object with the replacements.
    """
    if isinstance(obj, dict):
        for k, v in obj.items():
            if v == old_id:
                obj[k] = new_id
            else:
                recursive_replace(v, old_id, new_id)
    elif isinstance(obj, list):
        for idx, item in enumerate(obj):
            if item == old_id:
                obj[idx] = new_id
            else:
                recursive_replace(item, old_id, new_id)
    return obj


async def reconstruct_integrations(
    target_form_name, target_space_id, kuali_from, kuali_to
):
    """
    Reconstructs integrations for a target form using the provided target form name, target space ID,
    and Kuali instances for the source and destination.

    Args:
        target_form_name: The name of the target form.
        target_space_id: The ID of the target space.
        kuali_from: The Kuali instance for the source.
        kuali_to: The Kuali instance for the destination.

    Returns:
        None.
    """
    file_path_begin = join(parent_dir, "kuali_skeletons", target_form_name)
    with open(join(file_path_begin, "integrations.json"), "r") as f:
        integrations = json.load(f)
    index = 1
    for idx, integration in enumerate(integrations):
        integration_id = integration.get("integrationId")
        url = kuali_from.url()
        headers = kuali_from.headers()
        graphql = KualiGraphQL.Queries.ExtractIntegrationBodies()
        variables = KualiVariables.Queries.ExtractIntegrationBodies(integration_id)
        outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
        try:
            integration_data = (
                outcome.get("data", {}).get("integration", {}).get("data")
            )

            if "__legacyType" in integration_data:
                logger.info(
                    f"Integration {integration_id} is a legacy integration and will not be added to the form."
                )
                continue

            auth_type = integration_data.get("__authenticationType", {}).get("id")
            if auth_type == "basic" and "__password" in integration_data:
                integration_data["__password"] = "PLACEHOLDER_PASSWORD_VALUE"
            if "__spaceId" in integration_data:
                del integration_data["__spaceId"]

            variables = {"spaceId": target_space_id, "data": integration_data}

            with open(
                join(
                    file_path_begin,
                    "integrations",
                    f"integration_body_{index}.json",
                ),
                "w",
            ) as f:
                f.write(json.dumps(variables, indent=4))
            index += 1

            url = kuali_to.url()
            headers = kuali_to.headers()
            graphql = KualiGraphQL.Mutations.CreateIntegration()
            integration_outcome = await Kuali.FetchResponse(
                url, headers, graphql, variables
            )

            new_integration_id = (
                integration_outcome.get("data", {})
                .get("createIntegration", {})
                .get("id")
            )
            logger.info(f"new integration: {new_integration_id}")

            if new_integration_id:
                integrations[idx]["integrationId"] = new_integration_id

                with open(join(file_path_begin, "form.json"), "r") as f:
                    form_data = json.load(f)
                form_data = recursive_replace(
                    form_data, integration_id, new_integration_id
                )
                with open(join(file_path_begin, "form.json"), "w") as f:
                    json.dump(form_data, f, indent=4)

                with open(join(file_path_begin, "workflow.json"), "r") as f:
                    workflow_data = json.load(f)
                workflow_data = recursive_replace(
                    workflow_data, integration_id, new_integration_id
                )
                with open(join(file_path_begin, "workflow.json"), "w") as f:
                    json.dump(workflow_data, f, indent=4)

                integration_file_path = join(
                    file_path_begin,
                    "integrations",
                    f"integration_{index}.json",
                )
                try:
                    with open(integration_file_path, "r") as f:
                        integration_data = json.load(f)
                    integration_data["id"] = new_integration_id

                    with open(integration_file_path, "w") as f:
                        json.dump(integration_data, f, indent=4)

                except FileNotFoundError:
                    logger.debug(
                        f"Integration ID: {integration_id} is not an integration."
                    )
                except Exception as oops:
                    logger.error(
                        f"Error updating id in {integration_file_path}: {str(oops)}"
                    )

        except FileNotFoundError:
            logger.info(f"Skipping over faux integration {integration_id}")
        except Exception as oops:
            logger.error(f"Error: {str(oops)}")

    with open(join(file_path_begin, "integrations.json"), "w") as f:
        json.dump(integrations, f, indent=4)
```

</details>

<details><summary><b>recursive_replace - Docstring</b></summary>

```txt
Recursively replaces occurrences of the old ID with the new ID in the given object.

Args:
    obj: The object to perform the replacement on.
    old_id: The old ID to be replaced.
    new_id: The new ID to replace with.

Returns:
    The modified object with the replacements.
```

</details>
<details><summary><b>recursive_replace - Code</b></summary>

```python
    if isinstance(obj, dict):
        for k, v in obj.items():
            if v == old_id:
                obj[k] = new_id
            else:
                recursive_replace(v, old_id, new_id)
    elif isinstance(obj, list):
        for idx, item in enumerate(obj):
            if item == old_id:
                obj[idx] = new_id
            else:
                recursive_replace(item, old_id, new_id)
    return obj
```

</details>

### File: [search_apps.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/search_apps.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


def flatten_data(data):
    """
    Flattens the provided data by extracting items from the 'searchSpaceContents' field
    and returning a flattened list of items.

    Args:
        data: The data to flatten.

    Returns:
        A flattened list of items extracted from the 'searchSpaceContents' field.
    """
    flattened_list = []
    try:
        items = data.get("data", {}).get("searchSpaceContents", [])
        flattened_list.extend(flatten_item(item) for item in items)
    except Exception as e:
        logger.error(f"Error flattening data: {e}")
    return flattened_list


def flatten_item(item):
    """
    Flattens the provided item by extracting specific fields and returning a dictionary
    containing the flattened fields.

    Args:
        item: The item to flatten.

    Returns:
        A dictionary containing the flattened fields of the item.
    """
    return {
        "createdBy_displayName": item.get("createdBy", {}).get("displayName", ""),
        "createdBy_id": item.get("createdBy", {}).get("id", ""),
        "createdBy_name": item.get("createdBy", {}).get("name", ""),
        "createdBy_schoolId": item.get("createdBy", {}).get("schoolId", ""),
        "createdBy_username": item.get("createdBy", {}).get("username", ""),
        "id": item.get("id", ""),
        "name": item.get("name", ""),
        "space_id": item.get("space", {}).get("id", ""),
        "space_name": item.get("space", {}).get("name", ""),
        "tileOptions_backgroundColor": item.get("tileOptions", {}).get(
            "backgroundColor", ""
        ),
        "tileOptions_iconName": item.get("tileOptions", {}).get("iconName", ""),
        "type": item.get("type", ""),
    }


async def search_for_app(search, kuali_instance):
    """
    Searches for an app using the provided search query and Kuali instance.
    It fetches the response from the search and returns the flattened data.

    Args:
        search: The search query.
        kuali_instance: The Kuali instance.

    Returns:
        A list of dictionaries containing the flattened data of the search results.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Queries.SearchSpaces()
    variables = KualiVariables.Queries.SearchSpaces(search)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        return flatten_data(outcome)
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
    try:
        outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
        return flatten_data(outcome)
    except Exception as oops:
        logger.error(f"Error: {str(oops)}")
```

</details>

<details><summary><b>flatten_data - Docstring</b></summary>

```txt
Flattens the provided data by extracting items from the 'searchSpaceContents' field
and returning a flattened list of items.

Args:
    data: The data to flatten.

Returns:
    A flattened list of items extracted from the 'searchSpaceContents' field.
```

</details>
<details><summary><b>flatten_data - Code</b></summary>

```python
    flattened_list = []
    try:
        items = data.get("data", {}).get("searchSpaceContents", [])
        flattened_list.extend(flatten_item(item) for item in items)
    except Exception as e:
        logger.error(f"Error flattening data: {e}")
    return flattened_list
```

</details>

<details><summary><b>flatten_item - Docstring</b></summary>

```txt
Flattens the provided item by extracting specific fields and returning a dictionary
containing the flattened fields.

Args:
    item: The item to flatten.

Returns:
    A dictionary containing the flattened fields of the item.
```

</details>
<details><summary><b>flatten_item - Code</b></summary>

```python
    return {
        "createdBy_displayName": item.get("createdBy", {}).get("displayName", ""),
        "createdBy_id": item.get("createdBy", {}).get("id", ""),
        "createdBy_name": item.get("createdBy", {}).get("name", ""),
        "createdBy_schoolId": item.get("createdBy", {}).get("schoolId", ""),
        "createdBy_username": item.get("createdBy", {}).get("username", ""),
        "id": item.get("id", ""),
        "name": item.get("name", ""),
        "space_id": item.get("space", {}).get("id", ""),
        "space_name": item.get("space", {}).get("name", ""),
        "tileOptions_backgroundColor": item.get("tileOptions", {}).get(
            "backgroundColor", ""
        ),
        "tileOptions_iconName": item.get("tileOptions", {}).get("iconName", ""),
        "type": item.get("type", ""),
    }
```

</details>

### File: [update_favorite.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/update_favorite.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def update_target_form_favorite(
    target_form_name, target_form_id, kuali_instance, user_auth_token=None
):
    """
    Updates the favorite status of the target form using the provided target form name, ID, Kuali instance,
    and optional user authentication token. It sends a GraphQL mutation request to update the favorite status.

    Args:
        target_form_name: The name of the target form.
        target_form_id: The ID of the target form.
        kuali_instance: The Kuali instance.
        user_auth_token: Optional user authentication token.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers(user_auth_token)
    graphql = KualiGraphQL.Mutations.UpdateAppFavorite()
    variables = KualiVariables.Mutations.UpdateAppFavorite(target_form_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = (
            outcome.get("data", {}).get("updateAppFavorite", {}).get("isFavorite")
        )

        if success_returned is not None:
            logger.success(f"{target_form_name} form made favorite")
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
```

</details>

### File: [update_form.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/update_form.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def update_target_form(
    originating_form_id,
    target_form_id,
    originating_form_name,
    target_form_name,
    kuali_instance,
):
    """
    Updates the target form using the provided originating form ID, target form ID,
    originating form name, target form name, and Kuali instance.
    It sends a GraphQL mutation request to update the form.

    Args:
        originating_form_id: The ID of the originating form.
        target_form_id: The ID of the target form.
        originating_form_name: The name of the originating form.
        target_form_name: The name of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.UpdateForm()
    variables = KualiVariables.Mutations.UpdateForm(
        originating_form_id,
        target_form_id,
        originating_form_name,
        target_form_name,
    )
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = outcome

        if success_returned is not None:
            logger.success("Form updated!")
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
```

</details>

### File: [update_integrations.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/update_integrations.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def update_form_integrations(
    originating_form_id,
    target_form_id,
    originating_form_name,
    target_form_name,
    kuali_instance,
):
    """
    Updates the integrations of the target form using the provided originating form ID,
    target form ID, originating form name, target form name, and Kuali instance.
    It reads the integrations from a JSON file, iterates over each integration,
    and sends GraphQL mutation requests to update the integrations.

    Args:
        originating_form_id: The ID of the originating form.
        target_form_id: The ID of the target form.
        originating_form_name: The name of the originating form.
        target_form_name: The name of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    with open(
        join(
            parent_dir,
            "kuali_skeletons",
            target_form_name,
            "integrations.json",
        ),
        "r",
    ) as f:
        integrations = json.load(f)

    index = 1
    for integration in integrations:
        integration_id = integration.get("integrationId")
        integration_name = integration.get("integrationName")
        if integration_id and integration_name:
            url = kuali_instance.url()
            headers = kuali_instance.headers()
            graphql = KualiGraphQL.Mutations.UpdateIntegration()
            try:
                with open(
                    join(
                        parent_dir,
                        "kuali_skeletons",
                        target_form_name,
                        "integrations",
                        f"integration_{index}.json",
                    ),
                    "r",
                ) as f:
                    variables = json.load(f)

                # If "sharingType" is "ALL", skip it and log the situation
                if variables.get("sharingType", "") == "ALL":
                    logger.info(
                        f"{integration_name} is open to all apps, no need to give permissions to {target_form_name}"
                    )
                    index += 1
                    continue

                # Remove duplicates from "apps" just in case..
                apps = variables.get("apps", [])
                seen = set()
                variables["apps"] = [
                    app
                    for app in apps
                    if not (app["id"] in seen or seen.add(app["id"]))
                ]

                outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
                success_returned = outcome
                if success_returned is not None:
                    logger.success(
                        f"{integration_name} = successfully added to {target_form_name}"
                    )
                    index += 1
                else:
                    raise KeyError
            except FileNotFoundError:
                logger.info(f"Skipping over faux integration {integration_id}")
                continue
            except Exception as oops:
                logger.error(f"Error: {str(oops)}")
```

</details>

### File: [update_permissions.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/update_permissions.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def update_form_permissions(
    originating_form_id,
    target_form_id,
    originating_form_name,
    target_form_name,
    kuali_instance,
):
    """
    Updates the permissions of the target form using the provided originating form ID,
    target form ID, originating form name, target form name, and Kuali instance.
    It sends GraphQL mutation requests to update the permissions based on the provided JSON strings.

    Args:
        originating_form_id: The ID of the originating form.
        target_form_id: The ID of the target form.
        originating_form_name: The name of the originating form.
        target_form_name: The name of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    json_strings = KualiVariables.Mutations.UpdatePermissions(
        originating_form_id,
        target_form_id,
        originating_form_name,
        target_form_name,
    )
    try:
        for json_str in json_strings:
            variables = json.loads(json_str)
            logger.info(
                f"update_form_permissions variables: {json.dumps(variables, indent=4)}"
            )
            url = kuali_instance.url()
            headers = kuali_instance.headers()
            graphql = KualiGraphQL.Mutations.UpdatePermissions()
            outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
            try:
                logger.info(
                    f"update_form_permissions outcome: {json.dumps(outcome, indent=4)}"
                )
                success_returned = outcome
            except Exception as oops:
                logger.info(f"Error: {str(oops)}")

        logger.success("Permissions updated!")

    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
```

</details>

### File: [update_settings.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/update_settings.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def update_target_form_settings(form_settings, kuali_instance):
    """
    Updates the settings of the target form using the provided form settings and Kuali instance.
    It sends a GraphQL mutation request to update the form settings.

    Args:
        form_settings: The settings of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.UpdateFormSettings()
    variables = KualiVariables.Mutations.UpdateFormSettings(form_settings)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        if outcome is not None:
            logger.success("Form settings updated!")
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")


async def update_target_form_data_settings(data_settings, kuali_instance):
    """
    Updates the data settings of the target form using the provided data settings and Kuali instance.
    It sends a GraphQL mutation request to update the data settings.

    Args:
        data_settings: The data settings of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.UpdateDataSettings()
    variables = KualiVariables.Mutations.UpdateDataSettings(data_settings)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        if outcome is not None:
            logger.success("Data settings updated!")
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
```

</details>

### File: [update_token.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/update_token.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def revoke_target_user_token(
    user_id,
    target_form_name,
    user_name,
    user_auth_token_id_list_json,
    kuali_instance,
):
    """
    Revokes the target user's temporary authentication token associated with the specified user ID,
    target form name, user name, user authentication token ID list JSON, and Kuali instance.
    It sends a GraphQL mutation request to revoke the token.

    Args:
        user_id: The ID of the target user.
        target_form_name: The name of the target form.
        user_name: The name of the user.
        user_auth_token_id_list_json: The JSON string representing the user authentication token ID list.
        kuali_instance: The Kuali instance.

    Returns:
        The outcome of the token revocation request.
    """
    user_auth_token_id_list = json.loads(user_auth_token_id_list_json)
    if user_auth_token_id_list is not None:
        for user_auth_token in user_auth_token_id_list:
            if user_auth_token["name"] == target_form_name:
                url = kuali_instance.url()
                headers = kuali_instance.headers()
                graphql = KualiGraphQL.Mutations.RevokeToken()
                variables = KualiVariables.Mutations.RevokeToken(
                    user_auth_token["id"], user_id
                )
                outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
                try:
                    success_returned = outcome

                    if success_returned is not None:
                        logger.success(
                            f"{user_name} temp auth token removed! - proof: {success_returned}"
                        )
                        return outcome
                    else:
                        raise KeyError
                except Exception as oops:
                    logger.info(f"Error: {str(oops)}")
```

</details>

### File: [update_workflow.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/modules/update_workflow.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def update_target_workflow(
    originating_form_id,
    target_form_id,
    originating_form_name,
    target_form_name,
    kuali_instance,
):
    """
    Updates the workflow of the target form using the provided originating form ID,
    target form ID, originating form name, target form name, and Kuali instance.
    It sends a GraphQL mutation request to update the workflow.

    Args:
        originating_form_id: The ID of the originating form.
        target_form_id: The ID of the target form.
        originating_form_name: The name of the originating form.
        target_form_name: The name of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.UpdateWorkflow()
    variables = KualiVariables.Mutations.UpdateWorkflow(
        originating_form_id,
        target_form_id,
        originating_form_name,
        target_form_name,
    )
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = outcome

        if success_returned is not None:
            logger.success("Workflow updated!")
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
```

</details>

## Directory: GraphQL

## Directory: utilities

### File: [blocked_ip_models.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/utilities/blocked_ip_models.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
from sqlalchemy import create_engine, Column, Integer, String, DateTime, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.future import select
import asyncio

DATABASE_URL = "sqlite+aiosqlite:///./blocked_ips.db"
engine = create_async_engine(
    DATABASE_URL, echo=False, future=True, connect_args={"check_same_thread": False}
)
SessionLocal = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)

Base = declarative_base()


class BlockedIP(Base):
    """
    Represents a model for storing blocked IP addresses.

    Attributes:
        ip_address (str): The IP address of the blocked IP.
        attempts (int): The number of attempts made from the IP address.
        block_until (datetime): The date until which the IP address is blocked.
    """
    __tablename__ = "blocked_ips"

    ip_address = Column(String, primary_key=True, index=True)
    attempts = Column(Integer, default=0)
    block_until = Column(DateTime, nullable=True)


# Create the database tables
async def create_tables():
    """
    Creates the database tables.
    """
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
```

</details>

<details><summary><b>BlockedIP - Docstring</b></summary>

```txt
Represents a model for storing blocked IP addresses.

Attributes:
    ip_address (str): The IP address of the blocked IP.
    attempts (int): The number of attempts made from the IP address.
    block_until (datetime): The date until which the IP address is blocked.
```

</details>
<details><summary><b>BlockedIP - Code</b></summary>

```python
    __tablename__ = "blocked_ips"

    ip_address = Column(String, primary_key=True, index=True)
    attempts = Column(Integer, default=0)
    block_until = Column(DateTime, nullable=True)
```

</details>

### File: [kuali_environment.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/utilities/kuali_environment.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
from os import getenv
from dotenv import load_dotenv
from base64 import b64decode
import binascii

load_dotenv(verbose=True)


def is_base64(s):
    """
    Checks if a string is a valid base64 encoded value.

    Args:
        s (str): The string to be checked.

    Returns:
        Union[bytes, None]: The decoded bytes if the string is a valid base64 encoded value, otherwise None.
    """
    try:
        return b64decode(s, validate=True)
    except binascii.Error:
        return None


def get_env_variables(env_label: str):
    """
    Retrieves environment variables based on the provided environment label.

    Args:
        env_label (str): The label of the environment.

    Returns:
        Dict[str, str]: A dictionary containing the retrieved environment variables.

    Raises:
        ValueError: If the environment label is unknown.
    """
    if env_label == "production":
        auth_token = getenv("AUTH_TOKEN_PRD")
        instance = getenv("KUALI_INSTANCE_PRD")
    elif env_label == "sandbox":
        auth_token = getenv("AUTH_TOKEN_SBX")
        instance = getenv("KUALI_INSTANCE_SBX")
    elif env_label == "staging":
        auth_token = getenv("AUTH_TOKEN_STG")
        instance = getenv("KUALI_INSTANCE_STG")
    else:
        raise ValueError(f"Unknown environment label: {env_label}")

    decoded_auth_token = is_base64(auth_token)
    if decoded_auth_token is not None:
        auth_token = decoded_auth_token.decode("utf-8")

    return {
        "auth_token": auth_token,
        "instance": instance,
    }
```

</details>

<details><summary><b>is_base64 - Docstring</b></summary>

```txt
Checks if a string is a valid base64 encoded value.

Args:
    s (str): The string to be checked.

Returns:
    Union[bytes, None]: The decoded bytes if the string is a valid base64 encoded value, otherwise None.
```

</details>
<details><summary><b>is_base64 - Code</b></summary>

```python
    try:
        return b64decode(s, validate=True)
    except binascii.Error:
        return None
```

</details>

<details><summary><b>get_env_variables - Docstring</b></summary>

```txt
Retrieves environment variables based on the provided environment label.

Args:
    env_label (str): The label of the environment.

Returns:
    Dict[str, str]: A dictionary containing the retrieved environment variables.

Raises:
    ValueError: If the environment label is unknown.
```

</details>
<details><summary><b>get_env_variables - Code</b></summary>

```python
    if env_label == "production":
        auth_token = getenv("AUTH_TOKEN_PRD")
        instance = getenv("KUALI_INSTANCE_PRD")
    elif env_label == "sandbox":
        auth_token = getenv("AUTH_TOKEN_SBX")
        instance = getenv("KUALI_INSTANCE_SBX")
    elif env_label == "staging":
        auth_token = getenv("AUTH_TOKEN_STG")
        instance = getenv("KUALI_INSTANCE_STG")
    else:
        raise ValueError(f"Unknown environment label: {env_label}")

    decoded_auth_token = is_base64(auth_token)
    if decoded_auth_token is not None:
        auth_token = decoded_auth_token.decode("utf-8")

    return {
        "auth_token": auth_token,
        "instance": instance,
    }
```

</details>

### File: [main.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/utilities/main.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
import os
import sys
from typing import Optional
from loguru import logger
from dotenv import load_dotenv

# sys.path.insert(0, "./modules")
import modules as mod

# from modules.graphql_calls import Kuali


async def create_new_form(
    to_environment: str,
    user_duid: str,
    kuali_user_id: str,
    from_form_id: str,
    form_name: str,
    target_form_name: str,
    source_kuali: mod.Kuali,
    target_kuali: mod.Kuali,
    icon_color: str,
    target_space_id: str,
    to_form_id: Optional[str],
    icon_name: str,
):
    """
    Creates a new form in the target environment based on the provided parameters.
    It performs a series of operations including user provisioning, permission checks,
    form creation, settings and data settings extraction and updates, workflow updates,
    integration extraction and updates, permission extraction and updates, and form publishing.

    Args:
        to_environment: The target environment ("production", "sandbox", "staging").
        user_duid: The DUID of the user.
        kuali_user_id: The ID of the Kuali user.
        from_form_id: The ID of the originating form.
        form_name: The name of the form.
        target_form_name: The name of the target form.
        source_kuali: The source Kuali instance.
        target_kuali: The target Kuali instance.
        icon_color: The color of the form icon.
        target_space_id: The ID of the target space.
        to_form_id: The ID of the target form (optional).
        icon_name: The name of the form icon.

    Returns:
        None.
    """
    # Ensure target user provisioned
    new_env_user = await mod.search_for_user(kuali_user_id, source_kuali, target_kuali)

    # Ensure target user can create apps
    await mod.add_owner_to_build_group(to_environment, new_env_user["id"], target_kuali)

    # Extracts target user info
    user_id = await mod.extract_target_user_info(user_duid, target_kuali)

    # Publishes originating form
    await mod.publish_originating_form(from_form_id, source_kuali)

    # Creates target user token
    auth_token = await mod.create_target_user_token(
        user_id["id"], form_name, user_id["displayName"], target_kuali
    )

    # Formats target user token
    user_auth_token = f"Bearer {auth_token}"

    # Creates target form
    target_form_id = await mod.create_target_form(
        target_form_name,
        target_space_id,
        user_id["id"],
        icon_name,
        icon_color,
        user_auth_token,
        user_id["displayName"],
        target_kuali,
    )

    # Updates target form favorite
    await mod.update_target_form_favorite(
        form_name, target_form_id, target_kuali, user_auth_token
    )

    # Extracts target user token
    user_auth_token_id_list_json = await mod.extract_target_user_token(
        user_id["id"], target_form_name, target_kuali
    )

    # Revokes target user token
    await mod.revoke_target_user_token(
        user_id["id"],
        form_name,
        user_id["displayName"],
        user_auth_token_id_list_json,
        target_kuali,
    )

    # Publishes target form
    await mod.publish_target_form(target_form_id, target_kuali)

    # Extracts originating form
    await mod.extract_originating_form(
        from_form_id, target_form_id, form_name, target_form_name, source_kuali
    )

    # Extracts originating workflow
    await mod.extract_originating_workflow(
        from_form_id, target_form_id, form_name, target_form_name, source_kuali
    )

    # Extracts form integrations
    await mod.extract_form_integrations(
        from_form_id,
        target_form_id,
        form_name,
        target_form_name,
        source_kuali,
        target_kuali,
    )

    # Extracts originating form settings
    form_settings = await mod.extract_originating_form_settings(
        from_form_id, source_kuali
    )

    # Extracts originating form data settings
    data_settings = await mod.extract_originating_form_data_settings(
        from_form_id, source_kuali
    )

    # Updates target form data settings
    await mod.update_target_form_data_settings(data_settings, target_kuali)

    # Updates target form settings
    await mod.update_target_form_settings(form_settings, target_kuali)

    # Reconstructs integrations if source and target Kuali instances are different
    if source_kuali != target_kuali:
        await mod.reconstruct_integrations(
            target_form_name, target_space_id, source_kuali, target_kuali
        )

    # Updates target workflow
    await mod.update_target_workflow(
        from_form_id, target_form_id, form_name, target_form_name, target_kuali
    )
    # Updates form integrations
    await mod.update_form_integrations(
        from_form_id, target_form_id, form_name, target_form_name, target_kuali
    )

    # Extracts form permissions
    await mod.extract_and_update_permissions(
        from_form_id,
        target_form_id,
        form_name,
        target_form_name,
        source_kuali,
        target_kuali,
    )

    # Updates target form
    await mod.update_target_form(
        from_form_id, target_form_id, form_name, target_form_name, target_kuali
    )

    # Updates form permissions
    await mod.update_form_permissions(
        from_form_id, target_form_id, form_name, target_form_name, target_kuali
    )

    # Publishes target form
    await mod.publish_target_form(target_form_id, target_kuali)

    # await mod.delete_tests(
    #     target_form_name, target_kuali
    # )  # Deletes the target form for testing purposes
```

</details>

### File: [middleware.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/utilities/middleware.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
# Standard library imports
from time import time
from datetime import datetime, timedelta

# Third-party imports
from fastapi import Request, FastAPI, HTTPException
from fastapi.responses import JSONResponse, HTMLResponse
from starlette.exceptions import HTTPException as StarletteHTTPException
from loguru import logger
from Secweb import SecWeb

from sqlalchemy.future import select
from sqlalchemy import update


from utilities.blocked_ip_models import BlockedIP
from utilities.blocked_ip_models import SessionLocal

BLOCK_THRESHOLD = 5
BLOCK_DURATION = timedelta(minutes=60)  # durrrrrrration

# code ranges
SUCCESS_STATUS_CODES = range(200, 300)
CLIENT_ERROR_STATUS_CODES = range(400, 500)
SERVER_ERROR_STATUS_CODES = range(500, 600)


async def is_ip_blocked(ip_address: str) -> (bool, int):
    """
    Checks if the given IP address is blocked by querying the database for a matching BlockedIP record.
    It returns a tuple indicating whether the IP is blocked and the number of attempts made.

    Args:
        ip_address: The IP address to check.

    Returns:
        A tuple containing a boolean indicating if the IP is blocked and the number of attempts made.
    """
    async with SessionLocal() as session:
        result = await session.execute(
            select(BlockedIP).where(BlockedIP.ip_address == ip_address)
        )
        if blocked_ip := result.scalars().first():
            is_blocked = (
                blocked_ip.block_until and blocked_ip.block_until > datetime.now()
            )
            return is_blocked, blocked_ip.attempts
        return False, 0


async def log_failed_attempt(ip_address: str):
    """
    Logs a failed attempt for the specified IP address by updating the attempts count in the database.
    If the IP address is not found in the database, a new BlockedIP record is created.

    Args:
        ip_address: The IP address of the failed attempt.

    Returns:
        None.
    """
    async with SessionLocal() as session:
        result = await session.execute(
            select(BlockedIP).where(BlockedIP.ip_address == ip_address)
        )
        if blocked_ip := result.scalars().first():
            blocked_ip.attempts += 1
        else:
            new_ip = BlockedIP(ip_address=ip_address, attempts=1)
            session.add(new_ip)
        await session.commit()


async def update_block_status(ip_address: str):
    """
    Updates the block status of the specified IP address by setting the block_until field to the current time plus the block duration.
    It updates the BlockedIP record in the database based on the specified IP address and block threshold.

    Args:
        ip_address: The IP address to update.

    Returns:
        None.
    """
    block_until = datetime.now() + BLOCK_DURATION
    async with SessionLocal() as session:
        await session.execute(
            update(BlockedIP)
            .where(
                BlockedIP.ip_address == ip_address,
                BlockedIP.attempts >= BLOCK_THRESHOLD,
            )
            .values(block_until=block_until)
        )
        await session.commit()


def process_headers(headers):
    """
    Processes the headers dictionary and returns a new dictionary containing only the required headers.

    Args:
        headers: The headers dictionary.

    Returns:
        A new dictionary containing only the required headers.
    """
    required_headers = [
        "referer",
        "x-kuali-origin",
        "x-kuali-user-id",
        "x-kuali-user-ssoid",
    ]
    return {header: headers.get(header) for header in required_headers}


async def log_request_response(
    client_ip, client_host, referer, query_string, process_time, path, status_code
):
    """
    Logs the request and response information based on the provided parameters.
    It formats the log message based on the status code and logs it at the appropriate level.

    Args:
        client_ip: The client IP address.
        client_host: The client host.
        referer: The referer information.
        query_string: The query string.
        process_time: The process time.
        path: The path of the request.
        status_code: The status code of the response.

    Returns:
        None.
    """
    log_format = f"{client_ip} | {client_host} | {referer} | {query_string} | {process_time} | {path} | {status_code}"
    if status_code in SUCCESS_STATUS_CODES:
        logger.success(f"SUCCESS | {log_format}")
    elif status_code in CLIENT_ERROR_STATUS_CODES:
        logger.error(f"CLIENT ERROR | {log_format}")
    elif status_code in SERVER_ERROR_STATUS_CODES:
        logger.critical(f"SERVER ERROR | {log_format}")


# midware
async def middlewares_stuffs(request: Request, call_next):
    """
    Performs various middleware operations such as calculating process time, updating response headers,
    extracting client IP and referer information, and logging the request and response information.

    Args:
        request: The request object.
        call_next: The next middleware or endpoint function.

    Returns:
        The response object.
    """
    start_time = time()
    response = await call_next(request)
    headers = process_headers(request.headers)
    process_time = timedelta(seconds=time() - start_time)
    response.headers["X-Process-Time"] = str(process_time)
    client_host = f"Host: {request.client.host}"
    get_client_ip = (
        request.headers.get("x-forwarded-for", request.client.host)
        .split(",")[0]
        .strip()
    )
    client_ip = f"X-Forwarded-For: {get_client_ip}"
    referer = (
        headers.get("referer")
        or f"{headers.get('x-kuali-origin')} | {headers.get('x-kuali-user-id')} | {headers.get('x-kuali-user-ssoid')}"
    )
    query_string = request.query_params._list
    await log_request_response(
        client_ip,
        client_host,
        referer,
        query_string,
        process_time,
        request.url.path,
        response.status_code,
    )
    return response


# Exception handler
async def consuela_http_exception_handler(
    request: Request, exc: StarletteHTTPException
):
    """
    Handles HTTP exceptions in the Consuela middleware.

    Args:
        request (Request): The incoming request object.
        exc (StarletteHTTPException): The raised HTTP exception.

    Returns:
        Union[JSONResponse, HTMLResponse]: The response to be returned.
    """
    client_ip = request.client.host
    is_blocked, attempt_count = await is_ip_blocked(client_ip)
    await log_failed_attempt(client_ip)
    await update_block_status(client_ip)
    if is_blocked:
        logger.warning(
            f"Blocked IP {client_ip} attempted to access {request.url.path}. Attempt count: {attempt_count}"
        )
        return JSONResponse(
            status_code=403,
            content={
                "detail": f"Access from your IP address has been blocked. Attempt count: {attempt_count}"
            },
        )
    logger.warning(
        f"Unauthorized access attempted on endpoint '{request.url.path}' by {client_ip}"
    )
    if request.headers.get("Accept", "").startswith("application/json"):
        return JSONResponse(
            status_code=exc.status_code,
            content={
                "error": "Unauthorized, these are not the droids you are looking for."
            },
        )
    else:
        return HTMLResponse(
            status_code=exc.status_code,
            content=f"<html><body><h1>{exc.status_code} {exc.detail}</h1><p>Well.. go on now, get</p></body></html>",
        )


def configure_middlewares(app: FastAPI):
    """
    Configures the middlewares for the FastAPI application.

    Args:
        app (FastAPI): The FastAPI application instance.

    Examples:
        >>> configure_middlewares(app)
    """
    app.middleware("http")(middlewares_stuffs)
    app.add_exception_handler(StarletteHTTPException, consuela_http_exception_handler)
    # SecWeb(app=app)
```

</details>

<details><summary><b>process_headers - Docstring</b></summary>

```txt
Processes the headers dictionary and returns a new dictionary containing only the required headers.

Args:
    headers: The headers dictionary.

Returns:
    A new dictionary containing only the required headers.
```

</details>
<details><summary><b>process_headers - Code</b></summary>

```python
    required_headers = [
        "referer",
        "x-kuali-origin",
        "x-kuali-user-id",
        "x-kuali-user-ssoid",
    ]
    return {header: headers.get(header) for header in required_headers}
```

</details>

<details><summary><b>configure_middlewares - Docstring</b></summary>

```txt
Configures the middlewares for the FastAPI application.

Args:
    app (FastAPI): The FastAPI application instance.

Examples:
    >>> configure_middlewares(app)
```

</details>
<details><summary><b>configure_middlewares - Code</b></summary>

```python
    app.middleware("http")(middlewares_stuffs)
    app.add_exception_handler(StarletteHTTPException, consuela_http_exception_handler)
```

</details>

### File: [task_processor.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/utilities/task_processor.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
import os
import sys
import asyncio
import httpx
import logging
from fastapi import Request
from loguru import logger

from utilities.main import create_new_form
from utilities.kuali_environment import get_env_variables

import modules as mod

MAX_RETRIES = 3
SLEEP_DURATION = 5


async def process_create_form_task(
    to_environment: str,
    request: Request,
    user_duid: str,
    kuali_user_id: str,
    from_form_id: str,
    form_name: str,
    target_form_name: str,
    source_kuali: mod.Kuali,
    target_kuali: mod.Kuali,
    icon_name: str,
    icon_color: str,
):
    """
    Processes the task to create a new form in the target environment based on the provided parameters.
    It calls the create_new_form function to perform the necessary operations and sends the response back.

    Args:
        to_environment: The target environment ("production", "sandbox", "staging", or other).
        request: The request object.
        user_duid: The DUID of the user.
        kuali_user_id: The ID of the Kuali user.
        from_form_id: The ID of the originating form.
        form_name: The name of the form.
        target_form_name: The name of the target form.
        source_kuali: The source Kuali instance.
        target_kuali: The target Kuali instance.
        icon_name: The name of the form icon.
        icon_color: The color of the form icon.

    Returns:
        None.
    """
    await create_new_form(
        to_environment,
        user_duid,
        kuali_user_id,
        from_form_id,
        form_name,
        target_form_name,
        source_kuali,
        target_kuali,
        icon_color,
        target_space_id="5e47518b90adda9474c14adb",
        to_form_id=None,
        icon_name="app-window-check.svg",
    )
    await send_response_back(request)


async def send_response_back(request: Request):
    """
    Sends the response back to the specified response URL in the request headers.
    It retries the request with exponential backoff in case of HTTP errors or other exceptions.

    Args:
        request: The request object.

    Returns:
        None.
    """
    retries = 0
    while retries < MAX_RETRIES:
        try:
            if response_url := request.headers.get("X-Response-URL"):
                logger.info(f"Found response URL: {response_url}")
                headers = {"X-Status-Code": "201"}

                async with httpx.AsyncClient(http2=True, timeout=60.0) as client:
                    logger.info("Posting to response URL.")
                    response = await client.post(response_url, headers=headers)
                    response.raise_for_status()
                    logger.info(
                        f"Response status code: {response.status_code} {response.text}"
                    )
                    break
            else:
                logger.info("No X-Response-URL header found.")
                break
        except httpx.HTTPStatusError as e:
            retries += 1
            logger.error(
                f"HTTP Status Error: {e}. Attempt {retries} failed, retrying in {SLEEP_DURATION} seconds..."
            )
            await asyncio.sleep(SLEEP_DURATION)
        except Exception as e:
            retries += 1
            logger.error(
                f"An error occurred: {e}. Attempt {retries} failed, retrying in {SLEEP_DURATION} seconds..."
            )
            await asyncio.sleep(SLEEP_DURATION)

    if retries == MAX_RETRIES:
        logger.error(f"Failed to send response after {MAX_RETRIES} attempts.")
```

</details>

### File: [test.py](https://gitlab.oit.duke.edu/kuali-build/kb-form-migration/blob/trunk/utilities/test.py)

<details><summary><b>Module - Docstring</b></summary>

```txt
None
```

</details>
<details><summary><b>Module - Code</b></summary>

```python
from os import getenv
import httpx
import pytest
from httpx import BasicAuth
from dotenv import load_dotenv

load_dotenv(verbose=True)

BASE_URL = "https://kuali-build-api-sjc-01.oit.duke.edu/kbformmigration"

USERNAME = getenv("BASIC_AUTH_USERNAME")
PASSWORD = getenv("BASIC_AUTH_PASSWORD")


@pytest.fixture
def client():
    """
    Creates an HTTP client fixture.

    Args:
        self: The instance of the test class.

    Returns:
        httpx.Client: An HTTP client instance.
    """
    return httpx.Client(base_url=BASE_URL, auth=BasicAuth(USERNAME, PASSWORD))


def test_get_health_info(client):
    """
    Tests the `get_health_info` function.

    Args:
        client: The HTTP client fixture.

    Examples:
        >>> test_get_health_info(client)
    """
    response = client.get("/knock-knock")
    assert response.status_code == 200


def test_get_environment_info(client):
    """
    Tests the `get_environment_info` function.

    Args:
        client: The HTTP client fixture.

    Examples:
        >>> test_get_environment_info(client)
    """
    response = client.get("/environment")
    assert response.status_code == 200
```

</details>

<details><summary><b>client - Docstring</b></summary>

```txt
Creates an HTTP client fixture.

Args:
    self: The instance of the test class.

Returns:
    httpx.Client: An HTTP client instance.
```

</details>
<details><summary><b>client - Code</b></summary>

```python
    return httpx.Client(base_url=BASE_URL, auth=BasicAuth(USERNAME, PASSWORD))
```

</details>

<details><summary><b>test_get_health_info - Docstring</b></summary>

```txt
Tests the `get_health_info` function.

Args:
    client: The HTTP client fixture.

Examples:
    >>> test_get_health_info(client)
```

</details>
<details><summary><b>test_get_health_info - Code</b></summary>

```python
    response = client.get("/knock-knock")
    assert response.status_code == 200
```

</details>

<details><summary><b>test_get_environment_info - Docstring</b></summary>

```txt
Tests the `get_environment_info` function.

Args:
    client: The HTTP client fixture.

Examples:
    >>> test_get_environment_info(client)
```

</details>
<details><summary><b>test_get_environment_info - Code</b></summary>

```python
    response = client.get("/environment")
    assert response.status_code == 200
```

</details>
