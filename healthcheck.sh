#!/bin/bash

# Define
URLS=("https://duke.kualibuild.com/app/api/v0/graphql" "https://duke-sbx.kualibuild.com/app/api/v0/graphql" "https://duke-stg.kualibuild.com/app/api/v0/graphql")
INSTANCES=("production" "sandbox" "staging")
MULTI_AUTH_TOKENS=("${AUTH_TOKEN_PRD}" "${AUTH_TOKEN_SBX}" "${AUTH_TOKEN_STG}")
QUERY='{"query":"query ReviewDukeKualiBackendHealthStats {\n\thealth {\n\t\tstatus\n\t\tdatabase {\n\t\t\tstatus\n\t\t}\n\t\tforms {\n\t\t\tstatus\n\t\t}\n\t\tformsExternal {\n\t\t\tstatus\n\t\t}\n\t}\n}\n"}'
HSTS_PATH="/app/hsts.txt"

# Ensure we have installed curl
if ! command -v curl &>/dev/null; then
    echo "curl could not be found, we must install it to continue."
    exit 1
fi

# Ensure we have installed jq
if ! command -v jq &>/dev/null; then
    echo "jq could not be found, we need to install it to continue."
    exit 1
fi

# Get token(s)
AUTH_TOKENS=()
for token in "${MULTI_AUTH_TOKENS[@]}"; do
    AUTH_TOKENS+=("$token")
done

# Test Kuali Build
send_kuali_health_check() {
    response=$(
        curl -X 'POST' \
            --silent \
            --fail \
            --compressed \
            --retry-connrefused \
            --header 'Accept-Encoding: gzip, deflate' \
            --header 'Content-Type: application/json' \
            --header 'Accept: application/json' \
            --header 'Connection: keep-alive' \
            --header 'DNT: 1' \
            --header "Authorization: $2" \
            --hsts "$HSTS_PATH" \
            --data-binary "$3" \
            --url "$1"
    )

    if [ $? -ne 0 ]; then
        echo "Request to $1 failed"
        return 1
    else
        # Extracting each status and converting to lowercase for case-insensitive comparison
        database_status=$(echo "${response}" | jq -r '.data.health.database.status' | tr '[:upper:]' '[:lower:]')
        forms_status=$(echo "${response}" | jq -r '.data.health.forms.status' | tr '[:upper:]' '[:lower:]')
        formsExternal_status=$(echo "${response}" | jq -r '.data.health.formsExternal.status' | tr '[:upper:]' '[:lower:]')
        overall_status=$(echo "${response}" | jq -r '.data.health.status' | tr '[:upper:]' '[:lower:]')

        if [ "$database_status" != "connected" ]; then
            echo "Database status is not ok: $database_status"
            return 1
        fi

        if [ "$forms_status" != "ok" ]; then
            echo "Forms status is not ok: $forms_status"
            return 1
        fi

        if [ "$formsExternal_status" != "ok" ]; then
            echo "Forms External status is not ok: $formsExternal_status"
            return 1
        fi

        if [ "$overall_status" != "ok" ]; then
            echo "Overall status is not ok: $overall_status"
            return 1
        fi

        echo "All Kuali health checks passed: Database: $database_status, Forms: $forms_status, Forms External: $formsExternal_status, Overall: $overall_status"
    fi
}

# Send
for i in "${!URLS[@]}"; do
    echo "${INSTANCES[$i]}:"
    if ! send_kuali_health_check "${URLS[$i]}" "${AUTH_TOKENS[$i]}" "$QUERY"; then
        echo "Kuali ${INSTANCES[$i]} instance is beginning to 🤮"
        exit 1
    fi
done

compare_json_responses() {
    local expected_json=$1
    local actual_json=$2
    local context=$3

    normalize_json() {
        local json_output
        if ! json_output=$(echo "$1" | jq --sort-keys . 2>/dev/null); then
            echo "Error processing JSON in $context"
            return 1
        fi
        echo "$json_output"
    }

    # Normalize
    local normalized_expected=$(normalize_json "$expected_json") || exit 1
    local normalized_actual=$(normalize_json "$actual_json") || exit 1

    # Compare
    if [ "$normalized_actual" == "$normalized_expected" ]; then
        echo "OK - $context endpoint response matches expected"
    else
        echo "CRITICAL - $context endpoint response does not match expected"
        echo "$actual_json"
        exit 1
    fi
}

check_to_see_if_app_is_still_alive() {
    response=$(
        curl -X GET \
            --silent \
            --fail \
            --compressed \
            --retry-connrefused \
            --header 'Content-Type: application/json' \
            --header 'Accept: application/json' \
            --hsts "$HSTS_PATH" \
            --user "${BASIC_AUTH_USERNAME}:${BASIC_AUTH_PASSWORD}" \
            --url 'https://kuali-build-api-sjc-01.oit.duke.edu/kbgetpdfdocuments/knock-knock'
    )

    expected='{"app": "ok"}'
    compare_json_responses "$expected" "$response" "/knock-knock"
}

check_to_see_if_app_is_still_alive

# yayyy...
exit 0
