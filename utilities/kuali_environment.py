from os import getenv
from dotenv import load_dotenv
from base64 import b64decode
import binascii

load_dotenv(verbose=True)


def is_base64(s):
    """
    Checks if a string is a valid base64 encoded value.

    Args:
        s (str): The string to be checked.

    Returns:
        Union[bytes, None]: The decoded bytes if the string is a valid base64 encoded value, otherwise None.
    """
    try:
        return b64decode(s, validate=True)
    except binascii.Error:
        return None


def get_env_variables(env_label: str):
    """
    Retrieves environment variables based on the provided environment label.

    Args:
        env_label (str): The label of the environment.

    Returns:
        Dict[str, str]: A dictionary containing the retrieved environment variables.

    Raises:
        ValueError: If the environment label is unknown.
    """
    if env_label == "production":
        auth_token = getenv("AUTH_TOKEN_PRD")
        instance = getenv("KUALI_INSTANCE_PRD")
    elif env_label == "sandbox":
        auth_token = getenv("AUTH_TOKEN_SBX")
        instance = getenv("KUALI_INSTANCE_SBX")
    elif env_label == "staging":
        auth_token = getenv("AUTH_TOKEN_STG")
        instance = getenv("KUALI_INSTANCE_STG")
    else:
        raise ValueError(f"Unknown environment label: {env_label}")

    decoded_auth_token = is_base64(auth_token)
    if decoded_auth_token is not None:
        auth_token = decoded_auth_token.decode("utf-8")

    return {
        "auth_token": auth_token,
        "instance": instance,
    }
