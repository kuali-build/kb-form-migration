import os
import sys
import asyncio
import httpx
import logging
from fastapi import Request
from loguru import logger

from utilities.main import create_new_form
from utilities.kuali_environment import get_env_variables

import modules as mod

MAX_RETRIES = 3
SLEEP_DURATION = 5


async def process_create_form_task(
    to_environment: str,
    request: Request,
    user_duid: str,
    kuali_user_id: str,
    from_form_id: str,
    form_name: str,
    target_form_name: str,
    source_kuali: mod.Kuali,
    target_kuali: mod.Kuali,
    icon_name: str,
    icon_color: str,
):
    """
    Processes the task to create a new form in the target environment based on the provided parameters.
    It calls the create_new_form function to perform the necessary operations and sends the response back.

    Args:
        to_environment: The target environment ("production", "sandbox", "staging", or other).
        request: The request object.
        user_duid: The DUID of the user.
        kuali_user_id: The ID of the Kuali user.
        from_form_id: The ID of the originating form.
        form_name: The name of the form.
        target_form_name: The name of the target form.
        source_kuali: The source Kuali instance.
        target_kuali: The target Kuali instance.
        icon_name: The name of the form icon.
        icon_color: The color of the form icon.

    Returns:
        None.
    """
    await create_new_form(
        to_environment,
        user_duid,
        kuali_user_id,
        from_form_id,
        form_name,
        target_form_name,
        source_kuali,
        target_kuali,
        icon_color,
        target_space_id="5e47518b90adda9474c14adb",
        to_form_id=None,
        icon_name="app-window-check.svg",
    )
    await send_response_back(request)


async def send_response_back(request: Request):
    """
    Sends the response back to the specified response URL in the request headers.
    It retries the request with exponential backoff in case of HTTP errors or other exceptions.

    Args:
        request: The request object.

    Returns:
        None.
    """
    retries = 0
    while retries < MAX_RETRIES:
        try:
            if response_url := request.headers.get("X-Response-URL"):
                logger.info(f"Found response URL: {response_url}")
                headers = {"X-Status-Code": "201"}

                async with httpx.AsyncClient(http2=True, timeout=60.0) as client:
                    logger.info("Posting to response URL.")
                    response = await client.post(response_url, headers=headers)
                    response.raise_for_status()
                    logger.info(
                        f"Response status code: {response.status_code} {response.text}"
                    )
                    break
            else:
                logger.info("No X-Response-URL header found.")
                break
        except httpx.HTTPStatusError as e:
            retries += 1
            logger.error(
                f"HTTP Status Error: {e}. Attempt {retries} failed, retrying in {SLEEP_DURATION} seconds..."
            )
            await asyncio.sleep(SLEEP_DURATION)
        except Exception as e:
            retries += 1
            logger.error(
                f"An error occurred: {e}. Attempt {retries} failed, retrying in {SLEEP_DURATION} seconds..."
            )
            await asyncio.sleep(SLEEP_DURATION)

    if retries == MAX_RETRIES:
        logger.error(f"Failed to send response after {MAX_RETRIES} attempts.")
