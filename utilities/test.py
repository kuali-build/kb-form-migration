from os import getenv
import httpx
import pytest
from httpx import BasicAuth
from dotenv import load_dotenv

load_dotenv(verbose=True)

BASE_URL = "https://kuali-build-api-sjc-01.oit.duke.edu/kbformmigration"

USERNAME = getenv("BASIC_AUTH_USERNAME")
PASSWORD = getenv("BASIC_AUTH_PASSWORD")


@pytest.fixture
def client():
    """
    Creates an HTTP client fixture.

    Args:
        self: The instance of the test class.

    Returns:
        httpx.Client: An HTTP client instance.
    """
    return httpx.Client(base_url=BASE_URL, auth=BasicAuth(USERNAME, PASSWORD))


def test_get_health_info(client):
    """
    Tests the `get_health_info` function.

    Args:
        client: The HTTP client fixture.

    Examples:
        >>> test_get_health_info(client)
    """
    response = client.get("/knock-knock")
    assert response.status_code == 200


def test_get_environment_info(client):
    """
    Tests the `get_environment_info` function.

    Args:
        client: The HTTP client fixture.

    Examples:
        >>> test_get_environment_info(client)
    """
    response = client.get("/environment")
    assert response.status_code == 200
