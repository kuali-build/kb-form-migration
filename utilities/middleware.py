# Standard library imports
from time import time
from contextlib import asynccontextmanager
from datetime import timedelta
from pathlib import Path
from os import getenv

# Third-party imports
from dotenv import load_dotenv
from fastapi import Request, FastAPI, Depends
from fastapi.responses import JSONResponse, HTMLResponse, PlainTextResponse
from fastapi.middleware.gzip import GZipMiddleware
from fastapi.middleware.cors import CORSMiddleware
from fastapi.exceptions import RequestValidationError, HTTPException
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from loguru import logger
from Secweb import SecWeb


security = HTTPBasic()
load_dotenv(verbose=True, override=True)


@asynccontextmanager
async def lifespan(app: FastAPI):
    logger.success("starting up")
    yield
    logger.warning("shutting down")


UNAUTHORIZED_JSON = {
    "error": "Unauthorized, these are not the droids you are looking for."
}
UNAUTHORIZED_HTML = (
    "<html><body><h1>{} {}</h1><p>Well.. go on now, get</p></body></html>"
)
UNAUTHORIZED_ATTEMPT = "Unauthorized access attempted on endpoint '{}' by {}"
X_FORWARDED = "X-Forwarded-For: {}"
X_HOST = "Host: {}"
USERNAME = getenv("BASIC_AUTH_USERNAME")
PASSWORD = getenv("BASIC_AUTH_PASSWORD")

# code ranges
SUCCESS_STATUS_CODES = range(200, 300)
CLIENT_ERROR_STATUS_CODES = range(400, 500)
SERVER_ERROR_STATUS_CODES = range(500, 600)

logger.add(
    Path(__file__).parent / "log" / "kb-eohw-doc-truthiness.log",
    level="INFO",
    format="{time}",
    serialize=True,
    enqueue=True,
    diagnose=True,
    backtrace=True,
    catch=True,
    retention="2 weeks",
)


def access(credentials: HTTPBasicCredentials = Depends(security)):
    if credentials.username != USERNAME or credentials.password != PASSWORD:
        raise HTTPException(
            status_code=401, detail="Incorrect username or password"
        )
    return credentials


def process_headers(headers):
    excluded_headers = ["authorization", "cookie"]
    return {
        header: headers.get(header)
        for header in headers
        if all(x not in header.lower() for x in excluded_headers)
    }


def log_request_response(
    client_ip, client_host, headers, query_string, process_time, path, status_code
):
    headers_formatted = " | ".join(
        [f"{header}: {value}" for header, value in headers.items()]
    )
    log_format = f"{client_ip} | {client_host} | {headers_formatted} | {query_string} | {process_time} | {path} | {status_code}"

    if status_code in SUCCESS_STATUS_CODES:
        logger.success(f"SUCCESS | {log_format}")
    elif status_code in CLIENT_ERROR_STATUS_CODES:
        logger.error(f"CLIENT ERROR | {log_format}")
    elif status_code in SERVER_ERROR_STATUS_CODES:
        logger.critical(f"SERVER ERROR | {log_format}")


async def middlewares_stuffs(request: Request, call_next):
    start_time = time()
    response = await call_next(request)
    headers = process_headers(request.headers)
    process_time = timedelta(seconds=time() - start_time)
    response.headers["X-Process-Time"] = str(process_time)
    get_client_ip = (
        request.headers.get("x-forwarded-for", request.client.host)
        .split(",")[0]
        .strip()
    )
    log_request_response(
        X_FORWARDED.format(get_client_ip),
        X_HOST.format(request.client.host),
        headers,
        request.query_params.multi_items(),
        process_time,
        request.url.path,
        response.status_code,
    )
    return response


async def consuela_http_exception_handler(
    request: Request, exc: HTTPException
):
    if exc.status_code in range(401, 403):
        client_ip = request.client.host
        logger.warning(UNAUTHORIZED_ATTEMPT.format(request.url.path, client_ip))
        return (
            JSONResponse(
                status_code=exc.status_code,
                content=UNAUTHORIZED_JSON,
            )
            if request.headers.get("Accept", "").startswith("application/json")
            else HTMLResponse(
                status_code=exc.status_code,
                content=UNAUTHORIZED_HTML.format(exc.status_code, exc.detail),
            )
        )


async def exception_handler(request: Request, exc: Exception):
    error_message = str(exc)
    exc_type = type(exc).__name__
    logger.error(f"{exc_type} at {request.url.path}: {error_message}")
    return JSONResponse(status_code=500, content={"detail": error_message})


async def validation_exception_handler(request, exc):
    logger.error(f"RequestValidationError: {{exc}}", exc=exc, status_code=400)
    return PlainTextResponse(str(exc), status_code=400)


def mware(app: FastAPI):
    app.middleware("http")(middlewares_stuffs)
    app.add_middleware(GZipMiddleware, minimum_size=1000)
    app.add_exception_handler(Exception, exception_handler)
    app.add_exception_handler(RequestValidationError, validation_exception_handler)
    app.add_exception_handler(HTTPException, consuela_http_exception_handler)
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    SecWeb(app=app)
