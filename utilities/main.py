import os
import sys
from typing import Optional
from loguru import logger
from dotenv import load_dotenv

# sys.path.insert(0, "./modules")
import modules as mod

# from modules.graphql_calls import Kuali


async def create_new_form(
    to_environment: str,
    user_duid: str,
    kuali_user_id: str,
    from_form_id: str,
    form_name: str,
    target_form_name: str,
    source_kuali: mod.Kuali,
    target_kuali: mod.Kuali,
    icon_color: str,
    target_space_id: str,
    to_form_id: Optional[str],
    icon_name: str,
):
    """
    Creates a new form in the target environment based on the provided parameters.
    It performs a series of operations including user provisioning, permission checks,
    form creation, settings and data settings extraction and updates, workflow updates,
    integration extraction and updates, permission extraction and updates, and form publishing.

    Args:
        to_environment: The target environment ("production", "sandbox", "staging").
        user_duid: The DUID of the user.
        kuali_user_id: The ID of the Kuali user.
        from_form_id: The ID of the originating form.
        form_name: The name of the form.
        target_form_name: The name of the target form.
        source_kuali: The source Kuali instance.
        target_kuali: The target Kuali instance.
        icon_color: The color of the form icon.
        target_space_id: The ID of the target space.
        to_form_id: The ID of the target form (optional).
        icon_name: The name of the form icon.

    Returns:
        None.
    """
    # Ensure target user provisioned
    new_env_user = await mod.search_for_user(kuali_user_id, source_kuali, target_kuali)

    # Ensure target user can create apps
    await mod.add_owner_to_build_group(to_environment, new_env_user["id"], target_kuali)

    # Extracts target user info
    user_id = await mod.extract_target_user_info(user_duid, target_kuali)

    # Publishes originating form
    await mod.publish_originating_form(from_form_id, source_kuali)

    # Creates target user token
    auth_token = await mod.create_target_user_token(
        user_id["id"], form_name, user_id["displayName"], target_kuali
    )

    # Formats target user token
    user_auth_token = f"Bearer {auth_token}"

    # Creates target form
    target_form_id = await mod.create_target_form(
        target_form_name,
        target_space_id,
        user_id["id"],
        icon_name,
        icon_color,
        user_auth_token,
        user_id["displayName"],
        target_kuali,
    )

    # Updates target form favorite
    await mod.update_target_form_favorite(
        form_name, target_form_id, target_kuali, user_auth_token
    )

    # Extracts target user token
    user_auth_token_id_list_json = await mod.extract_target_user_token(
        user_id["id"], target_form_name, target_kuali
    )

    # Revokes target user token
    await mod.revoke_target_user_token(
        user_id["id"],
        form_name,
        user_id["displayName"],
        user_auth_token_id_list_json,
        target_kuali,
    )

    # Publishes target form
    await mod.publish_target_form(target_form_id, target_kuali)

    # Extracts originating form
    await mod.extract_originating_form(
        from_form_id, target_form_id, form_name, target_form_name, source_kuali
    )

    # Extracts originating workflow
    await mod.extract_originating_workflow(
        from_form_id, target_form_id, form_name, target_form_name, source_kuali
    )

    # Extracts form integrations
    await mod.extract_form_integrations(
        from_form_id,
        target_form_id,
        form_name,
        target_form_name,
        source_kuali,
        target_kuali,
    )

    # Extracts originating form settings
    form_settings = await mod.extract_originating_form_settings(
        from_form_id, source_kuali
    )

    # Extracts originating form data settings
    data_settings = await mod.extract_originating_form_data_settings(
        from_form_id, source_kuali
    )

    # Updates target form data settings
    await mod.update_target_form_data_settings(data_settings, target_kuali)

    # Updates target form settings
    await mod.update_target_form_settings(form_settings, target_kuali)

    # Reconstructs integrations if source and target Kuali instances are different
    if source_kuali != target_kuali:
        await mod.reconstruct_integrations(
            target_form_name, target_space_id, source_kuali, target_kuali
        )

    # Updates target workflow
    await mod.update_target_workflow(
        from_form_id, target_form_id, form_name, target_form_name, target_kuali
    )
    # Updates form integrations
    await mod.update_form_integrations(
        from_form_id, target_form_id, form_name, target_form_name, target_kuali
    )

    # Extracts form permissions
    await mod.extract_and_update_permissions(
        from_form_id,
        target_form_id,
        form_name,
        target_form_name,
        source_kuali,
        target_kuali,
    )

    # Updates target form
    await mod.update_target_form(
        from_form_id, target_form_id, form_name, target_form_name, target_kuali
    )

    # Updates form permissions
    await mod.update_form_permissions(
        from_form_id, target_form_id, form_name, target_form_name, target_kuali
    )

    # Publishes target form
    await mod.publish_target_form(target_form_id, target_kuali)

    # await mod.delete_tests(
    #     target_form_name, target_kuali
    # )  # Deletes the target form for testing purposes
