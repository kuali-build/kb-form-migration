import os
import sys
import json
from os.path import dirname, abspath, join
from loguru import logger
import aiofiles

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def extract_target_user_token(user_id, target_form_name, kuali_instance):
    """
    Extracts the target user's API tokens associated with the specified user ID and target form name
    using the provided Kuali instance. It saves the extracted tokens as a JSON file and returns the JSON string.

    Args:
        user_id: The ID of the target user.
        target_form_name: The name of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        A JSON string representing the extracted API tokens.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Queries.ExtractUserApiKeys()
    variables = KualiVariables.Queries.ExtractUserApiKeys(user_id, target_form_name)

    try:
        outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
        success_returned = outcome.get("data", {}).get("user", {}).get("apiKeys", [{}])

        if not success_returned:
            raise KeyError("Key 'apiKeys' not found in the response or it's None")

        json_str = json.dumps(success_returned, indent=4)
        file_path = join(
            parent_dir,
            "kuali_skeletons",
            target_form_name,
            "user_api_keys.json",
        )

        async with aiofiles.open(file_path, mode="w") as f:
            await f.write(json_str)

        logger.success("Tokens extracted")
        return json_str

    except JSONDecodeError:
        logger.error("Error decoding JSON response")
    except KeyError as ke:
        logger.error(f"KeyError: {ke}")
    except Exception as oops:
        logger.error(f"Unexpected error: {str(oops)}")
