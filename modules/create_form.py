# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def create_target_form(
    target_form_name,
    target_space_id,
    user_id,
    icon_name,
    icon_color,
    user_auth_token,
    user_name,
    kuali_instance,
):
    """
    Creates a target form using the provided target form name, target space ID, user ID, icon name,
    icon color, user authentication token, user name, and Kuali instance.

    Args:
        target_form_name: The name of the target form.
        target_space_id: The ID of the target space.
        user_id: The ID of the user.
        icon_name: The name of the icon.
        icon_color: The color of the icon.
        user_auth_token: The authentication token of the user.
        user_name: The name of the user.
        kuali_instance: The Kuali instance.

    Returns:
        The ID of the created target form.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers(user_auth_token)
    graphql = KualiGraphQL.Mutations.CreateApp()
    variables = KualiVariables.Mutations.CreateApp(
        target_form_name, target_space_id, icon_name, icon_color
    )
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = outcome.get("data", {}).get("data", {})

        if success_returned is None:
            raise KeyError
        target_form_id = success_returned.get("id", None)
        logger.success(f"Form created with id: {target_form_id}")

        return target_form_id
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
