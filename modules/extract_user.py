# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def extract_target_user_info(app_owner, kuali_instance):
    """
    Extracts the target user's information associated with the specified app owner
    using the provided Kuali instance.

    Args:
        app_owner: The app owner.
        kuali_instance: The Kuali instance.

    Returns:
        A dictionary containing the extracted target user's information,
        or None if the information is not found.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Queries.GetUser()
    variables = KualiVariables.Queries.GetUser(app_owner)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = (
            outcome.get("data", {})
            .get("usersConnection", {})
            .get("edges", [])[0]
            .get("node")
        )

        if success_returned is None:
            raise KeyError
        user_id = success_returned.get("id", None)

        return success_returned
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
