# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def publish_target_form(target_form_id, kuali_instance):
    """
    Publishes the target form using the provided target form ID and Kuali instance.

    Args:
        target_form_id: The ID of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.PublishForm()
    variables = KualiVariables.Mutations.PublishTargetForm(target_form_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = outcome

        if success_returned is not None:
            logger.success("Form published!")
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
