# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def update_target_form(
    originating_form_id,
    target_form_id,
    originating_form_name,
    target_form_name,
    kuali_instance,
):
    """
    Updates the target form using the provided originating form ID, target form ID,
    originating form name, target form name, and Kuali instance.
    It sends a GraphQL mutation request to update the form.

    Args:
        originating_form_id: The ID of the originating form.
        target_form_id: The ID of the target form.
        originating_form_name: The name of the originating form.
        target_form_name: The name of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.UpdateForm()
    variables = KualiVariables.Mutations.UpdateForm(
        originating_form_id,
        target_form_id,
        originating_form_name,
        target_form_name,
    )
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = outcome

        if success_returned is not None:
            logger.success("Form updated!")
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
