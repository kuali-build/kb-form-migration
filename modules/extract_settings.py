# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def extract_originating_form_settings(originating_form_id, kuali_instance):
    """
    Extracts the settings of an originating form from a Kuali instance.

    Args:
        originating_form_id (str): The ID of the originating form.
        kuali_instance: The Kuali instance to extract the form settings from.

    Returns:
        dict: The extracted form settings.

    Raises:
        KeyError: If the form settings cannot be found.
    """

    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Queries.ExtractFormSettings()
    variables = KualiVariables.Queries.ExtractFormSettings(originating_form_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = outcome.get("data", {}).get("app", {}).get("dataset")
        if success_returned is None:
            raise KeyError
        logger.success("Extracted form settings")
        success_returned["appId"] = success_returned.pop("id")
        success_returned.pop("changeTypes", None)
        return success_returned
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")


async def extract_originating_form_data_settings(originating_form_id, kuali_instance):
    """
    Extracts the settings of an originating form from a Kuali instance.

    Args:
        originating_form_id (str): The ID of the originating form.
        kuali_instance: The Kuali instance to extract the form settings from.

    Returns:
        dict: The extracted form settings.

    Raises:
        KeyError: If the form settings cannot be found.

    Example:
        originating_form_id = "12345"
        kuali_instance = KualiInstance()
        settings = await extract_originating_form_settings(originating_form_id, kuali_instance)
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Queries.ExtractDataSettings()
    variables = KualiVariables.Queries.ExtractDataSettings(originating_form_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        form_data_setting = outcome.get("data", {}).get("app", {}).get("dataset")

        if form_data_setting is None:
            raise KeyError
        logger.success("Extracted form data")
        dataset = form_data_setting.get("documentListConfig")
        dataset["appId"] = form_data_setting.pop("id")
        dataset.pop("changeTypes", None)
        logger.success("Document dataset extracted")
        return dataset

    except JSONDecodeError:
        logger.error("Error decoding JSON response")
    except KeyError as ke:
        logger.error(f"KeyError: {ke}")
    except Exception as oops:
        logger.error(f"Unexpected error: {str(oops)}")
