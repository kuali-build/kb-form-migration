# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def update_target_form_settings(form_settings, kuali_instance):
    """
    Updates the settings of the target form using the provided form settings and Kuali instance.
    It sends a GraphQL mutation request to update the form settings.

    Args:
        form_settings: The settings of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.UpdateFormSettings()
    variables = KualiVariables.Mutations.UpdateFormSettings(form_settings)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        if outcome is not None:
            logger.success("Form settings updated!")
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")


async def update_target_form_data_settings(data_settings, kuali_instance):
    """
    Updates the data settings of the target form using the provided data settings and Kuali instance.
    It sends a GraphQL mutation request to update the data settings.

    Args:
        data_settings: The data settings of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.UpdateDataSettings()
    variables = KualiVariables.Mutations.UpdateDataSettings(data_settings)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        if outcome is not None:
            logger.success("Data settings updated!")
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
