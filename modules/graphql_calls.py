# Standard library imports
import sys
from os import getenv
from os.path import dirname, abspath, join
from pathlib import Path
from json import JSONDecodeError
from typing import Dict, Any

# Third-party imports
import orjson
from httpx import AsyncClient
from httpx import Timeout
from httpx import Limits
from httpx import RequestError
from httpx import ReadTimeout
from httpx import HTTPStatusError
from fastapi.exceptions import HTTPException
from dotenv import load_dotenv
from loguru import logger
from asyncio import Semaphore
import uvloop

load_dotenv(verbose=True)
uvloop.install()

BASE_URL = "https://pof.trinity.duke.edu/feed"
SEMAPHORE = Semaphore(10)
timeouts = Timeout(45, connect=25, read=45, write=45)
CLIENT = AsyncClient(
    http2=True,
    timeout=timeouts,
    trust_env=True,
    default_encoding="utf-8",
    limits=Limits(max_keepalive_connections=5, max_connections=10),
)


instance = getenv("KUALI_INSTANCE_PRD")

current_dir = dirname(abspath(__file__))
parent_dir = dirname(dirname(abspath(__file__)))
sys.path.append(current_dir)

from circuit_breaker import CircuitBreaker


circuit_breaker = CircuitBreaker()


def chunkify(lst, n):
    """Let give Kuali some breathing room and yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


async def fetch_json_from_url(url: str) -> Dict[str, Any]:
    """
    Fetches JSON data from a given URL.

    Args:
        url (str): The URL to fetch JSON data from.

    Returns:
        Dict[str, Any]: The JSON data as a dictionary.

    Raises:
        HTTPException: If the circuit breaker is open and execution is not allowed.

    Examples:
        >>> data = await fetch_json_from_url(url)
    """
    if not circuit_breaker.can_execute():
        raise HTTPException(
            status_code=503,
            detail="Service temporarily unavailable due to multiple failures",
        )
    async with SEMAPHORE:
        try:
            response = await CLIENT.get(url)
            response.raise_for_status()
            return response.json()
        except (HTTPStatusError, JSONDecodeError) as dang:
            circuit_breaker.record_failure()
            logger.error(f"Error occurred while fetching from {url}: {dang}")
            return {}


class Kuali:
    """
    Represents a Kuali API client.

    Args:
        instance: The Kuali instance.
        auth_token: The authentication token.

    Methods:
        url(): Get the URL for the Kuali API.
        headers(user_auth_token=None): Get the headers for API requests.
        FetchResponse(url, headers, query, variables, retries=3, timeout=30.0): Fetch a response from the Kuali API.

    Examples:
        >>> instance = "example"
        >>> auth_token = "1234567890"
        >>> kuali_client = Kuali(instance, auth_token)
        >>> api_url = kuali_client.url()
        >>> headers = kuali_client.headers()
        >>> response = await Kuali.FetchResponse(api_url, headers, query, variables)
    """

    def __init__(self, instance, auth_token):
        """
        Represents a GraphQL client.

        Args:
            instance (str): The instance URL of the GraphQL server.
            auth_token (str): The authentication token for accessing the GraphQL server.

        Attributes:
            instance (str): The instance URL of the GraphQL server.
            auth_token (str): The authentication token for accessing the GraphQL server.

        Examples:
            >>> client = GraphQLClient(instance="https://example.com/graphql", auth_token="my_token")
        """
        self.instance = instance
        self.auth_token = auth_token

    def url(self):
        """
        Returns the URL for the GraphQL endpoint.

        Returns:
            str: The URL for the GraphQL endpoint.

        Examples:
            >>> endpoint_url = url()
        """
        return f"https://{self.instance}.kualibuild.com/app/api/v0/graphql"

    def headers(self, user_auth_token=None):
        """
        Returns the headers for making a GraphQL request.

        Args:
            user_auth_token (str, optional): The user-specific authentication token. Defaults to None.

        Returns:
            Dict[str, str]: The headers for the GraphQL request.

        Examples:
            >>> request_headers = headers(user_auth_token="my_token")
        """
        token = self.auth_token if user_auth_token is None else user_auth_token
        return {
            "Accept-Encoding": "gzip, deflate",
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Connection": "keep-alive",
            "DNT": "1",
            "Origin": f"https://{self.instance}.kualibuild.com",
            "Authorization": f"{token}",
        }

    @staticmethod
    async def FetchResponse(url, headers, query, variables, retries=3, timeout=30.0):
        """
        Fetches a response from a GraphQL endpoint.

        Args:
            url (str): The URL of the GraphQL endpoint.
            headers (Dict[str, str]): The headers to include in the request.
            query (str): The GraphQL query.
            variables (Dict[str, Any]): The variables to include in the request.
            retries (int): The number of retries in case of failure. Defaults to 3.
            timeout (float): The timeout value for the request in seconds. Defaults to 30.0.

        Returns:
            Dict[str, Any]: The JSON response as a dictionary.

        Raises:
            JSONDecodeError: If there is an error decoding the JSON response.
            ReadTimeout: If the request times out after all retries.

        Examples:
            >>> response = await FetchResponse(url, headers, query, variables)
        """
        for retry in range(retries):
            try:
                response = await CLIENT.post(
                    url,
                    headers=headers,
                    json={"query": query, "variables": variables},
                )
                try:
                    return response.json()
                except JSONDecodeError:
                    logger.error(f"Error decoding JSON:\n{response.text}")
                    raise
            except ReadTimeout:
                if retry == retries - 1:
                    raise
                else:
                    logger.warning(
                        f"Request timed out. Retrying ({retry + 1}/{retries})..."
                    )


class KualiGraphQL:
    """
    Represents a collection of GraphQL queries and mutations for the Kuali API.

    Attributes:
        gql_path: The path to the GraphQL files.

    Classes:
        Mutations: Contains static methods for retrieving mutation queries.
        Queries: Contains static methods for retrieving query queries.

    Examples:
        >>> mutation = KualiGraphQL.Mutations.AddGroupRoleUser()
        >>> query = KualiGraphQL.Queries.GetUser()
    """

    gql_path = join(parent_dir, "GraphQL")

    class Mutations:
        """
        Contains static methods for retrieving GraphQL mutation queries related to various operations.

        Methods:
            AddGroupRoleUser(): Retrieves the AddGroupRoleUser mutation query.
            CreateGroup(): Retrieves the CreateGroup mutation query.
            CreateUser(): Retrieves the CreateUser mutation query.
            CreateIntegration(): Retrieves the CreateIntegration mutation query.
            CreateToken(): Retrieves the CreateToken mutation query.
            CreateApp(): Retrieves the CreateApp mutation query.
            UpdateFormSettings(): Retrieves the UpdateFormSettings mutation query.
            UpdateDataSettings(): Retrieves the UpdateDataSettings mutation query.
            RevokeToken(): Retrieves the RevokeToken mutation query.
            PublishForm(): Retrieves the PublishForm mutation query.
            UpdateForm(): Retrieves the UpdateForm mutation query.
            UpdateWorkflow(): Retrieves the UpdateWorkflow mutation query.
            UpdateIntegration(): Retrieves the UpdateIntegration mutation query.
            UpdatePermissions(): Retrieves the UpdatePermissions mutation query.
            CreateFilters(): Retrieves the CreateFilters mutation query.
            ShareFilters(): Retrieves the ShareFilters mutation query.
            UpdateAppFavorite(): Retrieves the UpdateAppFavorite mutation query.
            CreatePolicyGroup(): Retrieves the CreatePolicyGroup mutation query.
        """

        @staticmethod
        def AddGroupRoleUser():
            """
            Retrieves the AddGroupRoleUser GraphQL query.

            Returns:
                The AddGroupRoleUser query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "AddGroupRoleUser.gql"), "r") as file:
                return file.read()

        @staticmethod
        def CreateGroup():
            """
            Retrieves the CreateGroup GraphQL mutation.

            Returns:
                The CreateGroup mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateGroup.gql"), "r") as file:
                return file.read()

        @staticmethod
        def CreateUser():
            """
            Retrieves the CreateUser GraphQL mutation.

            Returns:
                The CreateUser mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateUser.gql"), "r") as file:
                return file.read()

        @staticmethod
        def CreateIntegration():
            """
            Retrieves the CreateIntegration GraphQL mutation.

            Returns:
                The CreateIntegration mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "CreateIntegration.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def CreateToken():
            """
            Retrieves the CreateToken GraphQL mutation.

            Returns:
                The CreateToken mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateToken.gql"), "r") as file:
                return file.read()

        @staticmethod
        def CreateApp():
            """
            Retrieves the CreateApp GraphQL mutation.

            Returns:
                The CreateApp mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateApp.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateFormSettings():
            """
            Retrieves the UpdateFormSettings GraphQL mutation.

            Returns:
                The UpdateFormSettings mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdateFormSettings.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def UpdateDataSettings():
            """
            Retrieves the UpdateDataSettings GraphQL mutation.

            Returns:
                The UpdateDataSettings mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdateDataSettings.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def RevokeToken():
            """
            Retrieves the RevokeToken GraphQL mutation.

            Returns:
                The RevokeToken mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "RevokeToken.gql"), "r") as file:
                return file.read()

        @staticmethod
        def PublishForm():
            """
            Retrieves the PublishForm GraphQL mutation.

            Returns:
                The PublishForm mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "PublishForm.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateForm():
            """
            Retrieves the UpdateForm GraphQL mutation.

            Returns:
                The UpdateForm mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "UpdateForm.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateWorkflow():
            """
            Retrieves the UpdateWorkflow GraphQL mutation.

            Returns:
                The UpdateWorkflow mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "UpdateWorkflow.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateIntegration():
            """
            Retrieves the UpdateIntegration GraphQL mutation.

            Returns:
                The UpdateIntegration mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdateIntegration.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def UpdatePermissions():
            """
            Retrieves the UpdatePermissions GraphQL mutation.

            Returns:
                The UpdatePermissions mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdatePermissions.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def CreateFilters():
            """
            Retrieves the CreateFilters GraphQL mutation.

            Returns:
                The CreateFilters mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "CreateFilters.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ShareFilters():
            """
            Retrieves the ShareFilters GraphQL mutation.

            Returns:
                The ShareFilters mutation as a string.
            """
            with open(join(KualiGraphQL.gql_path, "ShareFilters.gql"), "r") as file:
                return file.read()

        @staticmethod
        def UpdateAppFavorite():
            """
            Retrieves the UpdateAppFavorite GraphQL mutation.

            Returns:
                The UpdateAppFavorite mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "UpdateAppFavorite.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def CreatePolicyGroup():
            """
            Retrieves the CreatePolicyGroup GraphQL mutation.

            Returns:
                The CreatePolicyGroup mutation as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "CreatePolicyGroup.gql"), "r"
            ) as file:
                return file.read()

    class Queries:
        """
        Contains static methods for retrieving GraphQL queries related to various operations.

        Methods:
            SearchSpaces(): Retrieves the SearchSpaces query.
            GetGroupByKualiID(): Retrieves the GetGroupByKualiID query.
            GetUserByKualiID(): Retrieves the GetUserByKualiID query.
            GetUser(): Retrieves the GetUser query.
            ExtractUserApiKeys(): Retrieves the ExtractUserApiKeys query.
            ExtractIntegrationBodies(): Retrieves the ExtractIntegrationBodies query.
            ExtractForm(): Retrieves the ExtractForm query.
            ExtractFormSettings(): Retrieves the ExtractFormSettings query.
            ExtractIntegration(): Retrieves the ExtractIntegration query.
            ExtractWorkflows(): Retrieves the ExtractWorkflows query.
            SpaceIDs(): Retrieves the SpaceIDs query.
            ExtractApps(): Retrieves the ExtractApps query.
            ExtractPermissions(): Retrieves the ExtractPermissions query.
            ExtractDataSettings(): Retrieves the ExtractDataSettings query.
        """

        @staticmethod
        def SearchSpaces():
            """
            Retrieves the SearchSpaces GraphQL query.

            Returns:
                The SearchSpaces query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "SearchSpaces.gql"), "r") as file:
                return file.read()

        @staticmethod
        def GetGroupByKualiID():
            """
            Retrieves the GetGroupByKualiID GraphQL query.

            Returns:
                The GetGroupByKualiID query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "GetGroupByKualiID.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def GetUserByKualiID():
            """
            Retrieves the GetUserByKualiID GraphQL query.

            Returns:
                The GetUserByKualiID query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "GetUserByKualiID.gql"), "r") as file:
                return file.read()

        @staticmethod
        def GetUser():
            """
            Retrieves the GetUser GraphQL query.

            Returns:
                The GetUser query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "GetUser.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ExtractUserApiKeys():
            """
            Retrieves the ExtractUserApiKeys GraphQL query.

            Returns:
                The ExtractUserApiKeys query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractUserApiKeys.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def ExtractIntegrationBodies():
            """
            Retrieves the ExtractIntegrationBodies GraphQL query.

            Returns:
                The ExtractIntegrationBodies query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractIntegrationBodies.gql"),
                "r",
            ) as file:
                return file.read()

        @staticmethod
        def ExtractForm():
            """
            Retrieves the ExtractForm GraphQL query.

            Returns:
                The ExtractForm query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "ExtractForm.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ExtractFormSettings():
            """
            Retrieves the ExtractFormSettings GraphQL query.

            Returns:
                The ExtractFormSettings query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractFormSettings.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def ExtractIntegration():
            """
            Retrieves the ExtractIntegration GraphQL query.

            Returns:
                The ExtractIntegration query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractIntegration.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def ExtractWorkflows():
            """
            Retrieves the ExtractWorkflows GraphQL query.

            Returns:
                The ExtractWorkflows query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "ExtractWorkflows.gql"), "r") as file:
                return file.read()

        @staticmethod
        def SpaceIDs():
            """
            Retrieves the SpaceIDs GraphQL query.

            Returns:
                The SpaceIDs query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "SpaceIDs.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ExtractApps():
            """
            Retrieves the ExtractApps GraphQL query.

            Returns:
                The ExtractApps query as a string.
            """
            with open(join(KualiGraphQL.gql_path, "ExtractApps.gql"), "r") as file:
                return file.read()

        @staticmethod
        def ExtractPermissions():
            """
            Retrieves the ExtractPermissions GraphQL query.

            Returns:
                The ExtractPermissions query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractPermissions.gql"), "r"
            ) as file:
                return file.read()

        @staticmethod
        def ExtractDataSettings():
            """
            Retrieves the ExtractDataSettings GraphQL query.

            Returns:
                The ExtractDataSettings query as a string.
            """
            with open(
                join(KualiGraphQL.gql_path, "ExtractDataSettings.gql"), "r"
            ) as file:
                return file.read()
