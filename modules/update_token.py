# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def revoke_target_user_token(
    user_id,
    target_form_name,
    user_name,
    user_auth_token_id_list_json,
    kuali_instance,
):
    """
    Revokes the target user's temporary authentication token associated with the specified user ID,
    target form name, user name, user authentication token ID list JSON, and Kuali instance.
    It sends a GraphQL mutation request to revoke the token.

    Args:
        user_id: The ID of the target user.
        target_form_name: The name of the target form.
        user_name: The name of the user.
        user_auth_token_id_list_json: The JSON string representing the user authentication token ID list.
        kuali_instance: The Kuali instance.

    Returns:
        The outcome of the token revocation request.
    """
    user_auth_token_id_list = json.loads(user_auth_token_id_list_json)
    if user_auth_token_id_list is not None:
        for user_auth_token in user_auth_token_id_list:
            if user_auth_token["name"] == target_form_name:
                url = kuali_instance.url()
                headers = kuali_instance.headers()
                graphql = KualiGraphQL.Mutations.RevokeToken()
                variables = KualiVariables.Mutations.RevokeToken(
                    user_auth_token["id"], user_id
                )
                outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
                try:
                    success_returned = outcome

                    if success_returned is not None:
                        logger.success(
                            f"{user_name} temp auth token removed! - proof: {success_returned}"
                        )
                        return outcome
                    else:
                        raise KeyError
                except Exception as oops:
                    logger.info(f"Error: {str(oops)}")
