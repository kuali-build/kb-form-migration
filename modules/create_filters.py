# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def create_target_form_filters(target_form_id, target_form_name, kuali_instance):
    """
    Creates custom filters for the target form using the provided target form ID, target form name,
    and Kuali instance.

    Args:
        target_form_id: The ID of the target form.
        target_form_name: The name of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    with open(join(parent_dir, "custom_filters.json"), "r") as f:
        integrations = json.load(f)

    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.CreateFilters()

    for form_filter in integrations:
        form_filter["appId"] = target_form_id

        variables = KualiVariables.Mutations.CreateFilters(form_filter)
        outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
        try:
            success_returned = outcome

            if success_returned is not None:
                user_display_name = form_filter["name"]
                form_filter.pop("name", None)
                graphql = KualiGraphQL.Mutations.ShareFilters()
                variables = KualiVariables.Mutations.ShareFilters(form_filter)
                shared_filters_outcome = await Kuali.FetchResponse(
                    url, headers, graphql, variables
                )
                try:
                    success_returned = (
                        shared_filters_outcome.get("data", {})
                        .get("shareFilter", {})
                        .get("id")
                    )
                    constr_url = f"{user_display_name}: https://duke.kualibuild.com/app/builder/document-list/{target_form_id}?id={success_returned}"

                    if success_returned is not None:
                        logger.success(
                            f"Custom documents page filter and link created for {user_display_name}"
                        )
                        with open(
                            join(
                                parent_dir,
                                "kuali_skeletons",
                                target_form_name,
                                "shared_filters.txt",
                            ),
                            "a",
                        ) as f:
                            f.write(constr_url + "\n")
                    else:
                        raise KeyError
                except Exception as oops:
                    logger.error(f"Error: {str(oops)}")
            else:
                raise KeyError
        except Exception as oops:
            logger.error(f"Error: {str(oops)}")
