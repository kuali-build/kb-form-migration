import os
import sys
import json
from os.path import dirname, abspath, join
from loguru import logger
import aiofiles

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def delete_tests(target_form_name, kuali_instance):
    """
    Deletes test applications and integrations associated with the target form using the provided
    target form name and Kuali instance.

    Args:
        target_form_name: The name of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()

    gql_file = join(
        parent_dir,
        "kuali_skeletons",
        target_form_name,
        "remove_created_apps_and_integrations.gql",
    )
    json_file = join(
        parent_dir,
        "kuali_skeletons",
        target_form_name,
        "remove_created_apps_and_integrations.json",
    )

    async with aiofiles.open(gql_file, mode="r") as gql:
        graphql = await gql.read()

    async with aiofiles.open(json_file, mode="r") as variable:
        variables = await variable.read()

    variables = json.loads(variables)

    try:
        outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
        success_returned = outcome.get("data", {})
        if app_status := success_returned.get("deleteAppAlias"):
            logger.success(
                "#################App + Integrations deleted successfully#################"
            )

        for key in success_returned:
            if key.startswith("removeIntegration") and success_returned.get(key):
                logger.success(f"{key} deleted successfully")

        if not success_returned:
            raise KeyError("Items not deleted from target environment")

    except json.JSONDecodeError:
        logger.error("Error decoding JSON response")
    except KeyError as ke:
        logger.error(f"KeyError: {ke}")
    except Exception as oops:
        logger.error(f"Unexpected error: {str(oops)}")
