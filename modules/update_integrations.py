# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def update_form_integrations(
    originating_form_id,
    target_form_id,
    originating_form_name,
    target_form_name,
    kuali_instance,
):
    """
    Updates the integrations of the target form using the provided originating form ID,
    target form ID, originating form name, target form name, and Kuali instance.
    It reads the integrations from a JSON file, iterates over each integration,
    and sends GraphQL mutation requests to update the integrations.

    Args:
        originating_form_id: The ID of the originating form.
        target_form_id: The ID of the target form.
        originating_form_name: The name of the originating form.
        target_form_name: The name of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    with open(
        join(
            parent_dir,
            "kuali_skeletons",
            target_form_name,
            "integrations.json",
        ),
        "r",
    ) as f:
        integrations = json.load(f)

    index = 1
    for integration in integrations:
        integration_id = integration.get("integrationId")
        integration_name = integration.get("integrationName")
        if integration_id and integration_name:
            url = kuali_instance.url()
            headers = kuali_instance.headers()
            graphql = KualiGraphQL.Mutations.UpdateIntegration()
            try:
                with open(
                    join(
                        parent_dir,
                        "kuali_skeletons",
                        target_form_name,
                        "integrations",
                        f"integration_{index}.json",
                    ),
                    "r",
                ) as f:
                    variables = json.load(f)

                # If "sharingType" is "ALL", skip it and log the situation
                if variables.get("sharingType", "") == "ALL":
                    logger.info(
                        f"{integration_name} is open to all apps, no need to give permissions to {target_form_name}"
                    )
                    index += 1
                    continue

                # Remove duplicates from "apps" just in case..
                apps = variables.get("apps", [])
                seen = set()
                variables["apps"] = [
                    app
                    for app in apps
                    if not (app["id"] in seen or seen.add(app["id"]))
                ]

                outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
                success_returned = outcome
                if success_returned is not None:
                    logger.success(
                        f"{integration_name} = successfully added to {target_form_name}"
                    )
                    index += 1
                else:
                    raise KeyError
            except FileNotFoundError:
                logger.info(f"Skipping over faux integration {integration_id}")
                continue
            except Exception as oops:
                logger.error(f"Error: {str(oops)}")
