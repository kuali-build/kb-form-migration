# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


def find_ids_and_externals(obj, result=None):
    """
    Find integration IDs and external IDs within a nested dictionary or list.

    Args:
        obj: The nested dictionary or list to search for IDs and externals.
        result: A list to store the found integration IDs and external IDs.

    Returns:
        A list of dictionaries containing the integration IDs and external IDs found in the input object.

    Examples:
        >>> data = {
        ...     "id": "123456789012345678901234",
        ...     "externals": ["external:1", "external:2"],
        ...     "nested": {
        ...         "id": "987654321098765432109876",
        ...         "externals": ["external:3", "external:4"]
        ...     }
        ... }
        >>> find_ids_and_externals(data)
        [{'integrationId': '123456789012345678901234'}, {'integrationId': '1'}, {'integrationId': '2'}, {'integrationId': '987654321098765432109876'}, {'integrationId': '3'}, {'integrationId': '4'}]
    """
    if result is None:
        result = []
    if isinstance(obj, dict):
        for k, v in obj.items():
            if k == "id" and isinstance(v, str) and len(v) == 24:
                if v not in [item["integrationId"] for item in result]:
                    result.append({"integrationId": v})
            elif k == "externals" and isinstance(v, list):
                for i in v:
                    if isinstance(i, str):
                        external_id = i.split(":")[1]
                        if external_id not in [
                            item["integrationId"] for item in result
                        ]:
                            result.append({"integrationId": external_id})
            elif isinstance(v, (dict, list)):
                find_ids_and_externals(v, result)
    elif isinstance(obj, list):
        for item in obj:
            find_ids_and_externals(item, result)
    return result


def append_to_json_file(filename, data):
    """
    Append data to a JSON file.

    Args:
        filename: The path to the JSON file.
        data: The data to append to the file.

    Returns:
        None

    Raises:
        FileNotFoundError: If the specified file does not exist.
        JSONDecodeError: If the file contains invalid JSON.

    Examples:
        >>> data = [{"name": "John", "age": 30}, {"name": "Jane", "age": 25}]
        >>> append_to_json_file("data.json", data)
    """
    try:
        with open(filename, "r") as f:
            existing_data = json.load(f)
    except (FileNotFoundError, JSONDecodeError):
        existing_data = []

    for item in data:
        if item not in existing_data:
            existing_data.append(item)

    with open(filename, "w") as f:
        json.dump(existing_data, f, indent=4)


async def extract_originating_workflow(
    originating_form_id,
    target_form_id,
    originating_form_name,
    target_form_name,
    kuali_instance,
):
    """
    Extracts the originating workflow from a Kuali instance and saves it as JSON files.

    Args:
        originating_form_id: The ID of the originating form.
        target_form_id: The ID of the target form.
        originating_form_name: The name of the originating form.
        target_form_name: The name of the target form.
        kuali_instance: The instance of the Kuali API.

    Returns:
        None

    Raises:
        KeyError: If the workflow extraction fails.

    Examples:
        >>> originating_form_id = "1234567890"
        >>> target_form_id = "0987654321"
        >>> originating_form_name = "OriginatingForm"
        >>> target_form_name = "TargetForm"
        >>> kuali_instance = KualiInstance()
        >>> await extract_originating_workflow(
        ...     originating_form_id,
        ...     target_form_id,
        ...     originating_form_name,
        ...     target_form_name,
        ...     kuali_instance,
        ... )
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Queries.ExtractWorkflows()
    variables = KualiVariables.Queries.ExtractWorkflows(originating_form_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = (
            outcome.get("data", {})
            .get("app", {})
            .get("dataset", {})
            .get("workflow", {})
            .get("steps", None)
        )

        if success_returned is not None:
            success_returned = {
                "appId": target_form_id,
                "args": {"appId": target_form_id, "steps": success_returned},
            }
            json_str = json.dumps(success_returned, indent=4)
            with open(
                join(
                    parent_dir,
                    "kuali_skeletons",
                    target_form_name,
                    "workflow.json",
                ),
                "w",
            ) as f:
                f.write(json_str)
            logger.success("data.json file created")

            id_and_external_dict = find_ids_and_externals(success_returned)

            filename = join(
                parent_dir,
                "kuali_skeletons",
                target_form_name,
                "integrations.json",
            )

            append_to_json_file(filename, id_and_external_dict)

            logger.success(f"{filename} file updated")
        else:
            raise KeyError
    except Exception as oops:
        logger.error(f"Error when trying to extract workflow: {str(oops)}")
