# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


def flatten_data(data):
    """
    Flattens the provided data by extracting items from the 'searchSpaceContents' field
    and returning a flattened list of items.

    Args:
        data: The data to flatten.

    Returns:
        A flattened list of items extracted from the 'searchSpaceContents' field.
    """
    flattened_list = []
    try:
        items = data.get("data", {}).get("searchSpaceContents", [])
        flattened_list.extend(flatten_item(item) for item in items)
    except Exception as e:
        logger.error(f"Error flattening data: {e}")
    return flattened_list


def flatten_item(item):
    """
    Flattens the provided item by extracting specific fields and returning a dictionary
    containing the flattened fields.

    Args:
        item: The item to flatten.

    Returns:
        A dictionary containing the flattened fields of the item.
    """
    return {
        "createdBy_displayName": item.get("createdBy", {}).get("displayName", ""),
        "createdBy_id": item.get("createdBy", {}).get("id", ""),
        "createdBy_name": item.get("createdBy", {}).get("name", ""),
        "createdBy_schoolId": item.get("createdBy", {}).get("schoolId", ""),
        "createdBy_username": item.get("createdBy", {}).get("username", ""),
        "id": item.get("id", ""),
        "name": item.get("name", ""),
        "space_id": item.get("space", {}).get("id", ""),
        "space_name": item.get("space", {}).get("name", ""),
        "tileOptions_backgroundColor": item.get("tileOptions", {}).get(
            "backgroundColor", ""
        ),
        "tileOptions_iconName": item.get("tileOptions", {}).get("iconName", ""),
        "type": item.get("type", ""),
    }


async def search_for_app(search, kuali_instance):
    """
    Searches for an app using the provided search query and Kuali instance.
    It fetches the response from the search and returns the flattened data.

    Args:
        search: The search query.
        kuali_instance: The Kuali instance.

    Returns:
        A list of dictionaries containing the flattened data of the search results.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Queries.SearchSpaces()
    variables = KualiVariables.Queries.SearchSpaces(search)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        return flatten_data(outcome)
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
    try:
        outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
        return flatten_data(outcome)
    except Exception as oops:
        logger.error(f"Error: {str(oops)}")
