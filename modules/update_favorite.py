# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def update_target_form_favorite(
    target_form_name, target_form_id, kuali_instance, user_auth_token=None
):
    """
    Updates the favorite status of the target form using the provided target form name, ID, Kuali instance,
    and optional user authentication token. It sends a GraphQL mutation request to update the favorite status.

    Args:
        target_form_name: The name of the target form.
        target_form_id: The ID of the target form.
        kuali_instance: The Kuali instance.
        user_auth_token: Optional user authentication token.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers(user_auth_token)
    graphql = KualiGraphQL.Mutations.UpdateAppFavorite()
    variables = KualiVariables.Mutations.UpdateAppFavorite(target_form_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = (
            outcome.get("data", {}).get("updateAppFavorite", {}).get("isFavorite")
        )

        if success_returned is not None:
            logger.success(f"{target_form_name} form made favorite")
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
