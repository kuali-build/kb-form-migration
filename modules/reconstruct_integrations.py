# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger
import aiofiles

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


def recursive_replace(obj, old_id, new_id):
    """
    Recursively replaces occurrences of the old ID with the new ID in the given object.

    Args:
        obj: The object to perform the replacement on.
        old_id: The old ID to be replaced.
        new_id: The new ID to replace with.

    Returns:
        The modified object with the replacements.
    """
    if isinstance(obj, dict):
        for k, v in obj.items():
            if v == old_id:
                obj[k] = new_id
            else:
                recursive_replace(v, old_id, new_id)
    elif isinstance(obj, list):
        for idx, item in enumerate(obj):
            if item == old_id:
                obj[idx] = new_id
            else:
                recursive_replace(item, old_id, new_id)
    return obj


async def reconstruct_integrations(
    target_form_name, target_space_id, kuali_from, kuali_to
):
    """
    Reconstructs integrations for a target form using the provided target form name, target space ID,
    and Kuali instances for the source and destination.

    Args:
        target_form_name: The name of the target form.
        target_space_id: The ID of the target space.
        kuali_from: The Kuali instance for the source.
        kuali_to: The Kuali instance for the destination.

    Returns:
        None.
    """
    file_path_begin = join(parent_dir, "kuali_skeletons", target_form_name)
    with open(join(file_path_begin, "integrations.json"), "r") as f:
        integrations = json.load(f)
    index = 1
    for idx, integration in enumerate(integrations):
        integration_id = integration.get("integrationId")
        url = kuali_from.url()
        headers = kuali_from.headers()
        graphql = KualiGraphQL.Queries.ExtractIntegrationBodies()
        variables = KualiVariables.Queries.ExtractIntegrationBodies(integration_id)
        outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
        try:
            integration_data = (
                outcome.get("data", {}).get("integration", {}).get("data")
            )

            if "__legacyType" in integration_data:
                logger.info(
                    f"Integration {integration_id} is a legacy integration and will not be added to the form."
                )
                continue

            auth_type = integration_data.get("__authenticationType", {}).get("id")
            if auth_type == "basic" and "__password" in integration_data:
                integration_data["__password"] = "PLACEHOLDER_PASSWORD_VALUE"
            if "__spaceId" in integration_data:
                del integration_data["__spaceId"]

            variables = {"spaceId": target_space_id, "data": integration_data}

            with open(
                join(
                    file_path_begin,
                    "integrations",
                    f"integration_body_{index}.json",
                ),
                "w",
            ) as f:
                f.write(json.dumps(variables, indent=4))
            index += 1

            url = kuali_to.url()
            headers = kuali_to.headers()
            graphql = KualiGraphQL.Mutations.CreateIntegration()
            integration_outcome = await Kuali.FetchResponse(
                url, headers, graphql, variables
            )

            new_integration_id = (
                integration_outcome.get("data", {})
                .get("createIntegration", {})
                .get("id")
            )
            logger.info(f"new integration: {new_integration_id}")

            if new_integration_id:
                integrations[idx]["integrationId"] = new_integration_id

                with open(join(file_path_begin, "form.json"), "r") as f:
                    form_data = json.load(f)
                form_data = recursive_replace(
                    form_data, integration_id, new_integration_id
                )
                with open(join(file_path_begin, "form.json"), "w") as f:
                    json.dump(form_data, f, indent=4)

                with open(join(file_path_begin, "workflow.json"), "r") as f:
                    workflow_data = json.load(f)
                workflow_data = recursive_replace(
                    workflow_data, integration_id, new_integration_id
                )
                with open(join(file_path_begin, "workflow.json"), "w") as f:
                    json.dump(workflow_data, f, indent=4)

                integration_file_path = join(
                    file_path_begin,
                    "integrations",
                    f"integration_{index}.json",
                )
                try:
                    with open(integration_file_path, "r") as f:
                        integration_data = json.load(f)
                    integration_data["id"] = new_integration_id

                    with open(integration_file_path, "w") as f:
                        json.dump(integration_data, f, indent=4)

                except FileNotFoundError:
                    logger.debug(
                        f"Integration ID: {integration_id} is not an integration."
                    )
                except Exception as oops:
                    logger.error(
                        f"Error updating id in {integration_file_path}: {str(oops)}"
                    )

        except FileNotFoundError:
            logger.info(f"Skipping over faux integration {integration_id}")
        except Exception as oops:
            logger.error(f"Error: {str(oops)}")

    with open(join(file_path_begin, "integrations.json"), "w") as f:
        json.dump(integrations, f, indent=4)
