# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def create_target_user_token(
    user_id, target_form_name, user_name, kuali_instance
):
    """
    Creates a temporary authentication token for the target user using the provided user ID,
    target form name, user name, and Kuali instance.

    Args:
        user_id: The ID of the target user.
        target_form_name: The name of the target form.
        user_name: The name of the target user.
        kuali_instance: The Kuali instance.

    Returns:
        The temporary authentication token for the target user, or None if the creation failed.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.CreateToken()
    variables = KualiVariables.Mutations.CreateToken(user_id, target_form_name)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = outcome.get("data", {}).get("createApiKey", None)
        if success_returned is not None:
            user_auth_token = success_returned
            logger.success(f"{user_name} temp auth token created!")
            return user_auth_token
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
