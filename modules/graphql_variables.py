import os
from os.path import dirname, abspath, join
import sys
import glob
import json

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)


class KualiVariables:
    """
    Contains static methods for generating GraphQL mutation and query variables for various operations.

    Classes:
        Mutations: Contains methods for generating mutation variables.
        Queries: Contains methods for generating query variables.
    """

    class Mutations:
        """
        Contains static methods for generating GraphQL mutation variables for various operations.

        Methods:
            AddGroupRoleUser(group_id, role_id, user_id): Generates the mutation variables for adding a group role user.
            CreateGroup(group_name): Generates the mutation variables for creating a group.
            CreateUser(create_user): Generates the mutation variables for creating a user.
            PublishForm(originating_form_id): Generates the mutation variables for publishing a form.
            CreateToken(user_id, target_form_name): Generates the mutation variables for creating a token.
            CreateFilters(form_filters): Generates the mutation variables for creating filters.
            ShareFilters(shared_filters): Generates the mutation variables for sharing filters.
            CreateApp(target_form_name, target_space_id, icon_name, icon_color): Generates the mutation variables for creating an app.
            RevokeToken(token_id, user_id): Generates the mutation variables for revoking a token.
            UpdateForm(originating_form_id, target_form_id, originating_form_name, target_form_name): Generates the mutation variables for updating a form.
            UpdateFormSettings(form_settings): Generates the mutation variables for updating form settings.
            UpdateDataSettings(data_settings): Generates the mutation variables for updating data settings.
            UpdateWorkflow(originating_form_id, target_form_id, originating_form_name, target_form_name): Generates the mutation variables for updating a workflow.
            UpdatePermissions(originating_form_id, target_form_id, originating_form_name, target_form_name): Generates the mutation variables for updating permissions.
            PublishTargetForm(target_form_id): Generates the mutation variables for publishing a target form.
            UpdateAppFavorite(target_form_id): Generates the mutation variables for updating app favorite status.
            CreatePolicyGroup(target_form_id, role_name): Generates the mutation variables for creating a policy group.
        """

        @staticmethod
        def AddGroupRoleUser(groud_id, role_id, user_id):
            """
            Generates the mutation variables for adding a group role user.

            Args:
                group_id: The ID of the group.
                role_id: The ID of the role.
                user_id: The ID of the user.

            Returns:
                A dictionary containing the mutation variables.
            """
            return {"groupId": groud_id, "roleId": role_id, "userId": user_id}

        @staticmethod
        def CreateGroup(group_name):
            """
            Generates the mutation variables for creating a group.

            Args:
                group_name: The name of the group.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"name": group_name})

        @staticmethod
        def CreateUser(create_user):
            """
            Generates the mutation variables for creating a user.

            Args:
                create_user: A dictionary containing the user data.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(create_user)

        @staticmethod
        def PublishForm(originating_form_id):
            """
            Generates the mutation variables for publishing a form.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"id": originating_form_id, "showDraft": True})

        @staticmethod
        def CreateToken(user_id, target_form_name):
            """
            Generates the mutation variables for creating a token.

            Args:
                user_id: The ID of the user.
                target_form_name: The name of the target form.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"userId": user_id, "name": target_form_name})

        @staticmethod
        def CreateFilters(form_filters):
            """
            Generates the mutation variables for creating filters.

            Args:
                form_filters: A dictionary containing the form filters.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(form_filters)

        @staticmethod
        def ShareFilters(shared_filters):
            """
            Generates the mutation variables for sharing filters.

            Args:
                shared_filters: A dictionary containing the shared filters.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(shared_filters)

        @staticmethod
        def CreateApp(target_form_name, target_space_id, icon_name, icon_color):
            """
            Generates the mutation variables for creating an app.

            Args:
                target_form_name: The name of the target form.
                target_space_id: The ID of the target space.
                icon_name: The name of the app's icon.
                icon_color: The background color of the app's icon.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(
                {
                    "args": {
                        "name": target_form_name,
                        "backgroundColor": icon_color,
                        "iconName": icon_name,
                        "spaceId": target_space_id,
                    }
                }
            )

        @staticmethod
        def RevokeToken(token_id, user_id):
            """
            Generates the mutation variables for revoking a token.

            Args:
                token_id: The ID of the token.
                user_id: The ID of the user.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"userId": user_id, "tokenId": token_id})

        @staticmethod
        def UpdateForm(
            originating_form_id,
            target_form_id,
            originating_form_name,
            target_form_name,
        ):
            """
            Generates the mutation variables for updating a form.

            Args:
                originating_form_id: The ID of the originating form.
                target_form_id: The ID of the target form.
                originating_form_name: The name of the originating form.
                target_form_name: The name of the target form.

            Returns:
                A string containing the JSON data read from the form file.
            """
            with open(
                join(
                    parent_dir,
                    "kuali_skeletons",
                    target_form_name,
                    "form.json",
                ),
                "r",
            ) as f:
                json_str = f.read()
            return json_str

        @staticmethod
        def UpdateFormSettings(form_settings):
            """
            Generates the mutation variables for updating form settings.

            Args:
                form_settings: A dictionary containing the form settings.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(form_settings)

        @staticmethod
        def UpdateDataSettings(data_settings):
            """
            Generates the mutation variables for updating data settings.

            Args:
                data_settings: A dictionary containing the data settings.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps(data_settings)

        @staticmethod
        def UpdateWorkflow(
            originating_form_id,
            target_form_id,
            originating_form_name,
            target_form_name,
        ):
            """
            Generates the mutation variables for updating a workflow.

            Args:
                originating_form_id: The ID of the originating form.
                target_form_id: The ID of the target form.
                originating_form_name: The name of the originating form.
                target_form_name: The name of the target form.

            Returns:
                A string containing the JSON data read from the workflow file.
            """
            with open(
                join(
                    parent_dir,
                    "kuali_skeletons",
                    target_form_name,
                    "workflow.json",
                ),
                "r",
            ) as f:
                json_str = f.read()
            return json_str

        @staticmethod
        def UpdatePermissions(
            originating_form_id,
            target_form_id,
            originating_form_name,
            target_form_name,
        ):
            """
            Generates the mutation variables for updating permissions.

            Args:
                originating_form_id: The ID of the originating form.
                target_form_id: The ID of the target form.
                originating_form_name: The name of the originating form.
                target_form_name: The name of the target form.

            Returns:
                A list of strings containing the JSON data read from the permission files.
            """
            directory = join(
                parent_dir, "kuali_skeletons", target_form_name, "permissions"
            )
            files = glob.glob(join(directory, "*.json"))
            json_strings = []
            for file in files:
                with open(file, "r") as f:
                    json_str = f.read()
                    json_strings.append(json_str)
            return json_strings

        @staticmethod
        def PublishTargetForm(target_form_id):
            """
            Generates the mutation variables for publishing a target form.

            Args:
                target_form_id: The ID of the target form.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"id": target_form_id, "showDraft": False})

        @staticmethod
        def UpdateAppFavorite(target_form_id):
            """
            Generates the mutation variables for updating the favorite status of an app.

            Args:
                target_form_id: The ID of the target form (application).

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"applicationId": target_form_id, "isFavorite": True})

        @staticmethod
        def CreatePolicyGroup(target_form_id, role_name):
            """
            Generates the mutation variables for creating a policy group.

            Args:
                target_form_id: The ID of the target form (application).
                role_name: The name of the role.

            Returns:
                A JSON string containing the mutation variables.
            """
            return json.dumps({"applicationId": target_form_id, "name": role_name})

    class Queries:
        """
        Contains static methods for generating GraphQL query variables for various operations.

        Methods:
            SearchSpaces: Generates the query variables for searching spaces.
            GetGroupByKualiID: Generates the query variables for retrieving a group by Kuali ID.
            PermissionsPage: Generates the query variables for retrieving permissions page.
            GetUserByKualiID: Generates the query variables for retrieving a user by Kuali ID.
            GetUser: Generates the query variables for retrieving a user.
            ExtractApps: Generates the query variables for extracting apps.
            ExtractIntegrationBodies: Generates the query variables for extracting integration bodies.
            ExtractUserApiKeys: Generates the query variables for extracting user API keys.
            ExtractForm: Generates the query variables for extracting a form.
            ExtractFormSettings: Generates the query variables for extracting form settings.
            ExtractWorkflows: Generates the query variables for extracting workflows.
            ExtractIntegration: Generates the query variables for extracting an integration.
            ExtractPermissions: Generates the query variables for extracting permissions.
            ExtractDataSettings: Generates the query variables for extracting data settings.
        """

        @staticmethod
        def SearchSpaces(search):
            """
            Generates the query variables for searching spaces.

            Args:
                search: The search query.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"query": search})

        @staticmethod
        def GetGroupByKualiID(group_id):
            """
            Generates the query variables for retrieving a group by Kuali ID.

            Args:
                group_id: The Kuali ID of the group.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": group_id})

        @staticmethod
        def PermissionsPage(appId):
            """
            Generates the query variables for retrieving permissions page.

            Args:
                appId: The ID of the application.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"appId": appId})

        @staticmethod
        def GetUserByKualiID(user_kuali_id):
            """
            Generates the query variables for retrieving a user by Kuali ID.

            Args:
                user_kuali_id: The Kuali ID of the user.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": user_kuali_id})

        @staticmethod
        def GetUser(app_owner):
            """
            Generates the query variables for retrieving a user.

            Args:
                app_owner: The owner of the application.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps(
                {
                    "active": "ACTIVE",
                    "limit": 1,
                    "skip": 0,
                    "sort": [],
                    "q": app_owner,
                }
            )

        @staticmethod
        def ExtractApps():
            """
            Generates the query variables for extracting apps.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"query": ""})

        @staticmethod
        def ExtractIntegrationBodies(integrationId):
            """
            Generates the query variables for extracting integration bodies.

            Args:
                integrationId: The ID of the integration.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"integrationId": integrationId})

        @staticmethod
        def ExtractUserApiKeys(user_id, target_form_name):
            """
            Generates the query variables for extracting user API keys.

            Args:
                user_id: The ID of the user.
                target_form_name: The name of the target form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": user_id})

        @staticmethod
        def ExtractForm(originating_form_id):
            """
            Generates the query variables for extracting a form.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": originating_form_id})

        @staticmethod
        def ExtractFormSettings(originating_form_id):
            """
            Generates the query variables for extracting form settings.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"appId": originating_form_id})

        @staticmethod
        def ExtractWorkflows(originating_form_id):
            """
            Generates the query variables for extracting workflows.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"id": originating_form_id})

        @staticmethod
        def ExtractIntegration(integration_id):
            """
            Generates the query variables for extracting an integration.

            Args:
                integration_id: The ID of the integration.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"integrationId": integration_id})

        @staticmethod
        def ExtractPermissions(form_id):
            """
            Generates the query variables for extracting permissions.

            Args:
                form_id: The ID of the form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"appId": form_id})

        @staticmethod
        def ExtractDataSettings(originating_form_id):
            """
            Generates the query variables for extracting data settings.

            Args:
                originating_form_id: The ID of the originating form.

            Returns:
                A JSON string containing the query variables.
            """
            return json.dumps({"appId": originating_form_id, "query": ""})
