# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def create_permissions(target_form_id, role_name, kuali_instance):
    """
    Creates permissions for the target form using the provided target form ID, role name,
    and Kuali instance.

    Args:
        target_form_id: The ID of the target form.
        role_name: The name of the role.
        kuali_instance: The Kuali instance.

    Returns:
        The ID of the created policy group, or None if the creation failed.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.CreatePolicyGroup()
    variables = KualiVariables.Mutations.CreatePolicyGroup(target_form_id, role_name)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = outcome.get("data", {}).get("data", {})

        if success_returned is not None:
            target_form_id = success_returned.get("id", None)
            logger.success(f"Policy Group created for form: {target_form_id}")

            return target_form_id
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
