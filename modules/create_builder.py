# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
# sys.path.append(current_dir)

from modules.graphql_calls import Kuali, KualiGraphQL
from modules.graphql_variables import KualiVariables


async def add_owner_to_build_group(env_str, kuali_user_id, kuali_instance):
    """
    Adds the owner identified by the provided Kuali user ID to the build group based on the specified environment.
    The environment is determined by the env_str parameter.
    It sends a GraphQL mutation request to add the user to the corresponding group.

    Args:
        env_str: The environment string ("production", "sandbox", or other).
        kuali_user_id: The ID of the Kuali user.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    logger.info(kuali_instance)
    if env_str == "production":
        group_id = "62014a361db08c0a125717ce"
    if env_str == "sandbox":
        group_id = "621ce0b691de7c4ef79dc4d2"
    else:
        group_id = "65108c977c6aed989e18cf7b"

    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.AddGroupRoleUser()
    logger.info(graphql)
    variables = KualiVariables.Mutations.AddGroupRoleUser(
        group_id, "members", kuali_user_id
    )
    logger.info(variables)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    logger.info(outcome)
    try:
        results = (
            outcome.get("data", {})
            .get("addGroupRoleUser", {})
            .get("result", {})
            .get("members", [{}])
        )
        logger.info(results)
        for result in results:
            user_id = result.get("id")
            user_name = result.get("label")
            if user_id == kuali_user_id:
                logger.info(f"{user_name} has been added to build app creator group")
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
