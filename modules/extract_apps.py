# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def get_a_list_of_all_apps(kuali_instance):
    """
    Retrieves a list of all apps from the Kuali instance.

    Args:
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Queries.ExtractApps()
    variables = KualiVariables.Queries.ExtractApps()
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    returned_data = sorted(
        [
            {"id": app["id"], "name": " ".join(app["name"].strip().split())}
            for app in outcome["data"]["apps"]
        ],
        key=lambda app: app["name"],
    )

    for item in returned_data:
        item["name"] = " ".join(item["name"].split())

    file_path = join(parent_dir, "apps.json")

    with open(file_path, "w") as f:
        json.dump(returned_data, f, indent=2)


if __name__ == "__main__":
    import asyncio
    import uvloop

    uvloop.install()
    asyncio.run(get_a_list_of_all_apps())
