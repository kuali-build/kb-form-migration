# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def update_form_permissions(
    originating_form_id,
    target_form_id,
    originating_form_name,
    target_form_name,
    kuali_instance,
):
    """
    Updates the permissions of the target form using the provided originating form ID,
    target form ID, originating form name, target form name, and Kuali instance.
    It sends GraphQL mutation requests to update the permissions based on the provided JSON strings.

    Args:
        originating_form_id: The ID of the originating form.
        target_form_id: The ID of the target form.
        originating_form_name: The name of the originating form.
        target_form_name: The name of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    json_strings = KualiVariables.Mutations.UpdatePermissions(
        originating_form_id,
        target_form_id,
        originating_form_name,
        target_form_name,
    )
    try:
        for json_str in json_strings:
            variables = json.loads(json_str)
            logger.info(
                f"update_form_permissions variables: {json.dumps(variables, indent=4)}"
            )
            url = kuali_instance.url()
            headers = kuali_instance.headers()
            graphql = KualiGraphQL.Mutations.UpdatePermissions()
            outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
            try:
                logger.info(
                    f"update_form_permissions outcome: {json.dumps(outcome, indent=4)}"
                )
                success_returned = outcome
            except Exception as oops:
                logger.info(f"Error: {str(oops)}")

        logger.success("Permissions updated!")

    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
