# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger
import traceback

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def search_for_user(user_kuali_id, kuali_from, kuali_to):
    """
    Searches for a user with the provided Kuali ID in the source environment and retrieves or creates
    the corresponding user in the target environment using the provided Kuali instances.

    Args:
        user_kuali_id: The Kuali ID of the user.
        kuali_from: The Kuali instance for the source environment.
        kuali_to: The Kuali instance for the target environment.

    Returns:
        A dictionary containing the user data in the target environment, or None if the search or creation failed.
    """
    try:
        user_data_from = await get_user_data(user_kuali_id, kuali_from)
        if not user_data_from:
            raise ValueError("User not found in source environment")

        schoolId = user_data_from.get("schoolId")

        user_data_to = await get_target_user_data(
            schoolId, kuali_to
        ) or await create_new_users(format_create_user(user_data_from), kuali_to)

        if not user_data_to:
            raise ValueError("Failed to retrieve or create user in target environment")
        return {
            "email": user_data_to.get("email"),
            "firstName": user_data_to.get("firstName"),
            "id": user_data_to.get("id"),
            "lastName": user_data_to.get("lastName"),
            "schoolId": user_data_to.get("schoolId"),
            "username": user_data_to.get("username"),
            "name": user_data_to.get("displayName"),
        }
    except Exception as damn:
        logger.error(f"Error: {str(damn)}\n{traceback.format_exc()}")
        return None


async def get_user_data(user_kuali_id, kuali):
    """
    Retrieves user data for the specified Kuali ID using the provided Kuali instance.

    Args:
        user_kuali_id: The Kuali ID of the user.
        kuali: The Kuali instance.

    Returns:
        A dictionary containing the user data, or None if the user data could not be retrieved.
    """
    url = kuali.url()
    headers = kuali.headers()
    graphql = KualiGraphQL.Queries.GetUserByKualiID()
    variables = KualiVariables.Queries.GetUserByKualiID(user_kuali_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    return outcome.get("data", {}).get("user") or None


async def get_target_user_data(schoolId, kuali):
    """
    Retrieves the target user data for the specified school ID using the provided Kuali instance.

    Args:
        schoolId: The school ID of the target user.
        kuali: The Kuali instance.

    Returns:
        A dictionary containing the target user data, or None if the user data could not be retrieved.
    """
    url = kuali.url()
    headers = kuali.headers()
    graphql = KualiGraphQL.Queries.GetUser()
    variables = KualiVariables.Queries.GetUser(schoolId)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)

    if users_connection := outcome.get("data", {}).get("usersConnection"):
        edges = users_connection.get("edges", [])
        if not edges:
            logger.warning(
                f"No edges found in users_connection for schoolId: {schoolId}"
            )
        return edges[0].get("node") if edges else None
    logger.warning(f"No usersConnection found for schoolId: {schoolId}")
    return None


def format_create_user(user_data):
    """
    Formats the user data into a dictionary suitable for creating a new user.

    Args:
        user_data: The user data to be formatted.

    Returns:
        A dictionary containing the formatted user data.
    """
    if user_data:
        return {
            "data": {
                "role": "user",
                "approved": "true",
                "id": user_data.get("id"),
                "firstName": user_data.get("firstName"),
                "lastName": user_data.get("lastName"),
                "name": user_data.get("name"),
                "username": user_data.get("username"),
                "email": user_data.get("email"),
                "schoolId": user_data.get("schoolId"),
            }
        }


async def create_new_users(create_user, kuali_to):
    """
    Creates new users using the provided user data and Kuali instance.

    Args:
        create_user: The user data for creating new users.
        kuali_to: The Kuali instance for the target environment.

    Returns:
        A dictionary containing the user data of the created users, or None if the creation failed.
    """
    try:
        url = kuali_to.url()
        headers = kuali_to.headers()
        graphql = KualiGraphQL.Mutations.CreateUser()
        variables = KualiVariables.Mutations.CreateUser(create_user)
        outcome = await Kuali.FetchResponse(url, headers, graphql, variables)

        user_data = outcome.get("data", {}).get("createUser")
        if not user_data:
            logger.error("No user data returned while creating new user.")
            return None

        if errors := user_data.get("errors"):
            logger.error(f"Errors encountered while creating user: {errors}")

        return user_data
    except Exception as dammit:
        logger.error(
            f"Error in create_new_users: {str(dammit)}\n{traceback.format_exc()}"
        )
