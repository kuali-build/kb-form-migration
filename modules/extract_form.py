# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


async def publish_originating_form(originating_form_id, kuali_instance):
    """
    Publishes the originating form with the provided form ID using the provided Kuali instance.

    Args:
        originating_form_id: The ID of the originating form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Mutations.PublishForm()
    variables = KualiVariables.Mutations.PublishForm(originating_form_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = (
            outcome.get("data", {})
            .get("publishApp", {})
            .get("dataset", {})
            .get("isPublished", None)
        )
        if success_returned is not None:
            logger.success(
                "Published the origin form to ensure we have the most up-to-date template"
            )
        else:
            raise KeyError
    except Exception:
        error_returned = outcome.get("errors", [{}])[0].get("message", "Unknown error")
        logger.success(
            f"No need to publish the original form because Kuali says '{error_returned}'"
        )


def find_ids(obj, result=None):
    """
    Recursively searches for and extracts IDs from the provided object.

    Args:
        obj: The object to search for IDs.
        result: The list to store the extracted IDs.

    Returns:
        A list of extracted IDs.
    """
    if result is None:
        result = []
    if isinstance(obj, dict):
        for k, v in obj.items():
            if k == "id" and isinstance(v, str) and len(v) == 24 and "data" not in v:
                new_item = {"integrationId": v}
                if new_item not in result:
                    result.append(new_item)
            elif isinstance(v, (dict, list)):
                find_ids(v, result)
    elif isinstance(obj, list):
        for item in obj:
            find_ids(item, result)
    return result


async def extract_originating_form(
    originating_form_id,
    target_form_id,
    originating_form_name,
    target_form_name,
    kuali_instance,
):
    """
    Extracts the originating form with the provided form ID and saves it as a JSON file.
    Additionally, it searches for 'id' keys with 24-character values in the extracted form
    and saves them to another JSON file.

    Args:
        originating_form_id: The ID of the originating form.
        target_form_id: The ID of the target form.
        originating_form_name: The name of the originating form.
        target_form_name: The name of the target form.
        kuali_instance: The Kuali instance.

    Returns:
        None.
    """
    url = kuali_instance.url()
    headers = kuali_instance.headers()
    graphql = KualiGraphQL.Queries.ExtractForm()
    variables = KualiVariables.Queries.ExtractForm(originating_form_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        success_returned = (
            outcome.get("data", {}).get("app", {}).get("formVersion", None)
        )

        if success_returned is not None:
            success_returned["appId"] = target_form_id
            json_str = json.dumps(success_returned, ensure_ascii=False, indent=4)
            with open(
                join(
                    parent_dir,
                    "kuali_skeletons",
                    target_form_name,
                    "form.json",
                ),
                "w",
            ) as f:
                f.write(json_str)
            logger.success("form.json file created")

            # Find all 'id' keys with 24-character values and write them to a new JSON file
            id_dict = find_ids(success_returned)
            with open(
                join(
                    parent_dir,
                    "kuali_skeletons",
                    target_form_name,
                    "integrations.json",
                ),
                "w",
            ) as f:
                f.write(json.dumps(id_dict, indent=4))
            logger.success("integrations.json file created")
        else:
            raise KeyError
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")
