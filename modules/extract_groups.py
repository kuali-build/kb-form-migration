# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables
from .create_users import search_for_user


async def extract_groups(group_id, kuali_from, kuali_to):
    """
    Extracts the members of a group with the provided group ID from the source environment using the
    Kuali instance for the source environment and returns the extracted members.

    Args:
        group_id: The ID of the group.
        kuali_from: The Kuali instance for the source environment.
        kuali_to: The Kuali instance for the target environment.

    Returns:
        A list of dictionaries containing the extracted members of the group, or an empty list if no members are found.
    """
    url = kuali_from.url()
    headers = kuali_from.headers()
    graphql = KualiGraphQL.Queries.GetGroupByKualiID()
    variables = KualiVariables.Queries.GetGroupByKualiID(group_id)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)

    try:
        group = outcome.get("data", {}).get("group", {})
        group_name = group.get("name")
        roles = group.get("roles", [])

        for role in roles:
            return role.get("membersConnection", {}).get("edges", [{}])
    except Exception as oops:
        logger.info(f"Error: {str(oops)}")

    return []
