# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables


replace_ids = {"6499ba62ea5822d07308fa6e": "64c2fd781a39e7ec9d7807e5"}


def replace_id_in_object(obj, replace_ids):
    """
    Recursively replaces IDs in the provided object with the corresponding replacement IDs.

    Args:
        obj: The object in which to replace the IDs.
        replace_ids: A dictionary mapping original IDs to replacement IDs.

    Returns:
        The object with replaced IDs.
    """
    if isinstance(obj, dict):
        for k, v in obj.items():
            if k == "id" and v in replace_ids:
                obj["id"] = replace_ids[v]
            if isinstance(v, (dict, list)):
                replace_id_in_object(v, replace_ids)
    elif isinstance(obj, list):
        for item in obj:
            replace_id_in_object(item, replace_ids)
    return obj


def update_labels(obj, new_label, target_id):
    """
    Recursively updates the label of the integration with the specified target ID in the provided object
    to the new label.

    Args:
        obj: The object to search for the integration with the target ID.
        new_label: The new label to update the integration with.
        target_id: The target ID of the integration to update.

    Returns:
        The updated object.
    """
    if isinstance(obj, dict):
        if (
            "details" in obj
            and isinstance(obj["details"], dict)
            and obj["details"].get("id") == target_id
        ):
            logger.success(
                f"Updating matching integration | id: {target_id} | name: {new_label}"
            )
            if "label" in obj["details"]:
                logger.success(
                    f"Updating label from '{obj['details']['label']}' to '{new_label}'"
                )
                obj["details"]["label"] = new_label
        for k, v in obj.items():
            if isinstance(v, (dict, list)):
                update_labels(v, new_label, target_id)
    elif isinstance(obj, list):
        for item in obj:
            update_labels(item, new_label, target_id)
    return obj


async def extract_form_integrations(
    originating_form_id,
    target_form_id,
    originating_form_name,
    target_form_name,
    kuali_from,
    kuali_to,
):
    """
    Extracts form integrations from the originating form, updates the integration IDs and labels,
    and saves the updated integrations as JSON files. It also generates a GraphQL mutation string
    and a variables JSON file for removing the created apps and integrations.

    Args:
        originating_form_id: The ID of the originating form.
        target_form_id: The ID of the target form.
        originating_form_name: The name of the originating form.
        target_form_name: The name of the target form.
        kuali_from: The Kuali instance for the source environment.
        kuali_to: The Kuali instance for the target environment.

    Returns:
        None.
    """
    with open(
        join(
            parent_dir,
            "kuali_skeletons",
            target_form_name,
            "integrations.json",
        ),
        "r",
    ) as f:
        integrations = json.load(f)

    index = 1
    new_integrations = []
    for integration in integrations:
        if integration_id := integration.get("integrationId"):
            # Replace or remove the integration ID
            if integration_id in replace_ids:
                if replace_ids[integration_id] is None:
                    logger.warning(f"Removing integration {integration_id}...")
                    continue
                else:
                    logger.info(
                        f"Replacing integration {integration_id} with {replace_ids[integration_id]}..."
                    )
                    integration_id = replace_ids[integration_id]
            url = kuali_from.url()
            headers = kuali_from.headers()
            graphql = KualiGraphQL.Queries.ExtractIntegration()
            variables = KualiVariables.Queries.ExtractIntegration(integration_id)
            outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
            try:
                integration_name = (
                    outcome.get("data", {}).get("integration", {}).get("name")
                )
                data_shared_with = (
                    outcome.get("data", {})
                    .get("integration", {})
                    .get("dataSharedWith", {})
                )
                share_type = data_shared_with.get("type", None)
                success_returned = data_shared_with.get("apps", [])

                if integration_name is None:
                    logger.warning(
                        f"No name for integration {integration_id}. Removing from list..."
                    )
                    continue

                if data_shared_with is None:
                    logger.warning(
                        f"No dataSharedWith for integration {integration_id}. Skipping..."
                    )
                    continue

                integration["integrationName"] = integration_name
                if share_type == "ALL" and kuali_from == kuali_to:
                    integration["Integration availability"] = "Shared with all apps"
                elif share_type == "SPECIFIC":
                    integration[
                        "Integration availability"
                    ] = "Shared with specific apps only"
                new_integrations.append(integration)

                if share_type is None:
                    raise KeyError
                for app in success_returned:
                    app["label"] = app.pop("name", None)
                new_app = {"id": target_form_id, "label": target_form_name}
                success_returned.append(new_app)
                seen = set()
                success_returned = [
                    app
                    for app in success_returned
                    if app["id"] not in seen and not seen.add(app["id"])
                ]

                if kuali_from != kuali_to:
                    share_type = "SPECIFIC"

                data_to_prepend = {
                    "id": integration_id,
                    "sharingType": share_type,
                    "apps": success_returned,
                }

                json_str = json.dumps(data_to_prepend, indent=4)
                with open(
                    join(
                        parent_dir,
                        "kuali_skeletons",
                        target_form_name,
                        "integrations",
                        f"integration_{index}.json",
                    ),
                    "w",
                ) as f:
                    f.write(json_str)

                logger.success(f"integration_{index}.json file created")
                index += 1
            except Exception as oops:
                logger.success(
                    "Delisting unused integration from form integration options"
                )

        with open(
            join(
                parent_dir,
                "kuali_skeletons",
                target_form_name,
                "integrations.json",
            ),
            "w",
        ) as f:
            json.dump(integrations, f, indent=4)

    #####################Construct East removal###############################

    mutation_parts = []  # To store parts of the mutation string.
    variables = {"appId": target_form_id}  # Initializing the variables dict with appId.

    for idx, integration in enumerate(
        new_integrations, start=1
    ):  # Note: idx starts from 1.
        if integration_id := integration.get("integrationId"):
            mutation_parts.append(
                f"  removeIntegration{idx}: removeIntegration(args: {{id: $id{idx}}}) {{ id }}"
            )
            variables[f"id{idx}"] = integration_id

    # Constructing the final mutation string.
    mutation_str = f"mutation CombinedMutation($appId: ID!, {' ,'.join([f'$id{i}: ID!' for i in range(1, len(new_integrations) + 1)])}) {{\n"
    mutation_str += f"  deleteAppAlias: deleteApp(id: $appId)\n"
    mutation_str += "\n".join(mutation_parts)
    mutation_str += "\n}"

    # Writing the mutation string to a .gql file
    gql_filename = join(
        parent_dir,
        "kuali_skeletons",
        target_form_name,
        "remove_created_apps_and_integrations.gql",
    )
    with open(gql_filename, "w") as f:
        f.write(mutation_str)

    # Writing the variables to a .json file
    json_filename = join(
        parent_dir,
        "kuali_skeletons",
        target_form_name,
        "remove_created_apps_and_integrations.json",
    )
    with open(json_filename, "w") as f:
        json.dump(variables, f, indent=4)
