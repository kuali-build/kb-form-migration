# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger
import string
import aiofiles
import traceback

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables
from .create_users import search_for_user
from .create_group import create_groups, add_group_members

NAME_MAPPING = {
    "Administrators": "admin",
    "Reviewers": "reviewer",
    "All Anonymous Users": "users",
}
FILE_DIR = dirname(abspath(__file__))
PARENT_DIR = dirname(FILE_DIR)
sys.path.append(FILE_DIR)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables
from .create_users import search_for_user
from .extract_groups import extract_groups


async def extract_policy_groups(form_id, form_name, kuali):
    """
    Extracts the policy groups associated with the specified form ID using the provided Kuali instance.

    Args:
        form_id: The ID of the form.
        form_name: The name of the form.
        kuali: The Kuali instance.

    Returns:
        A list of dictionaries containing the extracted policy groups, or an empty list if no policy groups are found.
    """
    try:
        url = kuali.url()
        headers = kuali.headers()
        graphql = KualiGraphQL.Queries.ExtractPermissions()
        variables = KualiVariables.Queries.ExtractPermissions(form_id)
        outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
        return outcome.get("data", {}).get("app", {}).get("listPolicyGroups", [{}])
    except Exception as oops:
        logger.error(
            f"Error in extract_policy_groups: {str(oops)}\n{traceback.format_exc()}"
        )
        return []


async def update_policy_groups(policy_groups, newbee_policy_group_dict):
    """
    Updates the IDs of the policy groups in the provided list of policy groups
    using the dictionary of new policy group IDs.

    Args:
        policy_groups: The list of policy groups to update.
        newbee_policy_group_dict: The dictionary mapping policy group names to new policy group IDs.

    Returns:
        None.
    """
    try:
        for group in policy_groups:
            if group["name"] in newbee_policy_group_dict:
                group["id"] = newbee_policy_group_dict[group["name"]]
    except Exception as oops:
        logger.error(f"Error: {str(oops)}\n{traceback.format_exc()}")


async def write_permission_files(policy_groups, target_form_name, kuali_from, kuali_to):
    """
    Writes permission files for the specified policy groups and target form name.
    The function searches for users, updates user IDs, and creates new groups if necessary.
    It then writes the permission files in JSON format based on the granted permissions.

    Args:
        policy_groups: The list of policy groups.
        target_form_name: The name of the target form.
        kuali_from: The Kuali instance for the source environment.
        kuali_to: The Kuali instance for the target environment.

    Returns:
        None.
    """
    try:
        for group in policy_groups:
            for user in group["identities"]:
                if user.get("type") == "USER":
                    new_user_info = await search_for_user(
                        user["id"], kuali_from, kuali_to
                    )

                    if not new_user_info or not new_user_info.get("id"):
                        logger.error(f"No user info received for user ID: {user['id']}")
                        continue

                    user["id"] = new_user_info["id"]

                    granted_permission_name = user["label"]
                    group_name = NAME_MAPPING.get(group["name"], group["name"]).replace(
                        " ", "_"
                    )
                    filename = f"{granted_permission_name}_as_{group_name}.json".lower()
                    filepath = join(
                        PARENT_DIR,
                        "kuali_skeletons",
                        target_form_name,
                        "permissions",
                        filename,
                    )

                    async with aiofiles.open(filepath, mode="w") as file:
                        await file.write(
                            json.dumps(
                                {"roleId": group["id"], "identity": user},
                                indent=4,
                            )
                        )

                    logger.success(
                        f"{granted_permission_name} granted {group_name} privileges"
                    )

                elif user.get("type") == "GROUP":
                    group_peeps = await extract_groups(user["id"], kuali_from, kuali_to)
                    new_group_name = f"{user['label']}-{target_form_name}"
                    create_group = await create_groups(
                        group_peeps, new_group_name, kuali_from, kuali_to
                    )

                    if not create_group or not create_group.get("id"):
                        logger.error(f"No user info received for user ID: {user['id']}")
                        continue

                    user["id"] = create_group["id"]

                    granted_permission_name = user["label"]
                    group_name = NAME_MAPPING.get(group["name"], group["name"]).replace(
                        " ", "_"
                    )
                    filename = f"{granted_permission_name}_as_{group_name}.json".lower()
                    filepath = join(
                        PARENT_DIR,
                        "kuali_skeletons",
                        target_form_name,
                        "permissions",
                        filename,
                    )

                    async with aiofiles.open(filepath, mode="w") as file:
                        await file.write(
                            json.dumps(
                                {"roleId": group["id"], "identity": user},
                                indent=4,
                            )
                        )

                    logger.success(
                        f"{granted_permission_name} granted {group_name} privileges"
                    )

    except Exception as oops:
        logger.error(f"Error: {str(oops)}\n{traceback.format_exc()}")


async def extract_and_update_permissions(
    originating_form_id,
    target_form_id,
    originating_form_name,
    target_form_name,
    kuali_from,
    kuali_to,
):
    """
    Extracts policy groups from the originating form and the target form using the provided Kuali instances.
    It updates the policy group IDs in the originating form based on the target form's policy group IDs,
    and writes permission files for the policy groups in the originating form.

    Args:
        originating_form_id: The ID of the originating form.
        target_form_id: The ID of the target form.
        originating_form_name: The name of the originating form.
        target_form_name: The name of the target form.
        kuali_from: The Kuali instance for the source environment.
        kuali_to: The Kuali instance for the target environment.

    Returns:
        None.
    """
    try:
        og_policy_groups = await extract_policy_groups(
            originating_form_id, originating_form_name, kuali_from
        )
        newbee_policy_groups = await extract_policy_groups(
            target_form_id, target_form_name, kuali_to
        )
        newbee_policy_group_dict = {
            group["name"]: group["id"]
            for group in newbee_policy_groups
            if "name" in group and "id" in group
        }

        await update_policy_groups(og_policy_groups, newbee_policy_group_dict)
        await write_permission_files(
            og_policy_groups, target_form_name, kuali_from, kuali_to
        )
    except Exception as oops:
        logger.error(
            f"Error in extract_and_update_permissions: {str(oops)}\n{traceback.format_exc()}"
        )
