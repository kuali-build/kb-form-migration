# Standard libraries
import os
from os import getenv
from os.path import dirname, abspath, join
import sys
import json
from json import JSONDecodeError
from loguru import logger
import traceback

current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

from graphql_calls import Kuali, KualiGraphQL
from graphql_variables import KualiVariables
from .create_users import search_for_user


async def create_groups(members, group_name, kuali_from, kuali_to):
    """
    Creates a group with the provided members and group name using the Kuali instances for the source and destination.

    Args:
        members: The list of members to add to the group.
        group_name: The name of the group.
        kuali_from: The Kuali instance for the source.
        kuali_to: The Kuali instance for the destination.

    Returns:
        A dictionary containing the ID of the created group, or None if the group creation failed.
    """
    url = kuali_to.url()
    headers = kuali_to.headers()
    graphql = KualiGraphQL.Mutations.CreateGroup()
    variables = KualiVariables.Mutations.CreateGroup(group_name)
    outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
    try:
        if group := outcome.get("data", {}).get("createGroup", {}).get("group"):
            if group := group.get("id"):
                logger.info(group)
                await add_group_members(members, group, kuali_from, kuali_to)
                return {"id": group}
        logger.error("Group ID or Category ID not found in response.")
    except Exception as oops:
        logger.error(f"Error: {str(oops)}\n{traceback.format_exc()}")
    return None


async def add_group_members(members, new_group_id, kuali_from, kuali_to):
    """
    Adds members to a group with the provided group ID using the Kuali instances for the source and destination.

    Args:
        members: The list of members to add to the group.
        new_group_id: The ID of the group.
        kuali_from: The Kuali instance for the source.
        kuali_to: The Kuali instance for the destination.

    Returns:
        None.
    """
    url = kuali_to.url()
    headers = kuali_to.headers()
    graphql = KualiGraphQL.Mutations.AddGroupRoleUser()
    for member in members:
        user_id = member.get("node", {}).get("id")
        get_each_user = await search_for_user(user_id, kuali_from, kuali_to)

        if isinstance(get_each_user, dict):
            user_id = get_each_user.get("id")

        variables = KualiVariables.Mutations.AddGroupRoleUser(
            new_group_id, "members", user_id
        )
        outcome = await Kuali.FetchResponse(url, headers, graphql, variables)
        try:
            results = (
                outcome.get("data", {})
                .get("addGroupRoleUser", {})
                .get("result", {})
                .get("members", [])
            )
            logger.info(f"{results}")
        except Exception as oops:
            logger.error(f"Error: {str(oops)}\n{traceback.format_exc()}")
