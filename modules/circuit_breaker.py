import sys
from loguru import logger
import time


class CircuitBreaker:
    """
    Represents a circuit breaker for managing failures and retries.

    Args:
        max_failures (int): The maximum number of failures allowed before the circuit is opened.
        reset_time (float): The time in seconds after which the circuit is reset.
        backoff_factor (float): The factor by which the wait time increases for each retry attempt.
        max_attempts (int): The maximum number of retry attempts allowed.

    Methods:
        record_failure(): Records a failure and determines if a retry is possible.
        reset_attempts(): Resets the retry attempts counter.
        record_success(): Records a successful execution and resets the failure count.
        can_execute(): Checks if the circuit is closed and allows execution.

    Examples:
        >>> circuit_breaker = CircuitBreaker(max_failures=5, reset_time=1, backoff_factor=1.5, max_attempts=10)
        >>> circuit_breaker.record_failure()
        >>> circuit_breaker.can_execute()
    """

    def __init__(
        self, max_failures=5, reset_time=1, backoff_factor=1.5, max_attempts=10
    ):
        self.max_failures = max_failures
        self.reset_time = reset_time
        self.failures = 0
        self.last_failure_time = None
        self.backoff_factor = backoff_factor
        self.attempt = 0
        self.max_attempts = max_attempts

    async def record_failure(self):
        self.failures += 1
        self.attempt += 1
        self.last_failure_time = time.time()
        logger.warning(f"Failure recorded. Total failures: {self.failures}.")

        if self.failures >= self.max_failures:
            if self.attempt <= self.max_attempts:
                wait_time = self.reset_time * (self.backoff_factor**self.attempt)
                logger.warning(
                    f"Attempt {self.attempt} of {self.max_attempts}. Waiting for {wait_time} seconds before retry."
                )
                await asyncio.sleep(wait_time)
            else:
                logger.error(f"Max attempts ({self.max_attempts}) reached. Giving up.")

    def reset_attempts(self):
        self.attempt = 0

    def record_success(self):
        if self.failures > 0:
            logger.info(f"Success recorded after {self.failures} failures.")
        else:
            logger.info("Success recorded.")
        self.failures = 0
        self.last_failure_time = None

    def can_execute(self):
        if self.failures < self.max_failures:
            return True
        if time.time() - self.last_failure_time > self.reset_time:
            self.record_success()
            return True
        logger.warning(
            f"Cannot execute. Last failure was less than {self.reset_time} seconds ago."
        )
        return False
