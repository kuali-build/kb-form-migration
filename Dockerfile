FROM python:3.12.2-slim as builder

WORKDIR /build

COPY requirements.txt .
COPY healthcheck.sh /usr/local/bin/

RUN apt-get update && apt-get install -y --no-install-recommends \
    && pip install --no-cache-dir --upgrade pip setuptools wheel \
    && pip install --no-cache-dir -r requirements.txt \
    && pip install --no-cache-dir pip-audit \
    && pip-audit --fix \
    && pip uninstall -y pip-audit \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
    && rm -rf /var/lib/apt/lists/*

FROM python:3.12.2-slim

WORKDIR /app

COPY --from=builder /usr/local/lib/python3.12/site-packages /usr/local/lib/python3.12/site-packages
COPY --from=builder /usr/local/bin /usr/local/bin
COPY ./GraphQL/ ./GraphQL/
COPY ./modules/ ./modules/
COPY ./utilities/ ./utilities/
COPY endpoints.py .

RUN apt-get update && apt-get install -y --no-install-recommends \
    curl \
    jq \
    && cp /usr/share/zoneinfo/America/New_York /etc/localtime \
    && echo "America/New_York" > /etc/timezone \
    && chmod +x /usr/local/bin/healthcheck.sh \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
    && rm -rf /var/lib/apt/lists/*
