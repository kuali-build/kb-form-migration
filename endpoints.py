# Standard Libs
from os import makedirs
from pathlib import Path
from os.path import dirname, abspath, join
from fastapi import FastAPI, Depends, Request, BackgroundTasks
from fastapi.responses import JSONResponse, ORJSONResponse
from loguru import logger
from dotenv import load_dotenv
from starlette.types import Lifespan

import modules as mod
from utilities.task_processor import process_create_form_task
from utilities.middleware import mware, access, lifespan
from utilities.kuali_environment import get_env_variables


load_dotenv(verbose=True, override=True)
CDIR = Path(__file__).resolve().parent

def create_directories(target_form_name: str):
    base_dir = join(dirname(abspath(__file__)), "kuali_skeletons", target_form_name)
    sub_dirs = ["integrations", "permissions"]
    for sub_dir in sub_dirs:
        makedirs(join(base_dir, sub_dir), exist_ok=True)


def configure_logging(target_form_name: str):
    log_file = Path(CDIR) / "kuali_skeletons" / target_form_name / "actions.log"
    logger.add(
        log_file,
        rotation="10 MB",
        compression="zip",
        retention="10 days",
        format="{time}",
        serialize=True,
        enqueue=True,
        diagnose=True,
        backtrace=True,
        catch=True,
    )


app = FastAPI(
    openapi_url=None,
    docs_url=None,
    redoc_url=None,
    default_response_class=ORJSONResponse,
    dependencies=[Depends(access)],
    lifespan=lifespan,
)

mware(app)


@logger.catch
@app.get("/knock-knock")
async def alive_response():
    return {"app": "ok"}


@logger.catch
@app.get("/environment")
async def select_env():
    return [{"env": "production"}, {"env": "sandbox"}, {"env": "staging"}]


@logger.catch
@app.get("/search")
async def search_spaces(search: str, kuali_instance: str):
    from_env_vars = get_env_variables(kuali_instance)
    source_kuali = mod.Kuali(from_env_vars["instance"], from_env_vars["auth_token"])
    searching = await mod.search_for_app(search, source_kuali)
    return JSONResponse(searching)


@logger.catch
@app.post("/perform_tasks")
async def api_perform_tasks(request: Request, background_tasks: BackgroundTasks):
    data = await request.json()
    meta = data.get("meta", {}).get("createdBy", {})
    from_form = data.get("from_form", {}).get("data", {})
    if (
        form_other_owner := data.get("owner_select", {})
        .get("data", {})
        .get("typeahead", {})
    ):
        user_duid = form_other_owner.get("schoolId")
        kuali_user_id = form_other_owner.get("id")
    else:
        user_duid = meta.get("schoolId")
        kuali_user_id = meta.get("id")
    from_environment = data.get("from_environment", {}).get("data", {}).get("env")
    to_environment = data.get("to_environment", {}).get("data", {}).get("env")
    from_env_vars = get_env_variables(from_environment)
    to_env_vars = get_env_variables(to_environment)
    source_kuali = mod.Kuali(from_env_vars["instance"], from_env_vars["auth_token"])
    target_kuali = mod.Kuali(to_env_vars["instance"], to_env_vars["auth_token"])
    from_form_id = from_form.get("id")
    form_name = from_form.get("name")
    icon_name = from_form.get("tileOptions_iconName")
    icon_color = from_form.get("tileOptions_backgroundColor")
    target_form_name = data.get("target_form_name")
    configure_logging(target_form_name)
    create_directories(target_form_name)
    background_tasks.add_task(
        process_create_form_task,
        to_environment,
        request,
        user_duid,
        kuali_user_id,
        from_form_id,
        form_name,
        target_form_name,
        source_kuali,
        target_kuali,
        icon_name,
        icon_color,
    )

    return JSONResponse(
        status_code=202,
        content={"status": "Just.. give me a minute"},
        headers={"Connection": "close"},
    )
